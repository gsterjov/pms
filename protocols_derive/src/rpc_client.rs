use proc_macro2::TokenStream;
use quote::{format_ident, quote};
use syn;


struct Arg {
    ident: syn::Ident,
    type_path: syn::Path,
}


struct ResponseGetter {
    ty: syn::ReturnType,
}

impl ResponseGetter {
    fn new(ty: syn::ReturnType) -> Self {
        Self { ty }
    }

    fn list_to_tokens(&self, item_type: &syn::Path) -> TokenStream {
        quote! {
            let mut list = Vec::new();
            for response_reader in response.get()?.iter() {
                list.push(#item_type::from_capnp(&response_reader)?);
            }
            Ok(list)
        }
    }

    fn custom_to_tokens(&self, custom_type: &syn::Path) -> TokenStream {
        quote! {
            Ok(#custom_type::from_capnp(&response.get()?)?)
        }
    }

    fn result_to_tokens(&self, return_type: &syn::Path) -> TokenStream {
        let generics = extract_generics(return_type.segments.first().unwrap());

        if generics.len() <= 1 {
            quote! {
                Ok(())
            }
        } else {
            let ok_path = &generics[0];

            if ok_path.segments.first().unwrap().ident == "Vec" {
                self.list_to_tokens(&ok_path)
            }
            else {
                self.custom_to_tokens(&ok_path)
            }
        }
    }
}

impl quote::ToTokens for ResponseGetter {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let gen = if let syn::ReturnType::Type(_, ty) = &self.ty {
            match ty.as_ref() {
                syn::Type::Path(type_path) => {
                    self.result_to_tokens(&type_path.path)
                }
                _ => panic!("unsupported return type")
            }
        }
        else {
            panic!("return type must be a Result<T, E>")
        };
        gen.to_tokens(tokens);
    }
}


struct RpcTraitMethod {
    method: syn::TraitItemMethod,
    response_getter: ResponseGetter,
}

impl RpcTraitMethod {
    fn new(method: &syn::TraitItemMethod) -> Self {
        Self {
            method: method.clone(),
            response_getter: ResponseGetter::new(method.sig.output.clone()),
        }
    }

    fn primitive_to_tokens(&self, arg: &Arg) -> TokenStream {
        let param_name = &arg.ident;
        let set_method = format_ident!("set_{}", param_name);
        quote! {
            params_builder.#set_method(#param_name);
        }
    }

    fn custom_to_tokens(&self, arg: &Arg) -> TokenStream {
        let param_name = &arg.ident;
        let init_name = format_ident!("init_{}", param_name);
        quote! {
            #param_name.to_capnp(&mut params_builder.#init_name());
        }
    }

    fn transformed_args(&self) -> Vec<TokenStream> {
        let mut transforms = Vec::new();
        for arg in extract_args(&self.method.sig).iter() {
            let gen = if is_primitive(&arg.type_path) {
                self.primitive_to_tokens(arg)
            }
            else {
                self.custom_to_tokens(arg)
            };
            transforms.push(gen);
        }

        transforms
    }

    fn transformed_return(&self) -> TokenStream {
        match &self.method.sig.output {
            syn::ReturnType::Type(_, ty) => {
                if let syn::Type::Path(type_path) = ty.as_ref() {
                    let (result, error) = unpack_result(&type_path.path);
                    quote! {
                        ::capnp::capability::Promise<#result, #error>
                    }
                } else {
                    panic!("invalid return type")
                }
            },
            _ => panic!("you gotta return somthing"),
        }
    }

    fn default_block(&self) -> TokenStream {
        let method_name = &self.method.sig.ident;
        let request_name = format_ident!("{}_request", method_name);
        let transforms = self.transformed_args();
        let response_getter = &self.response_getter;

        quote! {
            let mut client = self.rpc_client();
            let mut request = client.#request_name();
            let mut params_builder = request.get();
            #(#transforms)*

            ::capnp::capability::Promise::from_future(async move {
                let response = request.send().promise.await?;
                #response_getter
            })
        }
    }
}

impl quote::ToTokens for RpcTraitMethod {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let method_name = &self.method.sig.ident;
        let method_args = &self.method.sig.inputs;
        let default_block = self.default_block();
        let return_type = self.transformed_return();

        let gen = quote! {
            fn #method_name(#method_args) -> #return_type {
                #default_block
            }
        };
        gen.to_tokens(tokens);
    }
}


pub fn impl_rpc_client_traits(args: &syn::AttributeArgs, item: &syn::Item) -> TokenStream {
    let module_path = get_module_path(args.first().unwrap()).unwrap();

    let (name, methods) = match item {
        syn::Item::Trait(item_trait) => {
            let mut methods = Vec::new();

            for item in item_trait.items.iter() {
                if let syn::TraitItem::Method(method) = item {
                    methods.push(RpcTraitMethod::new(&method));
                }
            }

            (&item_trait.ident, methods)
        }
        _ => panic!("Only trait implementations can be used with #[capnp_rpc_client]")
    };

    let gen = quote! {
        pub trait #name {
            fn rpc_client(&self) -> &#module_path::Client;
            #(#methods)*
        }
    };
    gen.into()
}


fn extract_args(sig: &syn::Signature) -> Vec<Arg> {
    let mut args = Vec::new();

    for arg in sig.inputs.iter() {
        match arg {
            syn::FnArg::Receiver(_) => {}
            syn::FnArg::Typed(pat_type) => args.push(extract_typed_arg(pat_type))
        }
    }

    args
}

fn extract_typed_arg(typed_arg: &syn::PatType) -> Arg {
    let arg_type = extract_type(&typed_arg.ty);

    let ident = match typed_arg.pat.as_ref() {
        syn::Pat::Ident(pat_ident) => Some(pat_ident.ident.clone()),
        _ => None,
    };

    Arg {
        ident: ident.unwrap().clone(),
        type_path: arg_type,
    }
}

fn extract_type(ty: &syn::Type) -> syn::Path {
    match ty {
        syn::Type::Path(type_path) => {
            type_path.path.clone()
        }
        syn::Type::Reference(reference) => {
            let ref_type = extract_type(reference.elem.as_ref());
            ref_type
        }
        _ => { panic!("unsupported type") }
    }
}


fn extract_generics(path: &syn::PathSegment) -> Vec<syn::Path> {
    let mut paths = Vec::new();

    if let syn::PathArguments::AngleBracketed(generics) = &path.arguments {
        for arg in &generics.args {
            if let syn::GenericArgument::Type(syn::Type::Path(generic_path)) = arg {
                paths.push(generic_path.path.clone());
            }
        }
    }

    paths
}


fn unpack_result(path: &syn::Path) -> (syn::GenericArgument, syn::GenericArgument) {
    match &path.segments.first().unwrap().arguments {
        syn::PathArguments::AngleBracketed(generics) => {
            (generics.args[0].clone(), generics.args[1].clone())
        }
        _ => panic!("must be a result with generic args")
    }
}


fn get_module_path(attr: &syn::NestedMeta) -> syn::Result<syn::Path> {
    match attr {
        syn::NestedMeta::Lit(syn::Lit::Str(lit_str)) => {
            lit_str.parse()
        }
        _ => {
            let message = "expected #[capnp_rpc_client(\"path::to::module\")]";
            Err(syn::Error::new_spanned(attr, message))
        }
    }
}


fn is_primitive(path: &syn::Path) -> bool {
    path.is_ident("i8")
        || path.is_ident("u8")
        || path.is_ident("i16")
        || path.is_ident("u16")
        || path.is_ident("i32")
        || path.is_ident("u32")
        || path.is_ident("i64")
        || path.is_ident("u64")
        || path.is_ident("i128")
        || path.is_ident("u128")
        || path.is_ident("isize")
        || path.is_ident("usize")
}
