use proc_macro2::TokenStream;
use quote::{format_ident, quote, quote_spanned};
use syn;
use heck::CamelCase;


#[derive(Clone)]
struct RpcArg {
    ident: syn::Ident,
    type_path: syn::Path,
}

impl quote::ToTokens for RpcArg {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let name = &self.ident;
        let ty = &self.type_path.get_ident().unwrap();

        let gen = quote_spanned!(name.span()=> #name: #ty);
        gen.to_tokens(tokens);
    }
}

struct RpcParamGetter {
    arg: RpcArg,
    get_method: syn::Ident,
}

impl RpcParamGetter {
    fn new(arg: RpcArg) -> RpcParamGetter {
        RpcParamGetter {
            get_method: format_ident!("get_{}", arg.ident),
            arg,
        }
    }

    fn primitive_to_tokens(&self) -> TokenStream {
        let param_name = &self.arg.ident;
        let get_method = &self.get_method;
        quote! {
            let #param_name = params_reader.#get_method();
        }
    }

    fn custom_to_tokens(&self) -> TokenStream {
        let param_name = &self.arg.ident;
        let param_type = &self.arg.type_path.get_ident().unwrap();
        let get_method = &self.get_method;

        quote_spanned! {param_name.span()=>
            // TODO: Replace trait call with qualified call (FromCapnp::from_capnp)
            let #param_name = #param_type::from_capnp(&params_reader.#get_method()?)?;
        }
    }
}

impl quote::ToTokens for RpcParamGetter {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let param_type_path = &self.arg.type_path;

        let gen = if is_primitive(param_type_path) {
            self.primitive_to_tokens()
        }
        else {
            self.custom_to_tokens()
        };

        gen.to_tokens(tokens);
    }
}

struct RpcResponseSetter {
    return_type: syn::ReturnType,
}

impl RpcResponseSetter {
    fn new(return_type: syn::ReturnType) -> Self {
        Self { return_type }
    }
}

impl quote::ToTokens for RpcResponseSetter {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        if let syn::ReturnType::Default = self.return_type {
            return;
        }

        let gen = quote! {
            result.to_capnp(&mut results.get());
        };

        gen.to_tokens(tokens);
    }
}

struct WrappedMethod {
    module_path: Vec<syn::Ident>,
    ident: syn::Ident,
    block: syn::Block,
    args: Vec<RpcArg>,
    return_type: syn::ReturnType,
}

impl WrappedMethod {
    fn new(module_path: Vec<syn::Ident>, method: &syn::ImplItemMethod) -> Self {
        WrappedMethod {
            module_path,
            ident: method.sig.ident.clone(),
            block: method.block.clone(),
            args: extract_method_args(&method.sig),
            return_type: method.sig.output.clone(),
        }
    }

    fn arg_names(&self) -> Vec<syn::Ident> {
        self.args.iter().map(|arg| arg.ident.clone()).collect()
    }

    fn arg_getters(&self) -> Vec<RpcParamGetter> {
        self.args.iter().filter_map(|arg| {
            if arg.ident != "ctx" {
                Some(RpcParamGetter::new(arg.clone()))
            } else {
                None
            }
        }).collect()
    }

    fn response_setter(&self) -> RpcResponseSetter {
        RpcResponseSetter::new(self.return_type.clone())
    }
}

impl quote::ToTokens for WrappedMethod {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let module_path = &self.module_path;
        let method_name = &self.ident;
        let block = &self.block;
        let params = format_ident!("{}Params", method_name.to_string().to_camel_case());
        let results = format_ident!("{}Results", method_name.to_string().to_camel_case());

        let args = &self.args;
        let arg_names = &self.arg_names();
        let arg_getters = &self.arg_getters();

        let response_setter = &self.response_setter();
        let method_ret = &self.return_type;

        let gen = quote! {
            fn #method_name(&mut self, params: #(#module_path)*::#params, mut results: #(#module_path)*::#results)
            -> ::capnp::capability::Promise<(), ::capnp::Error>
            {
                let ctx = self.create_context();

                ::capnp::capability::Promise::from_future(async move {
                    let params_reader = params.get()?;
                    #(#arg_getters)*

                    async fn #method_name(#(#args),*) #method_ret
                    #block

                    let result = #method_name(#(#arg_names),*).await;
                    #response_setter

                    ::std::result::Result::Ok(())
                })
            }
        };

        gen.to_tokens(tokens);
    }
}


pub fn wrap_rpc_server_traits(item: &syn::Item) -> TokenStream {
    let trait_impl = match item {
        syn::Item::Impl(item_impl) => item_impl,
        _ => panic!("rpc_server can only wrap a capnp server trait impl")
    };

    let (_, trait_path, _) = trait_impl.trait_.as_ref().expect("trait must implement a capnp server");
    let server_type = &trait_impl.self_ty;
    let module_path = extract_module_path(trait_path);

    let methods = extract_methods(&trait_impl);
    let wrapped_methods = methods.iter().map(|method| WrappedMethod::new(module_path.clone(), method));

    quote! {
        impl #trait_path for #server_type {
            #(#wrapped_methods)*
        }
    }
}


fn extract_module_path(trait_path: &syn::Path) -> Vec<syn::Ident> {
    let path_idents = extract_path_idents(trait_path);
    path_idents.iter().filter(|ident| ident.to_string() != "Server").cloned().collect()
}

fn extract_path_idents(path: &syn::Path) -> Vec<syn::Ident> {
    path.segments.iter().map(|segment| { segment.ident.clone() }).collect()
}

fn extract_methods(item_impl: &syn::ItemImpl) -> Vec<syn::ImplItemMethod> {
    item_impl.items.iter().filter_map(|item| {
        match item {
            syn::ImplItem::Method(item_method) => Some(item_method.clone()),
            _ => None
        }
    }).collect()
}

fn extract_method_args(sig: &syn::Signature) -> Vec<RpcArg> {
    sig.inputs.iter().filter_map(|arg| {
        match arg {
            syn::FnArg::Receiver(_) => None,
            syn::FnArg::Typed(arg_type) => Some(extract_method_arg(arg_type)),
        }
    }).collect()
}

fn extract_method_arg(pat_type: &syn::PatType) -> RpcArg {
    let ident = match pat_type.pat.as_ref() {
        syn::Pat::Ident(pat_ident) => Some(pat_ident.ident.clone()),
        _ => None,
    };

    let path = match pat_type.ty.as_ref() {
        syn::Type::Path(type_path) => Some(type_path.path.clone()),
        _ => None,
    };

    RpcArg {
        ident: ident.expect("rpc method parameter must have a name"),
        type_path: path.expect("rpc method parameter must have a type"),
    }
}


fn is_primitive(path: &syn::Path) -> bool {
    path.is_ident("i8")
        || path.is_ident("u8")
        || path.is_ident("i16")
        || path.is_ident("u16")
        || path.is_ident("i32")
        || path.is_ident("u32")
        || path.is_ident("i64")
        || path.is_ident("u64")
        || path.is_ident("i128")
        || path.is_ident("u128")
        || path.is_ident("isize")
        || path.is_ident("usize")
}
