
extern crate proc_macro;

use proc_macro::TokenStream;
use syn;

mod derive;
mod rpc_server;
mod rpc_client;

use derive::{impl_capnp_traits, impl_from_capnp_trait, impl_to_capnp_trait};
use rpc_server::wrap_rpc_server_traits;
use rpc_client::impl_rpc_client_traits;


#[proc_macro_derive(CapnpStruct, attributes(CapnpStructModule))]
pub fn capnp_struct_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();
    impl_capnp_traits(&ast).into()
}

#[proc_macro_derive(CopyFromCapnp, attributes(CapnpStructModule, capnp_enum, capnp_name))]
pub fn from_capnp_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();
    impl_from_capnp_trait(&ast).into()
}

#[proc_macro_derive(CopyToCapnp, attributes(CapnpStructModule, capnp_enum, capnp_name))]
pub fn to_capnp_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();
    impl_to_capnp_trait(&ast).into()
}


#[proc_macro_attribute]
pub fn capnp_rpc_server(_attr: TokenStream, input: TokenStream) -> TokenStream {
    let item = syn::parse_macro_input!(input as syn::Item);
    wrap_rpc_server_traits(&item).into()
}


#[proc_macro_attribute]
pub fn capnp_rpc_client(attrs: TokenStream, input: TokenStream) -> TokenStream {
    let args = syn::parse_macro_input!(attrs as syn::AttributeArgs);
    let item = syn::parse_macro_input!(input as syn::Item);
    impl_rpc_client_traits(&args, &item).into()
}
