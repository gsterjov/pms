extern crate proc_macro;

use proc_macro2::TokenStream;
use proc_macro2;
use quote::{format_ident, quote, quote_spanned};
use syn;


#[derive(Debug)]
enum ListType {
    Vec,
    Vector,
}

#[derive(Debug)]
enum FieldType {
    Primitive,
    String,
    Option,
    Custom,
    Enum,
    List(ListType),
}

impl From<&syn::TypePath> for FieldType {
    fn from(type_path: &syn::TypePath) -> Self {
        if is_primitive(&type_path.path) {
            FieldType::Primitive
        }
        else if is_string(&type_path.path) {
            FieldType::String
        }
        else if is_option(&type_path.path) {
            FieldType::Option
        }
        else if is_vec(&type_path.path) {
            FieldType::List(ListType::Vec)
        }
        else if is_vector(&type_path.path) {
            FieldType::List(ListType::Vector)
        }
        else {
            FieldType::Custom
        }
    }
}


struct Setter {
    ident: syn::Ident,
    field_type: FieldType,
    capnp_name: String,
}

impl quote::ToTokens for Setter {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let field_name = &self.ident;
        let set_method = format_ident!("set_{}", self.capnp_name);
        let init_method = format_ident!("init_{}", self.capnp_name);

        let gen = match self.field_type {
            FieldType::Primitive => quote! {
                builder.#set_method(self.#field_name);
            },
            FieldType::String => quote! {
                builder.#set_method(&self.#field_name);
            },
            FieldType::Option => quote! {
                if let ::std::option::Option::Some(ref field) = self.#field_name {
                    field.to_capnp(&mut builder.reborrow().#init_method());
                    // builder.#set_method(::capnp_rpc::new_client(field.clone()));
                }
            },
            FieldType::Custom => quote! {
                self.#field_name.to_capnp(&mut builder.reborrow().#init_method());
            },
            FieldType::Enum => quote! {
                builder.#set_method(self.#field_name.clone().into());
            },
            FieldType::List(_) => quote! {
                {
                    // FIXME: typecasting to u32 can break. do a proper conversion or guard against it
                    let mut list = builder.reborrow().#init_method(self.#field_name.len() as u32);

                    for (i, item) in self.#field_name.iter().enumerate() {
                        item.to_capnp(&mut list.reborrow().get(i as u32));
                    }
                }
            }
        };
        gen.to_tokens(tokens);
    }
}


struct Getter {
    ident: syn::Ident,
    path: syn::Path,
    field_type: FieldType,
    get_method: syn::Ident,
}

impl Getter {
    fn new(ident: syn::Ident, path: syn::Path, field_type: FieldType, capnp_name: String) -> Getter {
        Getter {
            get_method: format_ident!("get_{}", capnp_name),
            ident,
            path,
            field_type,
        }
    }

    fn primitive_to_tokens(&self) -> TokenStream {
        let field_name = &self.ident;
        let get_method = &self.get_method;
        quote! {
            #field_name: reader.#get_method(),
        }
    }

    fn string_to_tokens(&self) -> TokenStream {
        let field_name = &self.ident;
        let get_method = &self.get_method;
        quote! {
            #field_name: reader.#get_method()?.to_string(),
        }
    }

    fn option_to_tokens(&self) -> TokenStream {
        let field_name = &self.ident;
        let path = &self.path;
        let get_method = &self.get_method;
        let has_method = format_ident!("has_{}", field_name);

        match &path.segments.first().unwrap().arguments {
            syn::PathArguments::AngleBracketed(generics) => {
                for arg in &generics.args {
                    if let syn::GenericArgument::Type(syn::Type::Path(ty)) = arg {
                        let path = &ty.path;
                        return quote! {
                            #field_name: if reader.#has_method() {
                                Some(#path::from_capnp(&reader.#get_method()?)?)
                            } else {
                                None
                            },
                        };
                    }
                }
            }
            _ => panic!("unsupported path arg")
        }

        quote! {
            #field_name: Some(#path::from_capnp(&reader.#get_method()?)?),
        }
    }

    fn custom_to_tokens(&self) -> TokenStream {
        let field_name = &self.ident;
        let path = &self.path;
        let get_method = &self.get_method;
        quote! {
            #field_name: #path::from_capnp(&reader.#get_method()?)?,
        }
    }

    fn enum_to_tokens(&self) -> TokenStream {
        let field_name = &self.ident;
        let get_method = &self.get_method;
        quote! {
            #field_name: reader.#get_method()?.into(),
        }
    }

    fn list_to_tokens(&self, list_type: &ListType) -> TokenStream {
        let field_name = &self.ident;
        let get_method = &self.get_method;
        let item_type = get_list_item_type(&self.path);
        let list_name = match list_type {
            ListType::Vec => format_ident!("Vec"),
            ListType::Vector => format_ident!("Vector"),
        };

        quote! {
            #field_name: {
                let mut list_items = Vec::new();
                for item in reader.#get_method()?.iter() {
                    list_items.push(#item_type::from_capnp(&item)?);
                }
                #list_name::from(list_items)
            },
        }
    }
}

impl quote::ToTokens for Getter {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let gen = match &self.field_type {
            FieldType::Primitive => self.primitive_to_tokens(),
            FieldType::String => self.string_to_tokens(),
            FieldType::Option => self.option_to_tokens(),
            FieldType::Custom => self.custom_to_tokens(),
            FieldType::Enum => self.enum_to_tokens(),
            FieldType::List(list_type) => self.list_to_tokens(&list_type),
        };
        gen.to_tokens(tokens);
    }
}



pub fn impl_from_capnp_trait(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let module_path = extract_module_path(&ast.attrs).expect("Invalid or missing CapnpStructModule attribute");

    let fields = match ast.data {
        syn::Data::Struct(ref data_struct) => &data_struct.fields,
        syn::Data::Enum(_) => panic!("Enums are not supported. Can only implement capnp traits for structs"),
        syn::Data::Union(_) => panic!("Unions are not supported. Can only implement capnp traits for structs"),
    };

    let getters: Vec<Getter> = fields.iter().map(|field| impl_getter(&field)).collect();

    quote_spanned! {name.span()=>
        impl ::protocols::FromCapnp<#module_path::Reader<'_>> for #name {
            fn from_capnp(reader: &#module_path::Reader) -> ::std::result::Result<Self, ::capnp::Error> {
                ::std::result::Result::Ok(Self {
                    #(#getters)*
                })
            }
        }
    }
}

pub fn impl_to_capnp_trait(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let module_path = extract_module_path(&ast.attrs).expect("Invalid or missing CapnpStructModule attribute");

    let fields = match ast.data {
        syn::Data::Struct(ref data_struct) => &data_struct.fields,
        syn::Data::Enum(_) => panic!("Enums are not supported. Can only implement capnp traits for structs"),
        syn::Data::Union(_) => panic!("Unions are not supported. Can only implement capnp traits for structs"),
    };

    let setters: Vec<Setter> = fields.iter().map(|field| impl_setter(&field)).collect();

    quote_spanned! {name.span()=>
        impl ::protocols::ToCapnp<#module_path::Builder<'_>> for #name {
            fn to_capnp(&self, builder: &mut #module_path::Builder) {
                #(#setters)*
            }
        }
    }
}


pub fn impl_capnp_traits(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let module_path = extract_module_path(&ast.attrs).expect("Invalid or missing CapnpStructModule attribute");

    let fields = match ast.data {
        syn::Data::Struct(ref data_struct) => &data_struct.fields,
        syn::Data::Enum(_) => panic!("Enums are not supported. Can only implement capnp traits for structs"),
        syn::Data::Union(_) => panic!("Unions are not supported. Can only implement capnp traits for structs"),
    };

    let setters: Vec<Setter> = fields.iter().map(|field| impl_setter(&field)).collect();
    let getters: Vec<Getter> = fields.iter().map(|field| impl_getter(&field)).collect();

    quote_spanned! {name.span()=>
        impl ::protocols::ToCapnp<#module_path::Builder<'_>> for #name {
            fn to_capnp(&self, builder: &mut #module_path::Builder) {
                #(#setters)*
            }
        }

        impl ::protocols::FromCapnp<#module_path::Reader<'_>> for #name {
            fn from_capnp(reader: &#module_path::Reader) -> ::std::result::Result<Self, ::capnp::Error> {
                ::std::result::Result::Ok(Self {
                    #(#getters)*
                })
            }
        }
    }
}


fn impl_setter(field: &syn::Field) -> Setter {
    let mut type_override = None;
    let mut name_override = None;

    for attr in &field.attrs {
        if attr.path.is_ident("capnp_enum") {
            type_override = Some(FieldType::Enum);
        }
        if attr.path.is_ident("capnp_name") {
            name_override = Some(get_name(attr).unwrap());
        }
    }

    match field.ty {
        syn::Type::Path(ref type_path) => {
            Setter {
                ident: field.ident.clone().unwrap(),
                field_type: type_override.unwrap_or_else(|| FieldType::from(type_path)),
                capnp_name: name_override.unwrap_or_else(|| field.ident.as_ref().unwrap().to_string()),
            }
        }
        _ => panic!("Unsupported type")
    }
}

fn impl_getter(field: &syn::Field) -> Getter {
    let mut type_override = None;
    let mut name_override = None;

    for attr in &field.attrs {
        if attr.path.is_ident("capnp_enum") {
            type_override = Some(FieldType::Enum);
        }
        if attr.path.is_ident("capnp_name") {
            name_override = Some(get_name(attr).unwrap());
        }
    }

    match field.ty {
        syn::Type::Path(ref type_path) => {
            let ident = field.ident.clone().unwrap();
            let path = type_path.path.clone();
            let ty = type_override.unwrap_or_else(|| FieldType::from(type_path));
            let capnp_name = name_override.unwrap_or_else(|| ident.to_string());

            Getter::new(ident, path, ty, capnp_name)
        }
        _ => panic!("Unsupported type")
    }
}


fn is_primitive(path: &syn::Path) -> bool {
    path.is_ident("i8")
        || path.is_ident("u8")
        || path.is_ident("i16")
        || path.is_ident("u16")
        || path.is_ident("i32")
        || path.is_ident("u32")
        || path.is_ident("i64")
        || path.is_ident("u64")
        || path.is_ident("i128")
        || path.is_ident("u128")
        || path.is_ident("isize")
        || path.is_ident("usize")
        || path.is_ident("bool")
}


fn is_string(path: &syn::Path) -> bool {
    path.is_ident("String")
}


fn is_option(path: &syn::Path) -> bool {
    let first = path.segments.first().unwrap();
    first.ident == "Option"
}

fn is_vec(path: &syn::Path) -> bool {
    let first = path.segments.first().unwrap();
    first.ident == "Vec"
}

fn is_vector(path: &syn::Path) -> bool {
    let first = path.segments.first().unwrap();
    first.ident == "Vector"
}


fn extract_module_path(attrs: &Vec<syn::Attribute>) -> syn::Result<syn::Path> {
    let attr = attrs.iter().find(|attr| attr.path.is_ident("CapnpStructModule"));

    match attr {
        Some(attr) => get_path(attr),
        None => {
            let message = "A CapnpStruct also must specify the generated capnp module containing the reader and builder";
            Err(syn::Error::new_spanned(attr, message))
        },
    }
}


fn get_path(attr: &syn::Attribute) -> syn::Result<syn::Path> {
    match attr.parse_meta()? {
        syn::Meta::NameValue(syn::MetaNameValue { lit: syn::Lit::Str(lit_str), .. }) => {
            lit_str.parse()
        }
        _ => {
            let message = "expected #[CapnpStructModule = \"path::to::mod_capnp\"]";
            Err(syn::Error::new_spanned(attr, message))
        }
    }
}

fn get_name(attr: &syn::Attribute) -> syn::Result<String> {
    match attr.parse_meta()? {
        syn::Meta::NameValue(syn::MetaNameValue { lit: syn::Lit::Str(lit_str), .. }) => {
            Ok(lit_str.value())
        }
        _ => {
            let message = "expected #[capnp_name = \"renamed_field\"]";
            Err(syn::Error::new_spanned(attr, message))
        }
    }
}


fn get_list_item_type(path: &syn::Path) -> syn::Path {
    match &path.segments.first().unwrap().arguments {
        syn::PathArguments::AngleBracketed(generics) => {
            if let syn::GenericArgument::Type(syn::Type::Path(ty)) = generics.args.first().unwrap() {
                ty.path.clone()
            } else {
                panic!("unsupported generic arg")
            }
        }
        _ => panic!("unsupported path arg")
    }
}
