use chrono::{Datelike, Timelike, NaiveDate, DateTime, Utc, Date, NaiveTime, NaiveDateTime};

use super::types_capnp::{date, utc_date_time};
use crate::{FromCapnp, ToCapnp};

impl Into<NaiveDate> for date::Reader<'_> {
    fn into(self) -> NaiveDate {
        chrono::NaiveDate::from_ymd(
            self.get_year().into(),
            self.get_month().into(),
            self.get_day().into(),
        )
    }
}

impl ToCapnp<date::Builder<'_>> for NaiveDate {
    fn to_capnp(&self, builder: &mut date::Builder) {
        // these should never overflow according to the docs
        builder.set_year(self.year() as u16);
        builder.set_month(self.month() as u8);
        builder.set_day(self.day() as u8);
    }
}

impl FromCapnp<date::Reader<'_>> for NaiveDate {
    fn from_capnp(reader: &date::Reader) -> Result<Self, capnp::Error> {
        Ok(NaiveDate::from_ymd(
            reader.get_year().into(),
            reader.get_month().into(),
            reader.get_day().into(),
        ))
    }
}



impl ToCapnp<date::Builder<'_>> for Date<Utc> {
    fn to_capnp(&self, builder: &mut date::Builder) {
        let naive_date = self.naive_utc();
        builder.set_year(naive_date.year() as u16);
        builder.set_month(naive_date.month() as u8);
        builder.set_day(naive_date.day() as u8);
    }
}

impl FromCapnp<date::Reader<'_>> for Date<Utc> {
    fn from_capnp(reader: &date::Reader) -> Result<Self, capnp::Error> {
        let naive_date = NaiveDate::from_ymd(
            reader.get_year().into(),
            reader.get_month().into(),
            reader.get_day().into()
        );
        Ok(Date::from_utc(naive_date, Utc))
    }
}


impl ToCapnp<utc_date_time::Builder<'_>> for DateTime<Utc> {
    fn to_capnp(&self, builder: &mut utc_date_time::Builder) {
        self.date().to_capnp(&mut builder.reborrow().init_date());
        builder.set_time(self.time().num_seconds_from_midnight());
        // let dt = DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(61, 0), Utc);
    }
}

impl FromCapnp<utc_date_time::Reader<'_>> for DateTime<Utc> {
    fn from_capnp(reader: &utc_date_time::Reader) -> Result<Self, capnp::Error> {
        let date = NaiveDate::from_capnp(&reader.get_date()?)?;
        let time = NaiveTime::from_num_seconds_from_midnight(reader.get_time(), 0);
        Ok(DateTime::from_utc(NaiveDateTime::new(date, time), Utc))
    }
}
