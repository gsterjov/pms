pub mod types_capnp;
pub mod auth_capnp;
pub mod patient_capnp;
pub mod visit_capnp;
pub mod clinical_capnp;
pub mod terminology_capnp;

pub mod types;


pub trait ToCapnp<Builder> {
    fn to_capnp(&self, builder: &mut Builder);
}

pub trait FromCapnp<Reader> {
    fn from_capnp(reader: &Reader) -> Result<Self, capnp::Error>
    where
        Self: Sized;
}
