@0xa97844bfb55a1bce;

using import "types.capnp".Date;


struct Finding {
  id            @0 :Int64;
  patientId     @1 :Int64;
  terminologyId @2 :Int64;
  description   @3 :Text;
  effectiveDate @4 :Date;

  struct FilterCriteria {
    visitId @0 :Int64;
  }

  struct FilterResults {
    findings @0 :List(Finding);
  }
}

interface FindingStore {
  create @0 (finding :Finding) -> Finding;
  read   @1 (id      :Int64)   -> Finding;
  update @2 (finding :Finding) -> Finding;
  delete @3 (id      :Int64)   -> Finding;

  filter @4 (criteria :Finding.FilterCriteria) -> Finding.FilterResults;
}
