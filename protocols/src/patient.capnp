@0xd625a70870ce81e1;

using import "types.capnp".Date;

struct Patient {
  id @0 :Int32;

  firstName     @1 :Text;
  lastName      @2 :Text;
  middleName    @3 :Text;
  preferredName @4 :Text;

  birthDate @5 :Date;
  birthSex  @6 :BirthSex;

  # gender_identity @7
  # pronoun @8

  enum BirthSex {
    undisclosed @0;
    intersex    @1;
    female      @2;
    male        @3;
  }
}


struct PhoneNumber {
  id        @0 :Int32;
  patientId @1: Int32;
  number    @2 :Text;
  type      @3 :Type;

  enum Type {
    mobile @0;
    home   @1;
    work   @2;
  }
}


struct Email {
  id        @0 :Int32;
  patientId @1 :Int32;
  address   @2 :Text;
}


struct Address {
  id        @0 :Int32;
  patientId @1 :Int32;

  line1     @2 :Text;
  line2     @3 :Text;
  suburb    @4 :Text;
  postcode  @5 :Text;
  type      @6 :Type;

  enum Type {
    residential @0;
    postal      @1;
  }
}


struct PatientSearchCriteria {
  name @0 :Text;
}

struct PatientSearchResults {
  patients @0 :List(Patient);
}


struct Contact {
  patientId    @0 :Int32;
  phoneNumbers @1 :List(PhoneNumber);
  emails       @2 :List(Email);
  addresses    @3 :List(Address);
}


interface PatientStore {
  create @0 (patient :Patient) -> ();
  read   @1 (id :Int32) -> Patient;
  update @2 (patient :Patient) -> ();
  delete @3 (id :Int32) -> ();

  search @4 (criteria :PatientSearchCriteria) -> PatientSearchResults;
}


interface ContactStore {
  create @0 (contact :Contact) -> ();
  read   @1 (patient_id :Int32) -> Contact;
  update @2 (contact :Contact) -> ();
  delete @3 (patient_id :Int32) -> ();
}


interface AddressStore {
  create @0 (address :Address) -> ();
  read   @1 (id :Int32) -> Address;
  update @2 (address :Address) -> ();
  delete @3 (id :Int32) -> ();
}
