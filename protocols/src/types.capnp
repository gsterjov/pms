@0xe665bbbef3816ff1;

struct Date {
  year  @0 :UInt16; # 2020
  month @1 :UInt8;  # 1-12
  day   @2 :UInt8;  # 1-30
}

struct UtcDateTime {
  date   @0 :Date;
  time   @1 :UInt32; # seconds
}
