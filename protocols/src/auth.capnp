@0xeb981e07fc213da3;

using Patient = import "patient.capnp";
using Visit = import "visit.capnp";
using Clinical = import "clinical.capnp";
using Terminology = import "terminology.capnp";

struct Capabilities {
  patients    @0 :Patient.PatientStore;
  contacts    @1 :Patient.ContactStore;
  addresses   @2 :Patient.AddressStore;

  visits        @3 :Visit.VisitStore;
  visitNotes    @4 :Visit.NoteStore;
  prescriptions @5 :Visit.PrescriptionStore;

  clincialFindings @6 :Clinical.FindingStore;

  terminologySearch @7 :Terminology.Search;
}

interface Auth {
  stores @0 () -> Capabilities;
}
