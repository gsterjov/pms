 @0xbb6e116aec7eb2d3;

struct Finding {
  id   @0 :Int64;
  term @1 :Text;

  struct SearchCriteria {
    query @0 :Text;
    refsetId @1 :Int64;
  }

  struct SearchResults {
    findings @0 :List(Finding);
  }
}

struct Medicine {
  id   @0 :Int64;
  term @1 :Text;

  struct SearchCriteria {
    query @0 :Text;
  }

  struct SearchResults {
    medicines @0 :List(Medicine);
  }
}

interface Search {
  findings  @0 (criteria :Finding.SearchCriteria)  -> Finding.SearchResults;
  medicines @1 (criteria :Medicine.SearchCriteria) -> Medicine.SearchResults;
}
