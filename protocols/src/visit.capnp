@0xbc8434a24bb63d28;

using import "types.capnp".UtcDateTime;
using import "types.capnp".Date;
using import "clinical.capnp".Finding;

struct Visit {
  id         @0 :Int64;
  patientId  @1 :Int32;
  started    @2 :UtcDateTime;
  ended      @3 :UtcDateTime;
  draft      @4 :Bool;

  struct SearchCriteria {
    patientId @0 :Int32;
    isDraft @1 :Bool;
  }

  struct SearchResults {
    visits @0 :List(Visit);
  }

  struct FindingResults {
    findings @0 :List(Finding);
  }
}

interface VisitStore {
  create @0 (visit :Visit) -> ();
  read   @1 (id    :Int64) -> Visit;
  update @2 (visit :Visit) -> ();
  delete @3 (id    :Int64) -> ();

  search @4 (criteria :Visit.SearchCriteria) -> Visit.SearchResults;

  getNotes @5 (visitId :Int64) -> Note.SearchResults;
  getPrescriptions @6 (visitId :Int64) -> Prescription.SearchResults;
  getClinicalFindings @7 (visitId :Int64) -> Visit.FindingResults;

  addClinicalFinding @8 (visitId :Int64, finding :Finding) -> Finding;
}


struct Note {
  id      @0 :Int64;
  visitId @1 :Int64;
  text    @2 :Text;

  struct SearchCriteria {
    visitId @0 :Int64;
  }

  struct SearchResults {
    notes @0 :List(Note);
  }
}

interface NoteStore {
  create @0 (note :Note)  -> Note;
  read   @1 (id   :Int64) -> Note;
  update @2 (note :Note)  -> ();
  delete @3 (id   :Int64) -> ();

  search @4 (criteria :Note.SearchCriteria) -> Note.SearchResults;
}


struct Prescription {
  id            @0 :Int64;
  visitId       @1 :Int64;
  terminologyId @2 :Int64;
  description   @3 :Text;
  effectiveDate @4 :Date;

  struct SearchCriteria {
    visitId @0 :Int64;
  }

  struct SearchResults {
    prescriptions @0 :List(Prescription);
  }
}

interface PrescriptionStore {
  create @0 (prescription :Prescription) -> Prescription;
  read   @1 (id           :Int64)        -> Prescription;
  update @2 (prescription :Prescription) -> Prescription;
  delete @3 (id           :Prescription) -> Prescription;

  search @4 (criteria :Prescription.SearchCriteria) -> Prescription.SearchResults;
}
