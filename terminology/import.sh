#!/bin/sh

for f in Refset/Content/der2_Refset_*.txt; do
    echo "\copy refset (id, effective_time, active, module_id, refset_id, referenced_component_id) FROM '$f' DELIMITER E'\t' QUOTE E'\b' CSV HEADER;"
done
