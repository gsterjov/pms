CREATE TABLE concepts (
  id                   bigint      NOT NULL,
  effective_time       timestamptz NOT NULL,
  active               boolean     NOT NULL,
  module_id            bigint      NOT NULL,
  definition_status_id bigint      NOT NULL
);



CREATE TABLE descriptions (
  id                   bigint      not null,
  effective_time       timestamptz not null,
  active               boolean     not null,
  module_id            bigint      not null,
  concept_id           bigint      not null,
  language_code        varchar     not null,
  type_id              bigint      not null,
  term                 text        not null,
  case_significance_id bigint      not null
);



CREATE TABLE identifiers (
  identifier_scheme_id    bigint      NOT NULL,
  alternative_identifier  varchar     NOT NULL,
  effective_time          timestamptz NOT NULL,
  active                  boolean     NOT NULL,
  module_id               bigint      NOT NULL,
  referenced_component_id bigint      NOT NULL
);



CREATE TABLE relationships (
  id                     bigint      NOT NULL,
  effective_time         timestamptz NOT NULL,
  active                 boolean     NOT NULL,
  module_id              bigint      NOT NULL,
  source_id              bigint      NOT NULL,
  destination_id         bigint      NOT NULL,
  relationship_group     bigint      NOT NULL,
  type_id                bigint      NOT NULL,
  characteristic_type_id bigint      NOT NULL,
  modifier_id            bigint      NOT NULL
);



CREATE TABLE language_refset (
  id                      uuid        NOT  NULL,
  effective_time          timestamptz NOT  NULL,
  active                  boolean     NOT  NULL,
  module_id               bigint      NOT  NULL,
  refset_id               bigint      NOT  NULL,
  referenced_component_id bigint      NOT  NULL,
  acceptability_id        bigint      NOT  NULL
);


CREATE TABLE refset (
  id                      uuid        NOT NULL,
  effective_time          timestamptz NOT NULL,
  active                  boolean     NOT NULL,
  module_id               bigint      NOT NULL,
  refset_id               bigint      NOT NULL,
  referenced_component_id bigint      NOT NULL
);



-- Views

-- join all terms with the language reference set and only select
-- terms acceptable for that language
CREATE MATERIALIZED VIEW preferred_descriptions AS
SELECT descriptions.* FROM descriptions
INNER JOIN language_refset AS lang ON lang.referenced_component_id = descriptions.id
WHERE lang.active = true
AND lang.acceptability_id = 900000000000548007;


-- only select the latest version of preferred terms
CREATE MATERIALIZED VIEW preferred_descriptions_latest AS
WITH partitioned AS (
  SELECT *,
  ROW_NUMBER() OVER (PARTITION BY id ORDER BY effective_time) AS row_num
  FROM preferred_descriptions
)
SELECT
  id,
  effective_time,
  active,
  module_id,
  concept_id,
  language_code,
  type_id,
  term,
  case_significance_id
FROM partitioned
WHERE row_num = 1;



-- select all the latest terms part of the clinical findings refset
CREATE MATERIALIZED VIEW clinical_findings_latest AS
SELECT preferred_descriptions_latest.* FROM preferred_descriptions_latest
INNER JOIN refset ON refset.referenced_component_id = preferred_descriptions_latest.concept_id
WHERE refset.active = true
AND refset.refset_id = 32570071000036102;




select preferred_descriptions_latest.id, term, preferred_descriptions_latest.module_id, concept_id, array_agg(refset_id)
from preferred_descriptions_latest
join refset on referenced_component_id=concept_id
where concept_id=291366007
group by preferred_descriptions_latest.id, term, preferred_descriptions_latest.module_id, concept_id;




-- select all latest refsets that can be used when limiting the
-- search for a specific term
CREATE VIEW active_refsets AS
SELECT
  concept_id AS refset_id,
  term
FROM preferred_descriptions_latest
WHERE active = true
AND concept_id IN (
  SELECT refset_id FROM refset
  WHERE active = true
  GROUP BY refset_id
);




-- copy the raw data into the database
\copy concepts (id, effective_time, active, module_id, definition_status_id) FROM 'sct2_Concept_Full_AU1000036_20201231.txt' DELIMITER E'\t' QUOTE E'\b' CSV HEADER;
\copy descriptions (id, effective_time, active, module_id, concept_id, language_code, type_id, term, case_significance_id) FROM './sct2_Description_Full-en-AU_AU1000036_20201231.txt' DELIMITER E'\t' QUOTE E'\b' CSV HEADER;
\copy relationships (id, effective_time, active, module_id, source_id, destination_id, relationship_group, type_id, characteristic_type_id, modifier_id) FROM 'sct2_Relationship_Full_AU1000036_20201231.txt' DELIMITER E'\t' QUOTE E'\b' CSV HEADER;

\copy language_refset (id, effective_time, active, module_id, refset_id, referenced_component_id, acceptability_id) FROM 'der2_cRefset_LanguageFull-en-AU_AU1000036_20201231.txt' DELIMITER E'\t' QUOTE E'\b' CSV HEADER;



-- refresh all views to make sure the latest data is cached
REFRESH MATERIALIZED VIEW preferred_descriptions;
REFRESH MATERIALIZED VIEW preferred_descriptions_latest;
REFRESH MATERIALIZED VIEW clinical_findings;
