
#[derive(Debug)]
pub enum TerminologyError {
    Index(tantivy::TantivyError),
}

impl From<tantivy::TantivyError> for TerminologyError {
    fn from(err: tantivy::TantivyError) -> Self {
        TerminologyError::Index(err)
    }
}


#[derive(Clone)]
pub struct Terminology {
    index: tantivy::Index,
    reader: tantivy::IndexReader,
}

impl Terminology {
    const CLINICAL_FINDING_REFSET_ID: i64 = 32570071000036102;
    const MEDICINES_REFSET_ID: i64 = 929360041000036105;

    pub fn open(path: &str) -> Result<Self, TerminologyError> {
        let index = tantivy::Index::open_in_dir(path)?;
        let reader = index.reader()?;

        let terminology = Self {
            index,
            reader,
        };
        Ok(terminology)
    }


    pub fn findings(&self, query: &str, limit: usize) -> Result<Vec<Doc>, TerminologyError> {
        self.search(query, limit, Self::CLINICAL_FINDING_REFSET_ID)
    }

    pub fn medicines(&self, query: &str, limit: usize) -> Result<Vec<Doc>, TerminologyError> {
        self.search(query, limit, Self::MEDICINES_REFSET_ID)
    }


    fn search(&self, query: &str, limit: usize, refset: i64) -> Result<Vec<Doc>, TerminologyError> {
        let searcher = self.reader.searcher();
        let concept_field = searcher.schema().get_field("concept_id").unwrap();
        let term_field = searcher.schema().get_field("term").unwrap();
        let refset_field = searcher.schema().get_field("refset_id").unwrap();

        let term = tantivy::Term::from_field_text(term_field, query);
        let refset = tantivy::Term::from_field_i64(refset_field, refset);

        let term_query = tantivy::query::FuzzyTermQuery::new(term, 1, true);
        let refset_query = tantivy::query::TermQuery::new(refset, tantivy::schema::IndexRecordOption::Basic);
        let query = tantivy::query::BooleanQuery::new(vec![
            (tantivy::query::Occur::Must, Box::new(refset_query)),
            (tantivy::query::Occur::Must, Box::new(term_query)),
        ]);
        let docs = searcher.search(&query, &tantivy::collector::TopDocs::with_limit(limit))?;

        let mut results = Vec::with_capacity(docs.len());
        for (_score, address) in docs {
            let doc = searcher.doc(address)?;

            results.push(Doc {
                id: doc.get_first(concept_field).unwrap().i64_value().unwrap(),
                term: doc.get_first(term_field).unwrap().text().unwrap().to_string(),
            });
        }

        Ok(results)
    }
}


#[derive(Debug)]
pub struct Doc {
    pub id: i64,
    pub term: String,
}
