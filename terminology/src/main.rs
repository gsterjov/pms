use std::env;
use futures_util::stream::TryStreamExt;

use tantivy::schema::*;
use tantivy::Index;
use tantivy::doc;

use sqlx::postgres::{PgPool, PgPoolOptions};


#[tokio::main(flavor = "current_thread")]
async fn main() -> tantivy::Result<()> {
    let pool = establish_db_connection()
        .await
        .expect("Failed to connect to database");

    index_all(&pool).await?;
    index_all_refsets(&pool).await?;

    Ok(())
}


async fn establish_db_connection() -> Result<PgPool, sqlx::Error> {
    let url = &env::var("DATABASE_URL").unwrap();
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&url)
        .await?;
    Ok(pool)
}


fn schema() -> Schema {
    let mut schema_builder = Schema::builder();
    schema_builder.add_text_field("term", TEXT | STORED);
    schema_builder.add_i64_field("concept_id", STORED);
    schema_builder.add_i64_field("type_id", STORED);

    schema_builder.add_date_field("effective_time", INDEXED);
    schema_builder.add_i64_field("refset_id", INDEXED);

    schema_builder.build()
}


fn create_index(schema: Schema) -> tantivy::Result<Index> {
    let index_path = "/home/gsterjov/devel/personal/data/terminology";
    Index::create_in_dir(&index_path, schema)
}

fn open_index() -> tantivy::Result<Index> {
    let index_path = "/home/gsterjov/devel/personal/data/terminology";
    Index::open_in_dir(index_path)
}


async fn index_all(pool: &PgPool) -> tantivy::Result<()> {
    let schema = schema();

    let index = create_index(schema.clone())?;
    let mut index_writer = index.writer(50_000_000)?;

    let term = schema.get_field("term").unwrap();
    let concept_id = schema.get_field("concept_id").unwrap();
    let type_id = schema.get_field("type_id").unwrap();
    let effective_time = schema.get_field("effective_time").unwrap();
    let refset_id = schema.get_field("refset_id").unwrap();


    let mut descriptions = sqlx::query!(
        "
            SELECT term, concept_id, type_id, effective_time
            FROM preferred_descriptions_latest
            WHERE active = true
        ")
        .fetch(pool);

    let mut count: usize = 0;

    while let Some(row) = descriptions.try_next().await.unwrap() {
        index_writer.add_document(doc!(
            term => row.term.unwrap(),
            concept_id => row.concept_id.unwrap(),
            type_id => row.type_id.unwrap(),
            effective_time => row.effective_time.unwrap(),
            refset_id => 0 as i64,
        ));
        count = count + 1;

        if count == 100_000 {
            println!("indexing 100,000 descriptions");
            index_writer.commit()?;
            count = 0;
        }
    }

    println!("indexing {} descriptions", count);
    index_writer.commit()?;
    Ok(())
}


async fn index_all_refsets(pool: &PgPool) -> tantivy::Result<()> {
    for id in refset_ids(pool).await {
        let name = sqlx::query!("
            SELECT term FROM preferred_descriptions_latest
            WHERE concept_id = $1
            AND active = true
            ORDER BY effective_time DESC
        ", id)
            .fetch_one(pool)
            .await.unwrap();

        println!("Indexing referense set '{}: {}", id, name.term.unwrap());
        index_refset(pool, id).await?;
    }

    Ok(())
}


async fn index_refset(pool: &PgPool, id: i64) -> tantivy::Result<()> {
    let schema = schema();

    let index = open_index()?;
    let mut index_writer = index.writer(50_000_000)?;

    let term = schema.get_field("term").unwrap();
    let concept_id = schema.get_field("concept_id").unwrap();
    let type_id = schema.get_field("type_id").unwrap();
    let effective_time = schema.get_field("effective_time").unwrap();
    let refset_id = schema.get_field("refset_id").unwrap();

    let mut descriptions = sqlx::query!(
        "
            SELECT term, concept_id, type_id, preferred_descriptions_latest.effective_time
            FROM preferred_descriptions_latest
            INNER JOIN refset ON concept_id = referenced_component_id
            WHERE preferred_descriptions_latest.active = true
            AND refset.active = true
            AND refset_id = $1
        ", id)
        .fetch(pool);

    let mut count: usize = 0;

    while let Some(row) = descriptions.try_next().await.unwrap() {
        index_writer.add_document(doc!(
            term => row.term.unwrap(),
            concept_id => row.concept_id.unwrap(),
            type_id => row.type_id.unwrap(),
            effective_time => row.effective_time.unwrap(),
            refset_id => id,
        ));
        count = count + 1;

        if count == 100_000 {
            println!("indexing 100,000 descriptions");
            index_writer.commit()?;
            count = 0;
        }
    }

    println!("indexing {} descriptions", count);
    index_writer.commit()?;
    Ok(())
}


async fn refset_ids(pool: &PgPool) -> Vec<i64> {
    let ids = sqlx::query!("SELECT refset_id FROM refset WHERE active = true GROUP BY refset_id")
        .fetch_all(pool)
        .await.unwrap();

    ids.iter().map(|row| row.refset_id).collect()
}
