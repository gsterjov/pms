CREATE TABLE clinical_findings (
  id                 bigint         primary key generated by default as identity,
  patient_id         bigint         references patients(id) not null,
  terminology_id     bigint         not null,
  description        varchar        not null,
  effective_date     date           not null
);

COMMENT ON COLUMN clinical_findings.terminology_id IS 'The ID of the selected item from the terminology server (the ID of the node in the SNOWMED dataset)';
COMMENT ON COLUMN clinical_findings.description IS 'The exact text given by the terminology server at the time of diagnoses';
COMMENT ON COLUMN clinical_findings.effective_date IS 'The date the clinical finding was created so that the correct historic data can be recreated with the terminology server';
