use std::env;

use sqlx::postgres::{PgPool, PgPoolOptions};
use tokio_util::compat::TokioAsyncReadCompatExt;

use tokio::net::{TcpListener, TcpStream};

use capnp_rpc::{rpc_twoparty_capnp::Side, twoparty::VatNetwork, RpcSystem};

use futures::{AsyncReadExt, FutureExt};

use crate::auth;
use protocols::auth_capnp::auth::Client as AuthRpcClient;

#[derive(Debug)]
pub enum Error {
    Network(std::io::Error),
    Database(sqlx::Error),
    Environment(env::VarError),
    Terminology(terminology::TerminologyError),
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Error::Network(err)
    }
}

impl From<sqlx::Error> for Error {
    fn from(err: sqlx::Error) -> Self {
        Error::Database(err)
    }
}

impl From<env::VarError> for Error {
    fn from(err: env::VarError) -> Self {
        Error::Environment(err)
    }
}

impl From<terminology::TerminologyError> for Error {
    fn from(err: terminology::TerminologyError) -> Self {
        Error::Terminology(err)
    }
}


struct Server {
    db_pool: sqlx::PgPool,
    terminology_index: terminology::Terminology,
}

impl Server {
    async fn connect() -> Self {
        let db_pool = establish_db_connection().await.expect("failed to connect to database");
        let terminology_index = terminology_index().expect("Failed to open terminology index");

        Self { db_pool, terminology_index }
    }
}

pub async fn run() {
    let server = Server::connect().await;

    tokio::task::LocalSet::new()
        .run_until(async move {
            listen(&server)
                .await
                .expect("Failed to bind or accept sockets");
        })
        .await;
}

async fn establish_db_connection() -> Result<PgPool, Error> {
    let url = &env::var("DATABASE_URL")?;
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&url)
        .await?;
    Ok(pool)
}

fn terminology_index() -> Result<terminology::Terminology, Error> {
    let path = "/home/gsterjov/devel/personal/data/terminology";
    let index = terminology::Terminology::open(path)?;
    Ok(index)
}

async fn listen(server: &Server) -> Result<(), Error> {
    let listener = TcpListener::bind("127.0.0.1:8080").await?;

    loop {
        let (socket, addr) = listener.accept().await?;
        println!("Received connection from {:?}", addr);

        socket.set_nodelay(true)?;
        handle_connection(socket, server.db_pool.clone(), server.terminology_index.clone()).await;
    }
}

async fn handle_connection(socket: TcpStream, db_pool: PgPool, terminology_index: terminology::Terminology) {
    let (reader, writer) = TokioAsyncReadCompatExt::compat(socket).split();
    let network = VatNetwork::new(reader, writer, Side::Server, Default::default());

    // the auth client is the entry point into the rpc world. from there clients
    // can get the capabilities sent to them via the auth interface and make calls
    // as if it were bootstrapped on the client side.
    //
    // this has the downside of the auth client needing to know how to construct
    // every single rpc object in the system.
    let auth: AuthRpcClient = capnp_rpc::new_client(auth::AuthRpc {
        db_pool,
        terminology_index,
    });
    let system = RpcSystem::new(Box::new(network), Some(auth.client));

    tokio::task::spawn_local(Box::pin(system.map(|_| ())));
}
