mod auth;
mod patients;
mod server;
mod visits;
mod clinical;
mod terminology;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    server::run().await;
}
