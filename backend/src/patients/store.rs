use sqlx;

use super::types::*;

#[derive(Debug)]
pub enum StoreError {
    Database(sqlx::Error),
}

impl From<sqlx::Error> for StoreError {
    fn from(err: sqlx::Error) -> Self {
        StoreError::Database(err)
    }
}

#[derive(Clone)]
pub struct PatientStore {
    pool: sqlx::PgPool,
}

impl PatientStore {
    pub fn new(pool: sqlx::PgPool) -> PatientStore {
        PatientStore { pool }
    }

    pub async fn create(&self, patient: Patient) -> Result<(), StoreError> {
        sqlx::query!(
            "
                INSERT INTO patients
                (first_name, last_name, middle_name, preferred_name)
                VALUES
                ($1, $2, $3, $4)
            ",
            patient.first_name,
            patient.last_name,
            patient.middle_name,
            patient.preferred_name
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }

    pub async fn read(&self, id: i32) -> Result<Patient, StoreError> {
        let patient = sqlx::query_as!(
            Patient,
            r#"
                SELECT
                    id, first_name, last_name, middle_name, preferred_name, birth_date, birth_sex as "birth_sex: BirthSex"
                FROM patients
                WHERE id = $1
            "#,
            id)
            .fetch_one(&self.pool)
            .await?;

        Ok(patient)
    }

    pub async fn update(&self, patient: Patient) -> Result<(), StoreError> {
        sqlx::query!(
            "
                UPDATE patients SET
                    first_name = $1,
                    last_name = $2,
                    middle_name = $3,
                    preferred_name = $4,

                    birth_date = $5
                WHERE id = $6
            ",
            patient.first_name,
            patient.last_name,
            patient.middle_name,
            patient.preferred_name,
            patient.birth_date,
            patient.id
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }

    pub async fn search(&self, name: &str) -> Result<Vec<Patient>, StoreError> {
        let patients = sqlx::query_as(
            "
            SELECT *
            FROM patients
            WHERE preferred_name ILIKE $1
            OR first_name ILIKE $1
            OR last_name ILIKE $1
            OR middle_name ILIKE $1
        ",
        )
        .bind(format!("%{}%", name))
        .fetch_all(&self.pool)
        .await?;

        Ok(patients)
    }
}

#[derive(Clone)]
pub struct ContactStore {
    pool: sqlx::PgPool,
}

impl ContactStore {
    pub fn new(pool: sqlx::PgPool) -> ContactStore {
        ContactStore { pool }
    }

    pub async fn create(&self, contact: Contact) -> Result<(), StoreError> {
        for phone_number in contact.phone_numbers {
            sqlx::query(
                "
                INSERT INTO phone_numbers
                (patient_id, number, type)
                VALUES
                ($1, $2, $3)
                ",
            )
            .bind(contact.patient_id)
            .bind(phone_number.number)
            .bind(phone_number.r#type)
            .execute(&self.pool)
            .await?;
        }

        for email in contact.emails {
            sqlx::query!(
                "
                INSERT INTO emails
                (patient_id, address)
                VALUES
                ($1, $2)
            ",
                contact.patient_id,
                email.address
            )
            .execute(&self.pool)
            .await?;
        }

        Ok(())
    }

    pub async fn read(&self, patient_id: i32) -> Result<Contact, StoreError> {
        let phone_numbers = sqlx::query_as!(
            PhoneNumber,
            r#"
            SELECT
              id, patient_id, number, type as "type: PhoneNumberType"
            FROM phone_numbers
            WHERE patient_id = $1"#,
            patient_id
        )
        .fetch_all(&self.pool)
        .await?;

        let emails = sqlx::query_as!(
            Email,
            "SELECT * FROM emails WHERE patient_id = $1",
            patient_id
        )
        .fetch_all(&self.pool)
        .await?;

        let addresses = sqlx::query_as!(
            Address,
            r#"
            SELECT
              id, patient_id, line_1, line_2, suburb, postcode, type as "type: AddressType"
            FROM addresses
            WHERE patient_id = $1"#,
            patient_id
        )
        .fetch_all(&self.pool)
        .await?;

        Ok(Contact {
            patient_id,
            phone_numbers,
            emails,
            addresses,
        })
    }

    pub async fn update(&self, contact: Contact) -> Result<(), StoreError> {
        // split out new and existing phone numbers
        let mut new_numbers = Vec::new();
        let mut existing_numbers = Vec::new();

        for phone_number in contact.phone_numbers.into_iter() {
            if phone_number.id == 0 {
                new_numbers.push(phone_number);
            } else {
                existing_numbers.push(phone_number);
            }
        }

        // split out new and existing emails
        let mut new_emails = Vec::new();
        let mut existing_emails = Vec::new();

        for email in contact.emails.into_iter() {
            if email.id == 0 {
                new_emails.push(email);
            } else {
                existing_emails.push(email);
            }
        }

        // create any records that don't have an id present
        self.create(Contact {
            patient_id: contact.patient_id,
            phone_numbers: new_numbers,
            emails: new_emails,
            addresses: Vec::new(),
        })
        .await?;

        // update records that do have an id present
        for phone_number in existing_numbers {
            sqlx::query(
                "
                UPDATE phone_numbers
                SET number = $1, type = $2
                WHERE id = $3
                ",
            )
            .bind(phone_number.number)
            .bind(phone_number.r#type)
            .bind(phone_number.id)
            .execute(&self.pool)
            .await?;
        }

        for email in existing_emails {
            sqlx::query!(
                "
                UPDATE emails
                SET address = $1
                WHERE id = $2
            ",
                email.address,
                email.id
            )
            .execute(&self.pool)
            .await?;
        }

        Ok(())
    }
}

#[derive(Clone)]
pub struct AddressStore {
    pool: sqlx::PgPool,
}

impl AddressStore {
    pub fn new(pool: sqlx::PgPool) -> Self {
        Self { pool }
    }

    pub async fn create(&self, address: Address) -> Result<(), StoreError> {
        sqlx::query(
            "
            INSERT INTO addresses
            (patient_id, line_1, line_2, suburb, postcode, type)
            VALUES
            ($1, $2, $3, $4, $5, $6)
        ",
        )
        .bind(address.patient_id)
        .bind(address.line_1)
        .bind(address.line_2)
        .bind(address.suburb)
        .bind(address.postcode)
        .bind(address.r#type)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    pub async fn read(&self, id: i32) -> Result<Address, StoreError> {
        let address = sqlx::query_as!(
            Address,
            r#"
            SELECT
              id, patient_id, line_1, line_2, suburb, postcode, type as "type: AddressType"
            FROM addresses
            WHERE id = $1"#,
            id
        )
        .fetch_one(&self.pool)
        .await?;

        Ok(address)
    }

    pub async fn update(&self, address: Address) -> Result<(), StoreError> {
        sqlx::query(
            "
            UPDATE addresses
            (patient_id, line_1, line_2, suburb, postcode, type)
            VALUES
            ($1, $2, $3, $4, $5, $6)
            WHERE id = $7
        ",
        )
        .bind(address.patient_id)
        .bind(address.line_1)
        .bind(address.line_2)
        .bind(address.suburb)
        .bind(address.postcode)
        .bind(address.r#type)
        .bind(address.id)
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    pub async fn delete(&self, id: i32) -> Result<(), StoreError> {
        sqlx::query!("DELETE FROM addresses WHERE id = $1", id)
            .execute(&self.pool)
            .await?;
        Ok(())
    }
}
