use chrono::Datelike;

use protocols::patient_capnp::{address, patient, phone_number};
use protocols::{FromCapnp, ToCapnp};
use protocols_derive::{CopyFromCapnp, CopyToCapnp};


#[derive(Debug, sqlx::Type)]
#[sqlx(type_name = "birth_sex", rename_all = "lowercase")]
pub enum BirthSex {
    Undisclosed,
    Intersex,
    Female,
    Male,
}

impl From<patient::BirthSex> for BirthSex {
    fn from(birth_sex: patient::BirthSex) -> Self {
        match birth_sex {
            patient::BirthSex::Undisclosed => BirthSex::Undisclosed,
            patient::BirthSex::Intersex => BirthSex::Intersex,
            patient::BirthSex::Female => BirthSex::Female,
            patient::BirthSex::Male => BirthSex::Male,
        }
    }
}

#[derive(Debug, Clone, sqlx::Type)]
#[sqlx(type_name = "phone_number_type", rename_all = "lowercase")]
pub enum PhoneNumberType {
    Mobile,
    Home,
    Work,
}

impl From<phone_number::Type> for PhoneNumberType {
    fn from(phone_number_type: phone_number::Type) -> Self {
        match phone_number_type {
            phone_number::Type::Mobile => PhoneNumberType::Mobile,
            phone_number::Type::Home => PhoneNumberType::Home,
            phone_number::Type::Work => PhoneNumberType::Work,
        }
    }
}

impl Into<phone_number::Type> for PhoneNumberType {
    fn into(self) -> phone_number::Type {
        match self {
            PhoneNumberType::Mobile => phone_number::Type::Mobile,
            PhoneNumberType::Home => phone_number::Type::Home,
            PhoneNumberType::Work => phone_number::Type::Work,
        }
    }
}

#[derive(Debug, sqlx::Type)]
#[sqlx(type_name = "address_type", rename_all = "lowercase")]
pub enum AddressType {
    Residential,
    Postal,
}

impl From<address::Type> for AddressType {
    fn from(address_type: address::Type) -> Self {
        match address_type {
            address::Type::Residential => AddressType::Residential,
            address::Type::Postal => AddressType::Postal,
        }
    }
}


#[derive(Debug, sqlx::FromRow)]
pub struct Patient {
    pub id: i32,

    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub middle_name: Option<String>,
    pub preferred_name: Option<String>,

    pub birth_date: Option<chrono::NaiveDate>,
    pub birth_sex: Option<BirthSex>,
}

impl FromCapnp<patient::Reader<'_>> for Patient {
    fn from_capnp(reader: &patient::Reader) -> Result<Self, capnp::Error> {
        Ok(Self {
            id: reader.get_id(),

            first_name: if reader.has_first_name() {
                Some(reader.get_first_name()?.into())
            } else {
                None
            },
            last_name: if reader.has_last_name() {
                Some(reader.get_last_name()?.into())
            } else {
                None
            },
            middle_name: if reader.has_middle_name() {
                Some(reader.get_middle_name()?.into())
            } else {
                None
            },
            preferred_name: if reader.has_preferred_name() {
                Some(reader.get_preferred_name()?.into())
            } else {
                None
            },

            birth_date: if reader.has_birth_date() {
                Some(reader.get_birth_date()?.into())
            } else {
                None
            },
            birth_sex: Some(reader.get_birth_sex()?.into()),
        })
    }
}

impl ToCapnp<patient::Builder<'_>> for Patient {
    fn to_capnp(&self, builder: &mut patient::Builder) {
        builder.set_id(self.id);

        if let Some(first_name) = &self.first_name {
            builder.set_first_name(&first_name);
        }
        if let Some(last_name) = &self.last_name {
            builder.set_last_name(&last_name);
        }
        if let Some(middle_name) = &self.middle_name {
            builder.set_middle_name(&middle_name);
        }
        if let Some(preferred_name) = &self.preferred_name {
            builder.set_preferred_name(&preferred_name);
        }

        if let Some(date) = &self.birth_date {
            let mut date_builder = builder.reborrow().init_birth_date();

            // these should never overflow according to the docs
            date_builder.set_year(date.year() as u16);
            date_builder.set_month(date.month() as u8);
            date_builder.set_day(date.day() as u8);
        }
    }
}

#[derive(Debug, sqlx::FromRow, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::patient_capnp::phone_number"]
pub struct PhoneNumber {
    pub id: i32,
    pub patient_id: i32,
    pub number: String,
    #[capnp_enum]
    #[capnp_name = "type"]
    pub r#type: PhoneNumberType,
}


#[derive(Debug, sqlx::FromRow, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::patient_capnp::email"]
pub struct Email {
    pub id: i32,
    pub patient_id: i32,
    pub address: String,
}


#[derive(Debug, sqlx::FromRow)]
pub struct Address {
    pub id: i32,
    pub patient_id: i32,

    pub line_1: Option<String>,
    pub line_2: Option<String>,
    pub suburb: Option<String>,
    pub postcode: Option<String>,
    pub r#type: AddressType,
}

impl FromCapnp<address::Reader<'_>> for Address {
    fn from_capnp(reader: &address::Reader) -> Result<Self, capnp::Error> {
        Ok(Self {
            id: reader.get_id(),
            patient_id: reader.get_patient_id(),

            line_1: if reader.has_line1() {
                Some(reader.get_line1()?.into())
            } else {
                None
            },
            line_2: if reader.has_line2() {
                Some(reader.get_line2()?.into())
            } else {
                None
            },
            suburb: if reader.has_suburb() {
                Some(reader.get_suburb()?.into())
            } else {
                None
            },
            postcode: if reader.has_postcode() {
                Some(reader.get_postcode()?.into())
            } else {
                None
            },
            r#type: reader.get_type()?.into(),
        })
    }
}

impl ToCapnp<address::Builder<'_>> for Address {
    fn to_capnp(&self, builder: &mut address::Builder) {
        builder.set_id(self.id);
        builder.set_patient_id(self.patient_id);

        if let Some(line1) = &self.line_1 {
            builder.set_line1(&line1);
        }
        if let Some(line2) = &self.line_2 {
            builder.set_line2(&line2);
        }
        if let Some(suburb) = &self.suburb {
            builder.set_suburb(&suburb);
        }
        if let Some(postcode) = &self.postcode {
            builder.set_postcode(&postcode);
        }
    }
}



#[derive(Debug, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::patient_capnp::contact"]
pub struct Contact {
    pub patient_id: i32,
    pub phone_numbers: Vec<PhoneNumber>,
    pub emails: Vec<Email>,
    pub addresses: Vec<Address>,
}
