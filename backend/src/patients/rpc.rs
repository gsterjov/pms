use protocols::patient_capnp::{address_store, contact_store, patient_store};
use protocols::{FromCapnp, ToCapnp};

use super::store::*;
use super::types::*;
use protocols_derive::{capnp_rpc_server, CopyFromCapnp, CopyToCapnp};


#[derive(Debug, CopyFromCapnp)]
#[CapnpStructModule = "protocols::patient_capnp::patient_search_criteria"]
pub struct PatientSearchCriteria {
    pub name: String,
}

#[derive(Debug, CopyToCapnp)]
#[CapnpStructModule = "protocols::patient_capnp::patient_search_results"]
pub struct PatientSearchResults {
    pub patients: Vec<Patient>,
}

#[derive(Clone)]
pub struct PatientStoreRpcServer {
    pub store: PatientStore,
}

impl PatientStoreRpcServer {
    pub fn new(pool: sqlx::PgPool) -> Self {
        Self { store: PatientStore::new(pool) }
    }

    fn create_context(&mut self) -> PatientStoreContext {
        PatientStoreContext { store: self.store.clone() }
    }
}

struct PatientStoreContext {
    store: PatientStore,
}

#[capnp_rpc_server]
impl patient_store::Server for PatientStoreRpcServer {
    async fn create(ctx: PatientStoreContext, patient: Patient) {
        ctx.store.create(patient).await.unwrap()
    }

    async fn read(ctx: PatientStoreContext, id: i32) -> Patient {
        ctx.store.read(id).await.unwrap()
    }

    async fn update(ctx: PatientStoreContext, patient: Patient) {
        ctx.store.update(patient).await.unwrap()
    }

    async fn search(ctx: PatientStoreContext, criteria: PatientSearchCriteria) -> PatientSearchResults {
        let patients = ctx.store.search(&criteria.name).await.unwrap();
        PatientSearchResults { patients }
    }
}



#[derive(Clone)]
pub struct ContactStoreRpcServer {
    pub store: ContactStore,
}

impl ContactStoreRpcServer {
    pub fn new(pool: sqlx::PgPool) -> Self {
        Self { store: ContactStore::new(pool) }
    }

    fn create_context(&mut self) -> ContactStoreContext {
        ContactStoreContext { store: self.store.clone() }
    }
}

struct ContactStoreContext {
    store: ContactStore,
}

#[capnp_rpc_server]
impl contact_store::Server for ContactStoreRpcServer {
    async fn read(ctx: ContactStoreContext, patient_id: i32) -> Contact {
        ctx.store.read(patient_id).await.unwrap()
    }

    async fn update(ctx: ContactStoreContext, contact: Contact) {
        ctx.store.update(contact).await.unwrap()
    }
}




#[derive(Clone)]
pub struct AddressStoreRpcServer {
    pub store: AddressStore,
}

impl AddressStoreRpcServer {
    pub fn new(pool: sqlx::PgPool) -> Self {
        Self { store: AddressStore::new(pool) }
    }

    fn create_context(&mut self) -> AddressStoreContext {
        AddressStoreContext { store: self.store.clone() }
    }
}

struct AddressStoreContext {
    store: AddressStore,
}

#[capnp_rpc_server]
impl address_store::Server for AddressStoreRpcServer {
    async fn create(ctx: AddressStoreContext, address: Address) {
        ctx.store.create(address).await.unwrap()
    }

    async fn read(ctx: AddressStoreContext, id: i32) -> Address {
        ctx.store.read(id).await.unwrap()
    }

    async fn update(ctx: AddressStoreContext, address: Address) {
        ctx.store.update(address).await.unwrap()
    }

    async fn delete(ctx: AddressStoreContext, id: i32) {
        ctx.store.delete(id).await.unwrap()
    }
}
