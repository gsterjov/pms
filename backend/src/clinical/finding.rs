use chrono::prelude::*;

use protocols::{FromCapnp, ToCapnp};
use protocols_derive::{capnp_rpc_server, CopyFromCapnp, CopyToCapnp};

use protocols::clinical_capnp::finding_store;


#[derive(Debug)]
pub enum StoreError {
    Database(sqlx::Error),
}

impl From<sqlx::Error> for StoreError {
    fn from(err: sqlx::Error) -> Self {
        StoreError::Database(err)
    }
}



#[derive(Debug, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::clinical_capnp::finding"]
pub struct Finding {
    pub id: i64,
    pub patient_id: i64,
    pub terminology_id: i64,
    pub description: String,
    pub effective_date: NaiveDate,
}


#[derive(Clone)]
pub struct FindingStore {
    pool: sqlx::PgPool,
}

impl FindingStore {
    pub fn new(pool: sqlx::PgPool) -> Self { Self { pool } }

    pub async fn create(&self, finding: &Finding) -> Result<Finding, StoreError> {
        let finding = sqlx::query_as!(
            Finding,
            "
                INSERT INTO clinical_findings
                (patient_id, terminology_id, description, effective_date)
                VALUES
                ($1, $2, $3, $4)
                RETURNING *
            ",
            finding.patient_id,
            finding.terminology_id,
            finding.description,
            finding.effective_date
        )
        .fetch_one(&self.pool)
        .await?;

        Ok(finding)
    }

    pub async fn delete(&self, id: i64) -> Result<Finding, StoreError> {
        let finding = sqlx::query_as!(
            Finding,
            "
                DELETE FROM clinical_findings
                WHERE id = $1
                RETURNING *
            ",
            id
        )
        .fetch_one(&self.pool)
        .await?;

        Ok(finding)
    }
}


#[derive(Clone)]
pub struct FindingStoreRpcServer {
    store: FindingStore,
}

impl FindingStoreRpcServer {
    pub fn new(db_pool: sqlx::PgPool) -> Self {
        Self { store: FindingStore::new(db_pool) }
    }

    fn create_context(&mut self) -> FindingStoreContext {
        FindingStoreContext { store: self.store.clone() }
    }
}

struct FindingStoreContext {
    store: FindingStore,
}


#[capnp_rpc_server]
impl finding_store::Server for FindingStoreRpcServer {
    async fn create(ctx: FindingStoreContext, finding: Finding) -> Finding {
        ctx.store.create(&finding).await.unwrap()
    }

    async fn delete(ctx: FindingStoreContext, id: i64) -> Finding {
        ctx.store.delete(id).await.unwrap()
    }
}
