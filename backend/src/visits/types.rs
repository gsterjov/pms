use chrono::prelude::*;

use protocols::{FromCapnp, ToCapnp};
use protocols_derive::{capnp_rpc_server, CopyFromCapnp, CopyToCapnp};

use protocols::visit_capnp::{note_store, visit_store, prescription_store};

use crate::clinical::finding::Finding;


type UtcDateTime = DateTime<Utc>;

#[derive(Debug)]
pub enum StoreError {
    Database(sqlx::Error),
}

impl From<sqlx::Error> for StoreError {
    fn from(err: sqlx::Error) -> Self {
        StoreError::Database(err)
    }
}


#[derive(Debug, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::visit"]
pub struct Visit {
    pub id: i64,
    pub patient_id: i32,
    pub started: UtcDateTime,
    pub ended: Option<UtcDateTime>,
    pub draft: bool,
}

#[derive(Debug, CopyFromCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::visit::search_criteria"]
pub struct VisitSearchCriteria {
    pub patient_id: i32,
    pub is_draft: bool,
}

#[derive(Debug, CopyToCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::visit::search_results"]
pub struct VisitSearchResults {
    pub visits: Vec<Visit>,
}

#[derive(Debug, CopyToCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::visit::finding_results"]
pub struct VisitFindingResults {
    pub findings: Vec<Finding>,
}


#[derive(Clone)]
pub struct VisitStore {
    pool: sqlx::PgPool,
}

impl VisitStore {
    pub fn new(pool: sqlx::PgPool) -> Self { Self { pool } }

    pub async fn create(&self, visit: &Visit) -> Result<(), StoreError> {
        sqlx::query!(
            "
                INSERT INTO visits
                (patient_id, started, ended, draft)
                VALUES
                ($1, $2, $3, $4)
            ",
            visit.patient_id,
            visit.started,
            visit.ended,
            visit.draft
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }

    pub async fn read(&self, id: i64) -> Result<Visit, StoreError> {
        let visit = sqlx::query_as!(Visit,
            "SELECT * FROM visits WHERE id = $1", id)
            .fetch_one(&self.pool)
            .await?;
        Ok(visit)
    }

    pub async fn search(&self, criteria: VisitSearchCriteria) -> Result<VisitSearchResults, StoreError> {
        let visits = sqlx::query_as!(Visit,
            "SELECT * FROM visits WHERE patient_id = $1",
            criteria.patient_id
        )
        .fetch_all(&self.pool)
        .await?;
        Ok(VisitSearchResults { visits })
    }

    pub async fn notes(&self, visit_id: i64) -> Result<NoteSearchResults, StoreError> {
        let notes = sqlx::query_as!(
            Note,
            "SELECT * FROM visit_notes WHERE visit_id = $1",
            visit_id
        ).fetch_all(&self.pool)
         .await?;

        Ok(NoteSearchResults { notes })
    }

    pub async fn prescriptions(&self, visit_id: i64) -> Result<PrescriptionSearchResults, StoreError> {
        let prescriptions = sqlx::query_as!(
            Prescription,
            "SELECT * FROM prescriptions WHERE visit_id = $1",
            visit_id
        ).fetch_all(&self.pool)
         .await?;

        Ok(PrescriptionSearchResults { prescriptions })
    }

    pub async fn clinical_findings(&self, visit_id: i64) -> Result<VisitFindingResults, StoreError> {
        let findings = sqlx::query_as!(
            Finding,
            "SELECT clinical_findings.* FROM clinical_findings WHERE patient_id = (SELECT patient_id FROM visits where id = $1)",
            visit_id
        ).fetch_all(&self.pool)
         .await?;

        Ok(VisitFindingResults { findings })
    }
}


#[derive(Clone)]
pub struct VisitStoreRpcServer {
    store: VisitStore,
}

impl VisitStoreRpcServer {
    pub fn new(db_pool: sqlx::PgPool) -> Self {
        Self { store: VisitStore::new(db_pool) }
    }

    fn create_context(&mut self) -> VisitStoreContext {
        VisitStoreContext { store: self.store.clone() }
    }
}

struct VisitStoreContext {
    store: VisitStore,
}


#[capnp_rpc_server]
impl visit_store::Server for VisitStoreRpcServer {
    async fn create(ctx: VisitStoreContext, visit: Visit) {
        if let Err(err) = ctx.store.create(&visit).await {
            println!("{:?}", err);
        }
    }

    async fn read(ctx: VisitStoreContext, id: i64) -> Visit {
        ctx.store.read(id).await.unwrap()
    }

    async fn search(ctx: VisitStoreContext, criteria: VisitSearchCriteria) -> VisitSearchResults {
        ctx.store.search(criteria).await.unwrap()
    }

    async fn get_notes(ctx: VisitStoreContext, visit_id: i64) -> NoteSearchResults {
        ctx.store.notes(visit_id).await.unwrap()
    }

    async fn get_prescriptions(ctx: VisitStoreContext, visit_id: i64) -> PrescriptionSearchResults {
        ctx.store.prescriptions(visit_id).await.unwrap()
    }

    fn get_clinical_findings(ctx: VisitStoreContext, visit_id: i64) -> VisitFindingResults {
        ctx.store.clinical_findings(visit_id).await.unwrap()
    }
}



#[derive(Debug, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::note"]
pub struct Note {
    pub id: i64,
    pub visit_id: i64,
    pub text: String,
}

#[derive(Debug, CopyFromCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::note::search_criteria"]
pub struct NoteSearchCriteria {
    pub visit_id: i64,
}

#[derive(Debug, CopyToCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::note::search_results"]
pub struct NoteSearchResults {
    pub notes: Vec<Note>,
}



#[derive(Clone)]
pub struct NoteStore {
    pool: sqlx::PgPool,
}

impl NoteStore {
    pub fn new(pool: sqlx::PgPool) -> Self { Self { pool } }

    pub async fn create(&self, note: &Note) -> Result<Note, StoreError> {
        let note = sqlx::query_as!(
            Note,
            "
                INSERT INTO visit_notes
                (visit_id, text)
                VALUES
                ($1, $2)
                RETURNING *
            ",
            note.visit_id,
            note.text
        )
        .fetch_one(&self.pool)
        .await?;

        Ok(note)
    }

    pub async fn read(&self, id: i64) -> Result<Note, StoreError> {
        let note = sqlx::query_as!(
            Note,
            "SELECT * FROM visit_notes WHERE id = $1",
            id
        ).fetch_one(&self.pool)
         .await?;

        Ok(note)
    }

    pub async fn update(&self, note: &Note) -> Result<(), StoreError> {
        sqlx::query!(
            "
                UPDATE visit_notes
                SET visit_id = $1, text = $2
                WHERE id = $3
            ",
            note.visit_id,
            note.text,
            note.id
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }

    pub async fn delete(&self, id: i64) -> Result<(), StoreError> {
        sqlx::query!("DELETE FROM visit_notes WHERE id = $1", id).execute(&self.pool).await?;
        Ok(())
    }
}


#[derive(Clone)]
pub struct NoteStoreRpcServer {
    store: NoteStore,
}

impl NoteStoreRpcServer {
    pub fn new(db_pool: sqlx::PgPool) -> Self {
        Self { store: NoteStore::new(db_pool) }
    }

    fn create_context(&mut self) -> NoteStoreContext {
        NoteStoreContext { store: self.store.clone() }
    }
}

struct NoteStoreContext {
    store: NoteStore,
}


#[capnp_rpc_server]
impl note_store::Server for NoteStoreRpcServer {
    async fn create(ctx: NoteStoreContext, note: Note) -> Note {
        ctx.store.create(&note).await.unwrap()
    }

    async fn read(ctx: NoteStoreContext, id: i64) -> Note {
        ctx.store.read(id).await.unwrap()
    }

    async fn update(ctx: NoteStoreContext, note: Note) {
        ctx.store.update(&note).await.unwrap()
    }

    async fn delete(ctx: NoteStoreContext, id: i64) {
        ctx.store.delete(id).await.unwrap()
    }
}



#[derive(Debug, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::prescription"]
pub struct Prescription {
    pub id: i64,
    pub visit_id: i64,
    pub terminology_id: i64,
    pub description: String,
    pub effective_date: NaiveDate,
}

#[derive(Debug, CopyFromCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::prescription::search_criteria"]
pub struct PrescriptionSearchCriteria {
    pub visit_id: i64,
}

#[derive(Debug, CopyToCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::prescription::search_results"]
pub struct PrescriptionSearchResults {
    pub prescriptions: Vec<Prescription>,
}

#[derive(Clone)]
pub struct PrescriptionStore {
    pool: sqlx::PgPool,
}

impl PrescriptionStore {
    pub fn new(pool: sqlx::PgPool) -> Self { Self { pool } }

    pub async fn create(&self, prescription: &Prescription) -> Result<Prescription, StoreError> {
        println!("{:?}", prescription);
        let prescription = sqlx::query_as!(
            Prescription,
            "
                INSERT INTO prescriptions
                (visit_id, terminology_id, description, effective_date)
                VALUES
                ($1, $2, $3, $4)
                RETURNING *
            ",
            prescription.visit_id,
            prescription.terminology_id,
            prescription.description,
            prescription.effective_date
        )
        .fetch_one(&self.pool)
        .await?;

        Ok(prescription)
    }
}


#[derive(Clone)]
pub struct PrescriptionStoreRpcServer {
    store: PrescriptionStore,
}

impl PrescriptionStoreRpcServer {
    pub fn new(db_pool: sqlx::PgPool) -> Self {
        Self { store: PrescriptionStore::new(db_pool) }
    }

    fn create_context(&mut self) -> PrescriptionStoreContext {
        PrescriptionStoreContext { store: self.store.clone() }
    }
}

struct PrescriptionStoreContext {
    store: PrescriptionStore,
}


#[capnp_rpc_server]
impl prescription_store::Server for PrescriptionStoreRpcServer {
    async fn create(ctx: PrescriptionStoreContext, prescription: Prescription) -> Prescription {
        ctx.store.create(&prescription).await.unwrap()
    }
}
