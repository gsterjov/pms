use protocols::{FromCapnp, ToCapnp};
use protocols_derive::{capnp_rpc_server, CopyFromCapnp, CopyToCapnp};

use protocols::terminology_capnp::search;


#[derive(Debug)]
pub enum SearchError {
    Terminology(terminology::TerminologyError),
}

impl From<terminology::TerminologyError> for SearchError {
    fn from(err: terminology::TerminologyError) -> Self {
        SearchError::Terminology(err)
    }
}


#[derive(Debug, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::terminology_capnp::finding"]
pub struct Finding {
    pub id: i64,
    pub term: String,
}

#[derive(Debug, CopyFromCapnp)]
#[CapnpStructModule = "protocols::terminology_capnp::finding::search_criteria"]
pub struct FindingSearchCriteria {
    pub query: String,
    pub refset_id: i64,
}

#[derive(Debug, CopyToCapnp)]
#[CapnpStructModule = "protocols::terminology_capnp::finding::search_results"]
pub struct FindingSearchResults {
    pub findings: Vec<Finding>,
}


#[derive(Debug, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::terminology_capnp::medicine"]
pub struct Medicine {
    pub id: i64,
    pub term: String,
}

#[derive(Debug, CopyFromCapnp)]
#[CapnpStructModule = "protocols::terminology_capnp::medicine::search_criteria"]
pub struct MedicineSearchCriteria {
    pub query: String,
}

#[derive(Debug, CopyToCapnp)]
#[CapnpStructModule = "protocols::terminology_capnp::medicine::search_results"]
pub struct MedicineSearchResults {
    pub medicines: Vec<Medicine>,
}


#[derive(Clone)]
pub struct Search {
    index: terminology::Terminology,
}

impl Search {
    pub fn new(index: terminology::Terminology) -> Self {
        Self { index }
    }

    pub async fn findings(&self, criteria: FindingSearchCriteria) -> Result<FindingSearchResults, SearchError> {
        let docs = self.index.findings(&criteria.query, 10)?;

        let mut findings = Vec::with_capacity(docs.len());
        for doc in docs {
            findings.push(Finding { id: doc.id, term: doc.term });
        }

        Ok(FindingSearchResults { findings })
    }


    pub async fn medicines(&self, criteria: MedicineSearchCriteria) -> Result<MedicineSearchResults, SearchError> {
        let docs = self.index.medicines(&criteria.query, 20)?;

        let mut medicines = Vec::with_capacity(docs.len());
        for doc in docs {
            medicines.push(Medicine { id: doc.id, term: doc.term });
        }

        Ok(MedicineSearchResults { medicines })
    }
}


#[derive(Clone)]
pub struct SearchRpcServer {
    search: Search,
}

impl SearchRpcServer {
    pub fn new(index: terminology::Terminology) -> Self {
        Self { search: Search::new(index) }
    }

    fn create_context(&mut self) -> SearchContext {
        SearchContext { search: self.search.clone() }
    }
}

struct SearchContext {
    search: Search,
}


#[capnp_rpc_server]
impl search::Server for SearchRpcServer {
    async fn findings(ctx: SearchContext, criteria: FindingSearchCriteria) -> FindingSearchResults {
        ctx.search.findings(criteria).await.unwrap()
    }

    async fn medicines(ctx: SearchContext, criteria: MedicineSearchCriteria) -> MedicineSearchResults {
        ctx.search.medicines(criteria).await.unwrap()
    }
}
