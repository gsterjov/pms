use sqlx::PgPool;

use capnp::capability::Promise;
use protocols::auth_capnp::auth;
use protocols::ToCapnp;
// use protocols_derive::{CopyToCapnp, CapnpStruct, capnp_rpc_server};

use crate::patients::rpc::{PatientStoreRpcServer, ContactStoreRpcServer, AddressStoreRpcServer};
use crate::visits::types::{NoteStoreRpcServer, VisitStoreRpcServer, PrescriptionStoreRpcServer};
use crate::{clinical, terminology};


pub struct AuthRpc {
    pub db_pool: PgPool,
    pub terminology_index: ::terminology::Terminology,
}

impl auth::Server for AuthRpc {
    fn stores(&mut self, _params: auth::StoresParams, mut response: auth::StoresResults) -> Promise<(), capnp::Error> {
        let capabilities = Capabilities {
            patients: Some(PatientStoreRpcServer::new(self.db_pool.clone())),
            contacts: Some(ContactStoreRpcServer::new(self.db_pool.clone())),
            addresses: Some(AddressStoreRpcServer::new(self.db_pool.clone())),

            visits: Some(VisitStoreRpcServer::new(self.db_pool.clone())),
            visit_notes: Some(NoteStoreRpcServer::new(self.db_pool.clone())),
            prescriptions: Some(PrescriptionStoreRpcServer::new(self.db_pool.clone())),

            clinical_findings: Some(clinical::FindingStoreRpcServer::new(self.db_pool.clone())),

            terminology_search: Some(terminology::SearchRpcServer::new(self.terminology_index.clone())),
        };

        let mut builder = response.get();
        capabilities.to_capnp(&mut builder);

        Promise::ok(())
    }
}


// #[derive(CopyToCapnp)]
// #[CapnpStructModule = "protocols::auth_capnp::capabilities"]
struct Capabilities {
    patients: Option<PatientStoreRpcServer>,
    contacts: Option<ContactStoreRpcServer>,
    addresses: Option<AddressStoreRpcServer>,

    visits: Option<VisitStoreRpcServer>,
    visit_notes: Option<NoteStoreRpcServer>,
    prescriptions: Option<PrescriptionStoreRpcServer>,

    clinical_findings: Option<clinical::FindingStoreRpcServer>,

    terminology_search: Option<terminology::SearchRpcServer>,
}

impl ToCapnp<protocols::auth_capnp::capabilities::Builder<'_>> for Capabilities {
    fn to_capnp(&self, builder: &mut protocols::auth_capnp::capabilities::Builder) {
        if let Some(patients) = self.patients.as_ref() {
            builder.set_patients(capnp_rpc::new_client(patients.clone()));
        }
        if let Some(contacts) = self.contacts.as_ref() {
            builder.set_contacts(capnp_rpc::new_client(contacts.clone()));
        }
        if let Some(addresses) = self.addresses.as_ref() {
            builder.set_addresses(capnp_rpc::new_client(addresses.clone()));
        }

        if let Some(visits) = self.visits.as_ref() {
            builder.set_visits(capnp_rpc::new_client(visits.clone()));
        }
        if let Some(notes) = self.visit_notes.as_ref() {
            builder.set_visit_notes(capnp_rpc::new_client(notes.clone()));
        }
        if let Some(prescriptions) = self.prescriptions.as_ref() {
            builder.set_prescriptions(capnp_rpc::new_client(prescriptions.clone()));
        }

        if let Some(clinical_findings) = self.clinical_findings.as_ref() {
            builder.set_clincial_findings(capnp_rpc::new_client(clinical_findings.clone()));
        }

        if let Some(terminology) = self.terminology_search.as_ref() {
            builder.set_terminology_search(capnp_rpc::new_client(terminology.clone()));
        }
    }
}

// // #[capnp_rpc_server]
// impl auth::Server for AuthRpc {
//     async fn stores() -> Result<Capabilities, capnp::Error> {
//         let patient_store = PatientStore::new(self.db_pool.clone());
//         let contact_store = ContactStore::new(self.db_pool.clone());

//         let capabilities = Capabilities {
//             patients: Some(PatientStoreRpc { store: patient_store }),
//             contacts: Some(ContactStoreRpc { store: contact_store }),
//             addresses: None,
//             visits: Some(VisitStoreRpc {}),
//         };
//     }
// }
