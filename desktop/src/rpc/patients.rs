use protocols::{FromCapnp, ToCapnp};
use protocols::patient_capnp::{patient, phone_number};
use protocols::patient_capnp::{address_store, contact_store, patient_store};
use protocols_derive::{CopyToCapnp, CopyFromCapnp, capnp_rpc_client};

use druid::{im::Vector, Data, Lens};

use crate::client::BackendError;
use super::types::NaiveDate;


#[derive(Debug, Clone, Data, PartialEq)]
pub enum BirthSex {
    Undisclosed,
    Intersex,
    Female,
    Male,
}


impl From<patient::BirthSex> for BirthSex {
    fn from(birth_sex: patient::BirthSex) -> Self {
        match birth_sex {
            patient::BirthSex::Undisclosed => BirthSex::Undisclosed,
            patient::BirthSex::Intersex => BirthSex::Intersex,
            patient::BirthSex::Female => BirthSex::Female,
            patient::BirthSex::Male => BirthSex::Male,
        }
    }
}

impl Into<patient::BirthSex> for BirthSex {
    fn into(self) -> patient::BirthSex {
        match self {
            BirthSex::Undisclosed => patient::BirthSex::Undisclosed,
            BirthSex::Intersex => patient::BirthSex::Intersex,
            BirthSex::Female => patient::BirthSex::Female,
            BirthSex::Male => patient::BirthSex::Male,
        }
    }
}

#[derive(Debug, Clone, Data, PartialEq)]
pub enum PhoneNumberType {
    Mobile,
    Home,
    Work,
}

impl Default for PhoneNumber {
    fn default() -> Self {
        Self {
            id: 0,
            number: "".to_string(),
            number_type: PhoneNumberType::Mobile,
        }
    }
}

impl From<phone_number::Type> for PhoneNumberType {
    fn from(phone_type: phone_number::Type) -> Self {
        match phone_type {
            phone_number::Type::Mobile => PhoneNumberType::Mobile,
            phone_number::Type::Home => PhoneNumberType::Home,
            phone_number::Type::Work => PhoneNumberType::Work,
        }
    }
}

impl Into<phone_number::Type> for PhoneNumberType {
    fn into(self) -> phone_number::Type {
        match self {
            PhoneNumberType::Mobile => phone_number::Type::Mobile,
            PhoneNumberType::Home => phone_number::Type::Home,
            PhoneNumberType::Work => phone_number::Type::Work,
        }
    }
}

#[derive(Debug, Default, Clone, Data, Lens, CopyToCapnp, CopyFromCapnp)]
#[CapnpStructModule = "protocols::patient_capnp::patient"]
pub struct Patient {
    pub id: i32,

    pub first_name: String,
    pub last_name: String,
    pub middle_name: String,
    pub preferred_name: String,

    pub birth_date: Option<NaiveDate>,
    // #[capnp_enum]
    // pub birth_sex: Option<BirthSex>,
}


#[derive(Debug, Clone, Data, Lens, Default, CopyToCapnp, CopyFromCapnp)]
#[CapnpStructModule = "protocols::patient_capnp::email"]
pub struct Email {
    pub id: i32,
    pub address: String,
}


#[derive(Debug, Clone, Data, Lens, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::patient_capnp::phone_number"]
pub struct PhoneNumber {
    pub id: i32,
    pub number: String,
    #[capnp_enum]
    #[capnp_name = "type"]
    pub number_type: PhoneNumberType,
}

#[derive(Debug, Clone, Data, Lens, Default, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::patient_capnp::contact"]
pub struct Contact {
    pub patient_id: i32,
    pub phone_numbers: Vector<PhoneNumber>,
    pub emails: Vector<Email>,
}




#[derive(CopyToCapnp)]
#[CapnpStructModule = "protocols::patient_capnp::patient_search_criteria"]
pub struct PatientSearchCriteria {
    name: String,
}

#[derive(CopyFromCapnp)]
#[CapnpStructModule = "protocols::patient_capnp::patient_search_results"]
pub struct PatientSearchResults {
    patients: Vec<Patient>,
}


#[derive(Clone)]
pub struct PatientStore {
    client: patient_store::Client,
}

impl PatientStore {
    pub fn new(client: patient_store::Client) -> PatientStore {
        PatientStore { client }
    }

    pub async fn find(&mut self, name: &str) -> Result<Vec<Patient>, BackendError> {
        let results = self.search(&PatientSearchCriteria { name: String::from(name) }).await?;
        Ok(results.patients)
    }
}

#[capnp_rpc_client("protocols::patient_capnp::patient_store")]
pub trait RpcClient {
    async fn create(&mut self, patient: &Patient) -> Result<(), BackendError>;
    async fn read(&mut self, id: i32) -> Result<Patient, BackendError>;
    async fn update(&mut self, patient: &Patient) -> Result<(), BackendError>;
    async fn delete(&mut self, id: i32) -> Result<(), BackendError>;

    async fn search(&mut self, criteria: &PatientSearchCriteria) -> Result<PatientSearchResults, BackendError>;
}

impl RpcClient for PatientStore {
    fn rpc_client(&self) -> &patient_store::Client { &self.client }
}



pub struct ContactStore {
    client: contact_store::Client,
}

impl ContactStore {
    pub fn new(client: contact_store::Client) -> ContactStore {
        ContactStore { client }
    }
}

#[capnp_rpc_client("protocols::patient_capnp::contact_store")]
pub trait ContactStoreRpcClient {
    async fn create(&mut self, contact: &Contact) -> Result<(), BackendError>;
    async fn read(&mut self, patient_id: i32) -> Result<Contact, BackendError>;
    async fn update(&mut self, contact: &Contact) -> Result<(), BackendError>;
    async fn delete(&mut self, patient_id: i32) -> Result<(), BackendError>;
}

impl ContactStoreRpcClient for ContactStore {
    fn rpc_client(&self) -> &contact_store::Client { &self.client }
}


#[derive(Debug, Clone, Data, PartialEq)]
pub enum AddressType {
    Residential,
    Postal,
}

impl Default for AddressType {
    fn default() -> Self { AddressType::Residential }
}

impl From<protocols::patient_capnp::address::Type> for AddressType {
    fn from(address_type: protocols::patient_capnp::address::Type) -> Self {
        match address_type {
            protocols::patient_capnp::address::Type::Residential => AddressType::Residential,
            protocols::patient_capnp::address::Type::Postal => AddressType::Postal,
        }
    }
}

impl Into<protocols::patient_capnp::address::Type> for AddressType {
    fn into(self) -> protocols::patient_capnp::address::Type {
        match self {
            AddressType::Residential => protocols::patient_capnp::address::Type::Residential,
            AddressType::Postal => protocols::patient_capnp::address::Type::Postal,
        }
    }
}


#[derive(Debug, Clone, Data, Lens, Default, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::patient_capnp::address"]
pub struct Address {
    pub id: i32,
    pub patient_id: i32,
    pub line1: String,
    pub line2: String,
    pub suburb: String,
    pub postcode: String,
    #[capnp_enum]
    #[capnp_name = "type"]
    pub address_type: AddressType,
}


pub struct AddressStore {
    client: address_store::Client,
}

impl AddressStore {
    pub fn new(client: address_store::Client) -> AddressStore {
        AddressStore { client }
    }
}

#[capnp_rpc_client("protocols::patient_capnp::address_store")]
pub trait AddressStoreRpcClient {
    async fn create(&mut self, address: &Address) -> Result<(), BackendError>;
    async fn read(&mut self, id: i32) -> Result<Address, BackendError>;
    async fn update(&mut self, address: &Address) -> Result<(), BackendError>;
    async fn delete(&mut self, id: i32) -> Result<(), BackendError>;
}

impl AddressStoreRpcClient for AddressStore {
    fn rpc_client(&self) -> &address_store::Client { &self.client }
}
