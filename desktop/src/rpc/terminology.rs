use druid::{Data, Lens};

use protocols::terminology_capnp::search;
use protocols::{FromCapnp, ToCapnp};
use protocols_derive::{CopyFromCapnp, CopyToCapnp, capnp_rpc_client};

use crate::client::BackendError;


#[derive(Debug, Clone, CopyToCapnp)]
#[CapnpStructModule = "protocols::terminology_capnp::finding::search_criteria"]
pub struct FindingSearchCriteria {
    pub query: String,
    pub refset_id: i64,
}

#[derive(Debug, Clone, CopyFromCapnp)]
#[CapnpStructModule = "protocols::terminology_capnp::finding::search_results"]
pub struct FindingSearchResults {
    pub findings: Vec<Finding>,
}


#[derive(Debug, Default, Clone, Data, Lens, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::terminology_capnp::finding"]
pub struct Finding {
    pub id: i64,
    pub term: String,
}


#[derive(Debug, Clone, CopyToCapnp)]
#[CapnpStructModule = "protocols::terminology_capnp::medicine::search_criteria"]
pub struct MedicineSearchCriteria {
    pub query: String,
}

#[derive(Debug, Clone, CopyFromCapnp)]
#[CapnpStructModule = "protocols::terminology_capnp::medicine::search_results"]
pub struct MedicineSearchResults {
    pub medicines: Vec<Medicine>,
}


#[derive(Debug, Default, Clone, Data, Lens, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::terminology_capnp::medicine"]
pub struct Medicine {
    pub id: i64,
    pub term: String,
}


pub struct TerminologySearch {
    client: search::Client,
}

impl TerminologySearch {
    pub fn new(client: search::Client) -> Self {
        TerminologySearch { client }
    }
}

impl TerminologySearchRpcClient for TerminologySearch {
    fn rpc_client(&self) -> &search::Client { &self.client }
}

#[capnp_rpc_client("protocols::terminology_capnp::search")]
pub trait TerminologySearchRpcClient {
    async fn findings(&mut self, criteria: &FindingSearchCriteria) -> Result<FindingSearchResults, BackendError>;
    async fn medicines(&mut self, criteria: &MedicineSearchCriteria) -> Result<MedicineSearchResults, BackendError>;
}

