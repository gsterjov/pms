use druid::{Data, Lens};

use protocols::visit_capnp::{note_store, visit_store, prescription_store};
use protocols::{FromCapnp, ToCapnp};
use protocols_derive::{CopyFromCapnp, CopyToCapnp, capnp_rpc_client};

use crate::client::BackendError;
use super::types::{NaiveDate, UtcDateTime};
use super::clinical::Finding;


#[derive(Debug, Clone, CopyToCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::visit::search_criteria"]
pub struct VisitSearchCriteria {
    patient_id: i32,
}

#[derive(Debug, Clone, CopyFromCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::visit::search_results"]
pub struct VisitSearchResults {
    visits: Vec<Visit>,
}

#[derive(Debug, Clone, CopyFromCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::visit::finding_results"]
pub struct VisitFindingResults {
    findings: Vec<Finding>,
}


#[derive(Debug, Clone, Data, Lens, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::visit"]
pub struct Visit {
    pub id: i64,
    pub patient_id: i32,
    #[data(same_fn = "PartialEq::eq")]
    pub started: UtcDateTime,
    #[data(same_fn = "PartialEq::eq")]
    pub ended: Option<UtcDateTime>,
    pub draft: bool,
}

impl Default for Visit {
    fn default() -> Self {
        Visit {
            id: 0,
            patient_id: 0,
            started: chrono::Utc::now(),
            ended: None,
            draft: true,
        }
    }
}

pub struct VisitStore {
    client: visit_store::Client,
}

impl VisitStore {
    pub fn new(client: visit_store::Client) -> Self {
        VisitStore { client }
    }

    pub async fn find(&mut self, patient_id: i32) -> Result<Vec<Visit>, BackendError> {
        let results = self.search(&VisitSearchCriteria { patient_id }).await?;
        Ok(results.visits)
    }

    pub async fn notes(&mut self, visit_id: i64) -> Result<Vec<Note>, BackendError> {
        let results = self.get_notes(visit_id).await?;
        Ok(results.notes)
    }

    pub async fn prescriptions(&mut self, visit_id: i64) -> Result<Vec<Prescription>, BackendError> {
        let results = self.get_prescriptions(visit_id).await?;
        Ok(results.prescriptions)
    }

    pub async fn clinical_findings(&mut self, visit_id: i64) -> Result<Vec<Finding>, BackendError> {
        let results = self.get_clinical_findings(visit_id).await?;
        Ok(results.findings)
    }
}

impl VisitStoreRpcClient for VisitStore {
    fn rpc_client(&self) -> &visit_store::Client { &self.client }
}

#[capnp_rpc_client("protocols::visit_capnp::visit_store")]
pub trait VisitStoreRpcClient {
    async fn create(&mut self, visit: &Visit) -> Result<(), BackendError>;
    async fn read(&mut self, id: i64) -> Result<Visit, BackendError>;
    async fn update(&mut self, visit: &Visit) -> Result<(), BackendError>;
    async fn delete(&mut self, id: i64) -> Result<(), BackendError>;

    async fn search(&mut self, criteria: &VisitSearchCriteria) -> Result<VisitSearchResults, BackendError>;

    async fn get_notes(&mut self, visit_id: i64) -> Result<NoteSearchResults, BackendError>;
    async fn get_prescriptions(&mut self, visit_id: i64) -> Result<PrescriptionSearchResults, BackendError>;
    async fn get_clinical_findings(&mut self, visit_id: i64) -> Result<VisitFindingResults, BackendError>;
}


#[derive(Debug, Clone, CopyToCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::note::search_criteria"]
pub struct NoteSearchCriteria {
    visit_id: i64,
}

#[derive(Debug, Clone, CopyFromCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::note::search_results"]
pub struct NoteSearchResults {
    notes: Vec<Note>,
}

#[derive(Debug, Clone, Default, Data, Lens, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::note"]
pub struct Note {
    pub id: i64,
    pub visit_id: i64,
    pub text: String,
}

pub struct NoteStore {
    client: note_store::Client,
}

impl NoteStore {
    pub fn new(client: note_store::Client) -> Self {
        NoteStore { client }
    }
}

impl NoteStoreRpcClient for NoteStore {
    fn rpc_client(&self) -> &note_store::Client { &self.client }
}

#[capnp_rpc_client("note_store")]
pub trait NoteStoreRpcClient {
    async fn create(&mut self, note: &Note) -> Result<Note, BackendError>;
    async fn update(&mut self, note: &Note) -> Result<(), BackendError>;
}



#[derive(Debug, Clone, Data, Lens, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::prescription"]
pub struct Prescription {
    pub id: i64,
    pub visit_id: i64,
    pub terminology_id: i64,
    pub description: String,
    pub effective_date: NaiveDate,
}

#[derive(Debug, Clone, CopyToCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::prescription::search_criteria"]
pub struct PrescriptionSearchCriteria {
    visit_id: i64,
}

#[derive(Debug, Clone, CopyFromCapnp)]
#[CapnpStructModule = "protocols::visit_capnp::prescription::search_results"]
pub struct PrescriptionSearchResults {
    prescriptions: Vec<Prescription>,
}

pub struct PrescriptionStore {
    client: prescription_store::Client,
}

impl PrescriptionStore {
    pub fn new(client: prescription_store::Client) -> Self {
        PrescriptionStore { client }
    }
}

impl PrescriptionStoreRpcClient for PrescriptionStore {
    fn rpc_client(&self) -> &prescription_store::Client { &self.client }
}

#[capnp_rpc_client("prescription_store")]
pub trait PrescriptionStoreRpcClient {
    async fn create(&mut self, prescription: &Prescription) -> Result<Prescription, BackendError>;
    async fn update(&mut self, prescription: &Prescription) -> Result<Prescription, BackendError>;
}
