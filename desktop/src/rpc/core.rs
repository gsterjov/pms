use protocols::auth_capnp::auth;
use crate::client::BackendError;

use super::{patients, visits, terminology, clinical};


pub struct Capabilities {
    pub patient_store: Option<patients::PatientStore>,
    pub contact_store: Option<patients::ContactStore>,
    pub address_store: Option<patients::AddressStore>,

    pub visit_store: Option<visits::VisitStore>,
    pub visit_note_store: Option<visits::NoteStore>,
    pub prescription_store: Option<visits::PrescriptionStore>,

    pub finding_store: Option<clinical::FindingStore>,

    pub terminology_search: Option<terminology::TerminologySearch>,
}

pub struct Auth {
    client: auth::Client,
}

impl Auth {
    pub fn new(client: auth::Client) -> Auth {
        Auth { client }
    }

    pub async fn stores(&mut self) -> Result<Capabilities, BackendError> {
        let request = self.client.stores_request();
        let response = request.send().promise.await?;

        let caps = response.get()?;
        let capabilities = Capabilities {
            patient_store: Some(patients::PatientStore::new(caps.get_patients()?)),
            contact_store: Some(patients::ContactStore::new(caps.get_contacts()?)),
            address_store: Some(patients::AddressStore::new(caps.get_addresses()?)),

            visit_store: Some(visits::VisitStore::new(caps.get_visits()?)),
            visit_note_store: Some(visits::NoteStore::new(caps.get_visit_notes()?)),
            prescription_store: Some(visits::PrescriptionStore::new(caps.get_prescriptions()?)),

            finding_store: Some(clinical::FindingStore::new(caps.get_clincial_findings()?)),

            terminology_search: Some(terminology::TerminologySearch::new(caps.get_terminology_search()?)),
        };

        Ok(capabilities)
    }
}
