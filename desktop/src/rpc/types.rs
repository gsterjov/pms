use druid::Data;

use protocols::{FromCapnp, ToCapnp};
use protocols::types_capnp::date;


pub type UtcDateTime = chrono::DateTime<chrono::Utc>;


#[derive(Clone, Debug)]
pub struct NaiveDate(chrono::NaiveDate);


impl From<chrono::NaiveDate> for NaiveDate {
    fn from(date: chrono::NaiveDate) -> Self {
        NaiveDate(date)
    }
}

impl std::ops::Deref for NaiveDate {
    type Target = chrono::NaiveDate;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Data for NaiveDate {
    fn same(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl FromCapnp<date::Reader<'_>> for NaiveDate {
    fn from_capnp(reader: &date::Reader) -> Result<Self, capnp::Error> {
        Ok(chrono::NaiveDate::from_capnp(reader)?.into())
    }
}

impl ToCapnp<date::Builder<'_>> for NaiveDate {
    fn to_capnp(&self, builder: &mut date::Builder) {
        self.0.to_capnp(builder)
    }
}
