use druid::{Data, Lens};

use protocols::clinical_capnp::finding_store;
use protocols::{FromCapnp, ToCapnp};
use protocols_derive::{CopyFromCapnp, CopyToCapnp, capnp_rpc_client};

use crate::client::BackendError;
use super::types::NaiveDate;


#[derive(Debug, Clone, Data, Lens, CopyFromCapnp, CopyToCapnp)]
#[CapnpStructModule = "protocols::clinical_capnp::finding"]
pub struct Finding {
    pub id: i64,
    pub patient_id: i64,
    pub terminology_id: i64,
    pub description: String,
    pub effective_date: NaiveDate,
}

pub struct FindingStore {
    client: finding_store::Client,
}

impl FindingStore {
    pub fn new(client: finding_store::Client) -> Self {
        FindingStore { client }
    }
}

impl FindingStoreRpcClient for FindingStore {
    fn rpc_client(&self) -> &finding_store::Client { &self.client }
}

#[capnp_rpc_client("protocols::clinical_capnp::finding_store")]
pub trait FindingStoreRpcClient {
    async fn create(&mut self, finding: &Finding) -> Result<Finding, BackendError>;
    async fn update(&mut self, finding: &Finding) -> Result<Finding, BackendError>;
    async fn delete(&mut self, id: i64) -> Result<Finding, BackendError>;
}
