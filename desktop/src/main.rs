use druid::{AppLauncher, PlatformError, WindowDesc};
use std::thread;

mod client;
mod rpc;
mod ui;

use crate::client::Reactor;
use crate::ui::core::root;


fn main() -> Result<(), PlatformError> {
    let window = WindowDesc::new(root::view()).title("PMS");
    let app = AppLauncher::with_window(window);
    let ui_handle = app.get_external_handle();

    let (reactor, sender) = Reactor::new(ui_handle);
    thread::spawn(move || {
        rpc(reactor);
    });

    app.launch(root::State::new(sender))
}

#[tokio::main]
async fn rpc(mut reactor: Reactor) {
    // the rpc system is not Send so we have to ensure it's executed
    // on the same thread by using a local set
    tokio::task::LocalSet::new()
        .run_until(async move {
            reactor.run().await.expect("RPC thread failed");
        })
        .await;
}
