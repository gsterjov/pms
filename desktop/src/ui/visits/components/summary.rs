use druid::*;
use druid::widget::*;
use druid::im::Vector;

use crate::rpc::*;
use crate::ui::common::*;
use crate::ui::common::widgets::*;


pub type RemoteNotes = RemoteData<Vector<Note>>;
pub type RemoteFindings = RemoteData<Vector<clinical::Finding>>;
pub type RemotePrescriptions = RemoteData<Vector<Prescription>>;

const LABEL_COLUMN_WIDTH: f64 = 80.0;


pub fn view() -> impl Widget<Visit> {
    Flex::column()
        .with_child(visit_info())
}


fn visit_info() -> impl Widget<Visit> {
    let started = Label::new(|data: &Visit, _env: &_| {
        data.started.format("%e %b %Y - %H:%M").to_string()
    });

    let ended = Label::new(|data: &Visit, _env: &_| {
        match data.ended {
            Some(date) => date.format("%e %b %Y - %H:%M").to_string(),
            None => "ongoing".into(),
        }
    });

    Flex::column()
        .with_child(Flex::row()
                    .with_child(Label::new("Started:").fix_width(LABEL_COLUMN_WIDTH))
                    .with_child(started.fix_width(120.0))
        )
        .with_child(Flex::row()
                    .with_child(Label::new("Ended:").fix_width(LABEL_COLUMN_WIDTH))
                    .with_child(ended.fix_width(120.0))
        )
}


pub fn notes() -> impl Widget<RemoteNotes> {
    let list = List::new(note_item);
    Loader::new(list)
}

pub fn note_item() -> impl Widget<Note> {
    Label::new(|text: &String, _env: &_| text.clone()).lens(Note::text)
}


pub fn findings() -> impl Widget<RemoteFindings> {
    let list = List::new(finding_item);
    Loader::new(list)
}

pub fn finding_item() -> impl Widget<clinical::Finding> {
    Label::new(|text: &String, _env: &_| text.clone()).lens(clinical::Finding::description)
}


pub fn prescriptions() -> impl Widget<RemotePrescriptions> {
    let list = List::new(prescription_item);
    Loader::new(list)
}

pub fn prescription_item() -> impl Widget<Prescription> {
    Label::new(|text: &String, _env: &_| text.clone()).lens(Prescription::description)
}
