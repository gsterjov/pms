use druid::*;
use druid::widget::*;
use druid::im::Vector;

use crate::rpc::*;
// use crate::client::*;

use crate::ui::common::*;
use crate::ui::common::controllers::*;


type RemoteNote = RemoteData<Note>;
type RemoteNoteVec = Vector<RemoteNote>;

pub const SAVE_NOTE: Selector<Note> = Selector::new("pms.visits.notes.note.save");


pub fn view() -> impl Widget<RemoteData<RemoteNoteVec>> {
    Either::new(
        |data: &RemoteData<RemoteNoteVec>, _env: &_| data.loading() || data.failed(),
        loading_panel(),
        note_list().lens(RemoteData::<RemoteNoteVec>::data),
    )
        // .controller(RemoteDataResponse::new(VISIT_NOTES_RESPONSE, |data: &mut RemoteData<RemoteNoteVec>, resp: &Response<NoteList>| {
        //     if let Ok(notes) = resp {
        //         let mut remote_notes = Vec::with_capacity(notes.len());
        //         for note in notes {
        //             remote_notes.push(RemoteData::new(note.clone()));
        //         }

        //         data.data = Vector::from(&remote_notes);
        //         data.state = RemoteState::Loaded;
        //     }
        // }))
}

pub fn loading_panel() -> impl Widget<RemoteData<RemoteNoteVec>> {
    let loading = Flex::row()
        .with_child(Label::new("Loading notes.."))
        // .with_child(Spinner::new())
        .center();

    let error = Label::new(|data: &String, _env: &_| format!("Error: {}", data))
        .with_text_color(Color::rgb(1.0, 0.25, 0.25))
        .lens(RemoteData::<RemoteNoteVec>::error)
        .center();

    Either::new(|data, _env| data.failed(), error, loading)
}

pub fn note_list() -> impl Widget<RemoteNoteVec> {
    let list = Scroll::new(List::new(editor))
        .vertical()
        .expand_width()
        .fix_height(500.0);

    Flex::column()
        .with_child(new_note_button())
        .with_spacer(2.0)
        .with_child(list)
}

pub fn editor() -> impl Widget<RemoteNote> {
    let note_text = TextBox::multiline()
        .expand_width()
        .padding(10.0)
        .lens(Note::text)
        .controller(DataChanged::new(SAVE_NOTE))
        .lens(RemoteNote::data);
        // .controller(RemoteDataController::new(NOTE_CREATE_RESPONSE))
        // .controller(RemoteDataResponse::new(NOTE_UPDATE_RESPONSE, |data: &mut RemoteNote, _resp: &Response<()>| {
        //     data.state = RemoteState::Loaded;
        // }));

    let save_status = Label::new(|state: &RemoteState, _env: &_| {
        match state {
            RemoteState::Unloaded => "Unsaved",
            RemoteState::Loading => "Saving",
            RemoteState::Loaded => "Saved",
            RemoteState::Failed => "Failed",
        }.to_string()
    }).lens(RemoteNote::state);

    Flex::column()
        .with_child(note_text)
        .with_child(save_status)
}

pub fn new_note_button() -> impl Widget<RemoteNoteVec> {
    Button::new("Add note").on_click(|_ctx, data: &mut RemoteNoteVec, _env| {
        data.push_front(RemoteData::new(Note::default()));
    })
}
