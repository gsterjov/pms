use druid::*;
use druid::widget::*;

use crate::rpc::*;


pub fn view() -> impl Widget<Patient> {
    Flex::row()
        .with_child(summary())
        .with_spacer(50.0)
        .with_child(action_bar())
}


fn summary() -> impl Widget<Patient> {
    let name = Label::new(|data: &Patient, _env: &_| {
        format!("{} {}", data.first_name, data.last_name)
    })
    .with_text_size(30.0);

    let dob = Label::new(|dob: &Patient, _env: &_| {
        let dob = match dob.birth_date {
            Some(ref date) => date.format("%d/%m/%Y").to_string(),
            None => "".to_string(),
        };
        format!("{}", dob)
    });

    Flex::column()
        .with_child(name)
        .with_child(dob)
}


fn action_bar() -> impl Widget<Patient> {
    Flex::row()
}
