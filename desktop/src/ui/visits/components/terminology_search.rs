use druid::*;
use druid::widget::*;

use crate::client::*;

use crate::ui::common::widgets::*;
use crate::ui::common::controllers::*;
use crate::client::terminology;


pub type RemoteTerminologyList: RemoteData<TerminologyList>;


pub struct TerminologySearch {
    query: String,
    results: RemoteTerminologyList,
}

impl TerminologySearch {
    pub fn new(client: Client) -> Self {
        TerminologySearch {
            query: "".into(),
            results: RemoteData::new(TerminologyList::new()),
            client
        }
    }
}


fn view() -> impl Widget<TerminologySearch> {
    Flex::column()
        .with_child(search_bar)
        .with_child(search_list)
}


fn search_bar() -> impl Widget<TerminologySearch> {
    TextBox::new()
        .controller(TextChanged::new(Duration::from_millis(10)))
        .lens(State::query)
}

fn search_list() -> impl Widget<TerminologySearch> {
    List::new(finding_item)
        .lens(TerminologySearch::results.then(RemoteTerminologyList::data))
}

fn search_item() -> impl Widget<crate::rpc::terminology::Finding> {
    let term = Label::new(|data: &crate::rpc::terminology::Finding, _env: &_| {
        format!("{}: {}", data.id, data.term)
    });

    let row = Flex::row()
        .cross_axis_alignment(CrossAxisAlignment::Center)
        .must_fill_main_axis(true)
        .with_child(term)
        .padding(10.0)
        .expand_width();

    ListItem::new(row)
}
