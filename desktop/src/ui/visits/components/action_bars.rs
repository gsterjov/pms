use druid::*;
use druid::widget::*;

use crate::rpc::*;
use crate::ui::core::router;


pub fn visit_summary_actions() -> impl Widget<Visit> {
    open_visit_button()
}

fn open_visit_button() -> impl Widget<Visit> {
    Button::new("Open Visit").on_click(|ctx: &mut EventCtx, visit: &mut Visit, _env: &Env| {
        let route = router::Route::EditVisit(visit.clone());
        ctx.submit_notification(router::ROUTER_CHANGE_VIEW.with(route));
    })
}
