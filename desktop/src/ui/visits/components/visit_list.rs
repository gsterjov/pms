use druid::*;
use druid::widget::*;
use druid::im::Vector;

use crate::rpc::*;
use crate::ui::common::widgets::*;


pub const VISIT_SELECTED: Selector<Visit> = Selector::new("pms.ui.visits.patient_visits.selected");


pub fn view() -> impl Widget<Vector<Visit>> {
    List::new(visit_item).with_spacing(5.0)
}


fn visit_item() -> impl Widget<Visit> {
    let started = Label::new(|item: &Visit, _env: &_| {
        item.started.format("%e %b %Y - %H:%M").to_string()
    });

    let row = Flex::row()
        .cross_axis_alignment(CrossAxisAlignment::Center)
        .must_fill_main_axis(true)
        .with_child(started)
        .padding(10.0)
        .expand_width();

    ListItem::new(row)
        .on_click(|ctx, visit: &mut Visit, _env| {
            ctx.submit_notification(VISIT_SELECTED.with(visit.clone()));
        })
}
