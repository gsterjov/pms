use druid::*;
use druid::widget::*;

use crate::rpc::*;
use crate::rpc::terminology::Medicine;

use crate::client::*;
use crate::client::terminology;

use crate::ui::common::*;
use crate::ui::common::widgets::*;
use crate::ui::common::controllers::*;


pub const ADD_MEDICINE: Selector<Medicine> = Selector::new("pms.backend.visits.prescriptions.add");

// type RemoteMedicineList = RemoteData<terminology::MedicineList>;
type RemotePrescriptionList = RemoteData<PrescriptionList>;


#[derive(Clone, Data, Lens, Debug)]
pub struct State {
    #[data(ignore)]
    client: Client,

    query: String,
    medicines: RemoteData<terminology::MedicineList>,

    visit: Visit,
    prescribed: RemotePrescriptionList,
}

impl State {
    pub fn new(client: Client) -> Self {
        Self {
            client,
            visit: Visit::default(),
            query: "".into(),
            medicines: RemoteData::new(terminology::MedicineList::new()),
            prescribed: RemoteData::new(PrescriptionList::new()),
        }
    }

    pub fn load(&mut self, visit: Visit) {
        self.prescribed.state = RemoteState::Loading;
        self.client.visit.prescriptions(visit.id).unwrap();

        self.visit = visit;
    }


    fn add_medicine(&mut self, medicine: Medicine) {
        let effective_date = chrono::Utc::now().date().naive_utc();

        self.client.prescription.create(Prescription {
            id: 0,
            visit_id: self.visit.id,
            terminology_id: medicine.id,
            description: medicine.term.clone(),
            effective_date: effective_date.into(),
        }).unwrap();
        self.client.visit.prescriptions(self.visit.id).unwrap();
    }
}


impl RemoteDataLoader for State {
    fn load_response(&mut self, response: &ClientResponse) {
        match response {
            ClientResponse::Terminology(SearchReply::Medicines(result)) => {
                self.medicines.update_result(result);
            }
            ClientResponse::Visit(VisitReply::Prescriptions(result)) => {
                self.prescribed.update_result(result);
            }
            _ => {}
        }
    }
}


pub fn view() -> impl Widget<State> {
    let prescription_list = prescription_list()
        .padding(10.0)
        .align_left()
        .lens(State::prescribed);

    let search = terminology_search()
        .fix_width(500.0)
        .center()
        .controller(FilterController::new(|_ctx: &mut EventCtx, data: &mut State| {
            data.medicines.state = RemoteState::Loading;
            data.client.terminology.medicines(data.query.clone()).unwrap();
        }));

    let search_results = terminology_list()
        .lens(State::medicines)
        .controller(MedicineSelected {});

    let list_switcher = Either::new(
        |data: &State, _env: &Env| data.query.is_empty(),
        prescription_list,
        search_results
    );

    Flex::column()
        .with_child(search)
        .with_child(list_switcher)
        .controller(RemoteDataLoaderController::new(State::client))
}

fn prescription_list() -> impl Widget<RemotePrescriptionList> {
    List::new(prescription_item)
        .with_spacing(5.0)
        .lens(RemotePrescriptionList::data)
}

fn prescription_item() -> impl Widget<Prescription> {
    let desc = Label::new(|data: &Prescription, _env: &_| format!("{}", data.description));
    let id = Label::new(|data: &Prescription, _env: &_| format!("{}", data.terminology_id))
        .with_text_color(Color::GRAY)
        .with_text_size(12.0);

    Flex::column()
        .with_child(desc.align_left())
        .with_child(id.align_left())
}


fn terminology_search() -> impl Widget<State> {
    TextBox::new()
        .with_placeholder("Search for medicine to prescribe")
        .controller(TextChanged::new(|ctx: &mut EventCtx, _data: &mut String| {
            ctx.submit_notification(FILTER_CHANGED);
        }))
        .lens(State::query)
}

fn terminology_list() -> impl Widget<RemoteData<terminology::MedicineList>> {
    List::new(finding_item).lens(RemoteData::<terminology::MedicineList>::data)
}

fn finding_item() -> impl Widget<Medicine> {
    let term = Label::new(|data: &Medicine, _env: &_| {
        format!("{}: {}", data.id, data.term)
    });

    let row = Flex::row()
        .cross_axis_alignment(CrossAxisAlignment::Center)
        .must_fill_main_axis(true)
        .with_child(term)
        .padding(10.0)
        .expand_width();

    ListItem::new(row).on_click(|ctx: &mut EventCtx, medicine: &mut Medicine, _env: &Env| {
        ctx.submit_notification(ADD_MEDICINE.with(medicine.clone()));
    })
}


struct MedicineSelected;

impl<W: Widget<State>> Controller<State, W> for MedicineSelected {
    fn event(&mut self, child: &mut W, ctx: &mut EventCtx, event: &Event, data: &mut State, env: &Env) {
        match event {
            Event::Notification(notification) => {
                if let Some(medicine) = notification.get(ADD_MEDICINE) {
                    data.add_medicine(medicine.clone());
                    data.query.clear();
                    ctx.set_handled();
                }
            }
            _ => {}
        }

        child.event(ctx, event, data, env);
    }
}
