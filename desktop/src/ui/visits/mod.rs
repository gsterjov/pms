pub mod components;

pub mod visit;
pub mod diagnose;
pub mod prescriptions;
