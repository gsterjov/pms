use druid::*;
use druid::im::*;
use druid::widget::*;

use crate::rpc::*;
use crate::rpc::clinical::Finding as VisitFinding;
use crate::client::*;
use crate::client::terminology;

use crate::ui::common::*;
use crate::ui::common::widgets::*;
use crate::ui::common::controllers::*;


pub const ADD_FINDING: Selector<Finding> = Selector::new("pms.backend.visits.diagnose.add");
pub const REMOVE_FINDING: Selector<VisitFinding> = Selector::new("pms.backend.visits.diagnose.remove");


pub trait Editable {
    fn added(&self) -> bool;
    fn removed(&self) -> bool;
}


#[derive(Clone, Data, Debug)]
pub enum EditableListItem {
    Remote(VisitFinding),
    Added(VisitFinding),
    Removed(VisitFinding),
}

impl Editable for EditableListItem {
    fn added(&self) -> bool {
        match self {
            EditableListItem::Added(_) => true,
            _ => false,
        }
    }

    fn removed(&self) -> bool {
        match self {
            EditableListItem::Removed(_) => true,
            _ => false,
        }
    }
}


#[derive(Clone, Data, Lens, Debug)]
pub struct State {
    #[data(ignore)]
    client: Client,

    query: String,
    findings: RemoteData<terminology::FindingList>,

    clinical_findings: Vector<EditableListItem>,

    visit: Visit,
}

impl State {
    pub fn new(client: Client) -> Self {
        Self {
            client,
            visit: Visit::default(),
            query: "".into(),
            findings: RemoteData::new(terminology::FindingList::new()),
            clinical_findings: Vector::new(),
        }
    }


    pub fn load(&mut self, visit: Visit) {
        self.client.visit.clinical_findings(visit.id).unwrap();
        self.visit = visit;
    }


    fn add_finding(&mut self, finding: Finding) {
        let effective_date = chrono::Utc::now().date().naive_utc();
        let visit_finding = VisitFinding {
            id: 0,
            patient_id: self.visit.patient_id as i64,
            terminology_id: finding.id,
            description: finding.term.clone(),
            effective_date: effective_date.into(),
        };

        self.clinical_findings.push_back(EditableListItem::Added(visit_finding));
        self.sort_findings();
    }


    fn remove_finding(&mut self, finding: VisitFinding) {
        self.clinical_findings.retain(|record| {
            match record {
                EditableListItem::Remote(remote) => remote.id != finding.id,
                _ => true,
            }
        });
        self.clinical_findings.push_back(EditableListItem::Removed(finding));
        self.sort_findings();
    }


    fn sort_findings(&mut self) {
        self.clinical_findings.sort_by(|left, right| {
            match left {
                EditableListItem::Added(_) => match right {
                    EditableListItem::Added(_) => ::core::cmp::Ordering::Equal,
                    _ => ::core::cmp::Ordering::Less,
                },
                EditableListItem::Remote(_) => match right {
                    EditableListItem::Added(_) => ::core::cmp::Ordering::Greater,
                    EditableListItem::Remote(_) => ::core::cmp::Ordering::Equal,
                    EditableListItem::Removed(_) => ::core::cmp::Ordering::Less,
                },
                EditableListItem::Removed(_) => match right {
                    EditableListItem::Removed(_) => ::core::cmp::Ordering::Equal,
                    _ => ::core::cmp::Ordering::Greater,
                }
            }
        });

        self.clinical_findings = self.clinical_findings.clone();
    }


    fn save_changes(&mut self) {
        for item in self.clinical_findings.iter() {
            match item {
                EditableListItem::Added(finding) => {
                    self.client.finding.create(finding.clone()).unwrap();
                }
                EditableListItem::Removed(finding) => {
                    self.client.finding.delete(finding.id).unwrap();
                }
                EditableListItem::Remote(_) => {}
            }
        }

        self.client.visit.clinical_findings(self.visit.id).unwrap();
    }
}


impl RemoteDataLoader for State {
    fn load_response(&mut self, response: &ClientResponse) {
        match response {
            ClientResponse::Terminology(SearchReply::Findings(result)) => {
                self.findings.update_result(result);
            }
            ClientResponse::Visit(VisitReply::ClinicalFindings(result)) => {
                let mut clinical_findings = Vector::new();

                match result {
                    Ok(findings) => {
                        for item in findings.iter() {
                            clinical_findings.push_back(EditableListItem::Remote(item.clone()));
                        }
                    }
                    Err(_) => {}
                }

                self.clinical_findings = clinical_findings;
            }
            _ => {}
        }
    }
}


pub fn view() -> impl Widget<State> {
    let findings_list = finding_list()
        .padding(10.0)
        .align_left()
        .lens(State::clinical_findings);

    let search = terminology_search()
        .fix_width(500.0)
        .center()
        .controller(FilterController::new(|_ctx: &mut EventCtx, data: &mut State| {
            data.findings.state = RemoteState::Loading;
            data.client.terminology.findings(data.query.clone()).unwrap();
        }));

    let search_results = Either::new(
        |data: &RemoteData<terminology::FindingList>, _env: &Env| data.loading() || data.failed(),
        terminology_loading(),
        terminology_list()
    ).lens(State::findings);


    let list_switcher = Either::new(
        |data: &State, _env: &Env| data.query.is_empty(),
        findings_list,
        search_results,
    );

    Flex::column()
        .with_child(
            Flex::row()
                .with_child(search)
                .with_default_spacer()
                .with_child(save_button())
        )
        .with_child(list_switcher)
        .controller(FindingSelected {})
        .controller(FindingRemoved {})
        .controller(RemoteDataLoaderController::new(State::client))
}


fn save_button() -> impl Widget<State> {
    Button::new("Save Changes").on_click(|_ctx: &mut EventCtx, data: &mut State, _env: &Env| {
        data.save_changes();
    })
}


fn finding_list() -> impl Widget<Vector<EditableListItem>> {
    List::new(editable_finding_item)
}

fn editable_finding_item() -> impl Widget<EditableListItem> {
    let desc = Label::new(|data: &EditableListItem, _env: &_| {
        match data {
            EditableListItem::Remote(finding) => finding.description.clone(),
            EditableListItem::Added(finding) => format!("{}", finding.description),
            EditableListItem::Removed(finding) => format!("{}", finding.description),
        }
    });

    let id = Label::new(|data: &EditableListItem, _env: &_| {
        match data {
            EditableListItem::Remote(finding) => format!("{}", finding.terminology_id),
            EditableListItem::Added(finding) => format!("{}", finding.terminology_id),
            EditableListItem::Removed(finding) => format!("{}", finding.terminology_id),
        }
    })
        .with_text_color(Color::GRAY)
        .with_text_size(12.0);

    let delete_button = Button::new("X").on_click(|ctx: &mut EventCtx, data: &mut EditableListItem, _env: &Env| {
        match data {
            EditableListItem::Remote(finding) => ctx.submit_notification(REMOVE_FINDING.with(finding.clone())),
            _ => println!("not removable: {:?}", data),
        }
    });

    let item = Flex::column()
        .with_child(desc.align_left())
        .with_child(id.align_left());

    let item_row = Flex::row()
        .with_flex_child(item, 1.0)
        .with_child(delete_button)
        .expand_width()
        .padding(10.0);

    EditableItem::new(item_row)
}


fn terminology_search() -> impl Widget<State> {
    TextBox::new()
        .controller(TextChanged::new(|ctx: &mut EventCtx, _data: &mut String| {
            ctx.submit_notification(FILTER_CHANGED);
        }))
        .lens(State::query)
}

fn terminology_list() -> impl Widget<RemoteData<terminology::FindingList>> {
    List::new(finding_item).lens(RemoteData::<terminology::FindingList>::data)
}

fn finding_item() -> impl Widget<Finding> {
    let term = Label::new(|data: &Finding, _env: &_| {
        format!("{}: {}", data.id, data.term)
    });

    let row = Flex::row()
        .cross_axis_alignment(CrossAxisAlignment::Center)
        .must_fill_main_axis(true)
        .with_child(term)
        .padding(10.0)
        .expand_width();

    ListItem::new(row).on_click(|ctx: &mut EventCtx, finding: &mut Finding, _env: &Env| {
        ctx.submit_notification(ADD_FINDING.with(finding.clone()));
    })
}


fn terminology_loading() -> impl Widget<RemoteData<terminology::FindingList>> {
    let loading = Flex::row()
        .with_child(Label::new("Loading").with_text_size(30.0))
        .center();

    let error = Label::new(|data: &String, _env: &_| format!("Error: {}", data))
        .with_text_color(Color::RED)
        .lens(RemoteData::<terminology::FindingList>::error)
        .center();

    Either::new(|data, _env| data.failed(), error, loading)
}



struct FindingSelected;

impl<W: Widget<State>> Controller<State, W> for FindingSelected {
    fn event(&mut self, child: &mut W, ctx: &mut EventCtx, event: &Event, data: &mut State, env: &Env) {
        match event {
            Event::Notification(notification) => {
                if let Some(finding) = notification.get(ADD_FINDING) {
                    data.add_finding(finding.clone());
                    data.query.clear();
                    ctx.set_handled();
                }
            }
            _ => {}
        }

        child.event(ctx, event, data, env);
    }
}


struct FindingRemoved;

impl<W: Widget<State>> Controller<State, W> for FindingRemoved {
    fn event(&mut self, child: &mut W, ctx: &mut EventCtx, event: &Event, data: &mut State, env: &Env) {
        match event {
            Event::Notification(notification) => {
                if let Some(finding) = notification.get(REMOVE_FINDING) {
                    data.remove_finding(finding.clone());
                    ctx.set_handled();
                }
            }
            _ => {}
        }

        child.event(ctx, event, data, env);
    }
}



struct EditableItem<T: Data> {
    inner: WidgetPod<T, Container<T>>,
}

impl<T: Data> EditableItem<T> {
    pub fn new(inner: impl Widget<T> + 'static) -> Self {
        let container = Container::new(inner);
        Self {
            inner: WidgetPod::new(container),
        }
    }
}


impl<T: Data + Editable> Widget<T> for EditableItem<T> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        self.inner.event(ctx, event, data, env);
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, data: &T, env: &Env) {
        if let LifeCycle::WidgetAdded = event {
            if data.added() {
                self.inner.widget_mut().set_background(Color::rgb8(0, 60, 0));
                self.inner.widget_mut().set_border(Color::rgb8(0, 50, 0), 2.0);
            }
            else if data.removed() {
                self.inner.widget_mut().set_background(Color::rgb8(60, 0, 0));
                self.inner.widget_mut().set_border(Color::rgb8(50, 0, 0), 2.0);
            }
            else {
                self.inner.widget_mut().set_background(Color::from_rgba32_u32(env.get(theme::BACKGROUND_DARK).as_rgba_u32()));
                self.inner.widget_mut().set_border(Color::from_rgba32_u32(env.get(theme::BORDER_DARK).as_rgba_u32()), 2.0);
            }
        }

        self.inner.lifecycle(ctx, event, data, env);
    }

    fn update(&mut self, ctx: &mut UpdateCtx, _old_data: &T, data: &T, env: &Env) {
        if data.added() {
            self.inner.widget_mut().set_background(Color::rgb8(0, 60, 0));
            self.inner.widget_mut().set_border(Color::rgb8(0, 50, 0), 2.0);
        }
        else if data.removed() {
            self.inner.widget_mut().set_background(Color::rgb8(60, 0, 0));
            self.inner.widget_mut().set_border(Color::rgb8(50, 0, 0), 2.0);
        }
        else {
            self.inner.widget_mut().set_background(Color::from_rgba32_u32(env.get(theme::BACKGROUND_DARK).as_rgba_u32()));
            self.inner.widget_mut().set_border(Color::from_rgba32_u32(env.get(theme::BORDER_DARK).as_rgba_u32()), 2.0);
        }
        self.inner.update(ctx, data, env);
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &T, env: &Env) -> Size {
        self.inner.layout(ctx, bc, data, env)
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &T, env: &Env) {
        // let rect = ctx.size().to_rect();
        // let fill_rect = rect.inset(1.0);
        // ctx.with_save(|ctx| {
        //     ctx.stroke(rect, &env.get(theme::BORDER_DARK), 1.0);
        //     ctx.fill(fill_rect, &env.get(theme::PRIMARY_DARK));
        // });

        self.inner.paint(ctx, data, env);
    }
}
