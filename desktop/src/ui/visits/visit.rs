use std::time::Duration;

use druid::*;
use druid::widget::*;
use druid::im::Vector;

use crate::rpc::*;
use crate::client::*;

use crate::ui::common::*;
use crate::ui::common::controllers::*;

use super::components::notes;
use notes::SAVE_NOTE;

use super::{prescriptions, diagnose};


type RemoteNoteVec = Vector<RemoteData<Note>>;


#[derive(Clone, Data, Lens, Debug)]
pub struct State {
    #[data(ignore)]
    client: Client,

    visit: RemoteData<Visit>,
    notes: RemoteData<RemoteNoteVec>,

    prescriptions_state: prescriptions::State,
    diagnose_state: diagnose::State,
}


impl State {
    pub fn new(client: Client, visit: Visit) -> Self {
        Self {
            visit: RemoteData::new(visit),
            notes: RemoteData::new(RemoteNoteVec::new()),
            prescriptions_state: prescriptions::State::new(client.new_scope()),
            diagnose_state: diagnose::State::new(client.new_scope()),
            client,
        }
    }

    pub fn with_new_visit(client: Client, patient: &Patient) -> Self {
        let mut state = Self::new(client, Visit::default());
        let visit = Visit { patient_id: patient.id, ..Default::default() };
        state.visit.state = RemoteState::Loading;
        state.client.visit.create(visit).unwrap();
        state
    }

    pub fn with_existing_visit(client: Client, visit: Visit) -> Self {
        let mut state = Self::new(client, Visit::default());
        state.load(visit);
        state
    }

    pub fn load(&mut self, visit: Visit) {
        self.prescriptions_state.load(visit.clone());
        self.diagnose_state.load(visit.clone());

        self.notes.state = RemoteState::Loading;
        self.client.visit.notes(visit.id).unwrap();

        self.visit.state = RemoteState::Loaded;
        self.visit.data = visit;
    }
}


impl RemoteDataLoader for State {
    fn load_response(&mut self, response: &ClientResponse) {
        match response {
            ClientResponse::Visit(VisitReply::Notes(result)) => {
                match result {
                    Ok(notes) => {
                        let mut ret = Vec::with_capacity(notes.len());
                        for note in notes {
                            ret.push(RemoteData::new(note.clone()));
                        }
                        self.notes.update_result(&Ok(Vector::from(ret)));
                        // Ok(Vector::from(ret))
                    }
                    Err(err) => {
                        println!("{:?}", err);
                    }
                    // Err(err) => Err(err.clone())
                };

                // self.notes.update_result(&remote_notes);
            }
            _ => {}
        }
    }
}


pub fn breadcrumb() -> impl Widget<State> {
    Flex::column()
        .with_child(Label::new("Visit"))
        .with_child(
            Label::new(|data: &State, _env: &_| {
                data.visit.data.started.format("%e %b %Y").to_string()
            })
                .with_text_size(12.0)
                .with_text_color(Color::GRAY)
        )
}


pub fn view() -> impl Widget<State> {
    let notes = Flex::column()
        .with_child(notes::view().lens(State::notes).center())
        .expand_width()
        .controller(NoteSaver {});

    let prescriptions = prescriptions::view().lens(State::prescriptions_state);
    let diagnose = diagnose::view().lens(State::diagnose_state);

    let slide_duration = Duration::from_millis(120).as_nanos() as u64;
    let tabs = Tabs::new()
        .with_axis(druid::widget::Axis::Vertical)
        .with_transition(druid::widget::TabsTransition::Slide(slide_duration))
        .with_tab("Notes", notes)
        .with_tab("Prescriptions", prescriptions)
        .with_tab("Diagnose", diagnose);

    let view = Flex::column()
        .with_child(visit_header().lens(State::visit.then(RemoteData::<Visit>::data)))
        .with_spacer(10.0)
        .with_flex_child(tabs, 1.0);

    view.controller(RemoteDataLoaderController::new(State::client))
}

fn visit_header() -> impl Widget<Visit> {
    let started = Label::dynamic(|visit: &Visit, _env| {
        visit.started.format("%d/%m/%Y - %H:%M").to_string()
    });

    let ended = Label::dynamic(|visit: &Visit, _env| {
        visit.ended.map_or_else(|| "in progress".to_string(), |date| date.format("%d/%m/%Y - %H:%M").to_string())
    });

    Flex::column()
        .with_child(Flex::row()
                    .with_child(Label::new("Started:"))
                    .with_child(started))
        .with_child(Flex::row()
                    .with_child(Label::new("Ended:"))
                    .with_child(ended))
}



struct NoteSaver;

impl NoteSaver {
    pub fn save(&self, data: &mut State, note: Note) {
        if note.id == 0 {
            self.create(data, note)
        } else {
            self.update(data, note)
        }
    }

    pub fn create(&self, data: &mut State, mut note: Note) {
        note.visit_id = data.visit.data.id;
        data.client.visit_note.create(note).unwrap();
    }

    pub fn update(&self, data: &mut State, note: Note) {
        data.client.visit_note.update(note).unwrap();
    }
}

impl<W: Widget<State>> Controller<State, W> for NoteSaver {
    fn event(&mut self, child: &mut W, ctx: &mut EventCtx, event: &Event, data: &mut State, env: &Env) {
        match event {
            Event::Notification(notification) => {
                if let Some(note) = notification.get(SAVE_NOTE) {
                    self.save(data, note.clone());
                    ctx.set_handled();
                }
            }
            _ => {}
        }

        child.event(ctx, event, data, env)
    }
}
