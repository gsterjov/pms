use druid::*;
use druid::widget::*;

use crate::client::*;

use super::router;
use super::components::breadcrumb;


#[derive(Clone, Data, Lens)]
pub struct State {
    router: router::State,

    connection_status: String,
}

impl State {
    pub fn new(client: Client) -> State {
        State {
            router: router::State::new(client),
            connection_status: "Disconnected".into(),
        }
    }
}

pub fn view() -> impl Widget<State> {
    let router = router::view().lens(State::router);

    Flex::column()
        .with_child(top_bar().expand_width())
        .with_spacer(30.0)
        .with_flex_child(router, 1.0)
}


fn top_bar() -> impl Widget<State> {
    Flex::row()
        .with_flex_child(
            breadcrumb::view()
                .expand_width()
                .lens(State::router.then(router::State::stack))
            , 1.0)
        .with_child(connection_status().padding(10.0))
}


fn connection_status() -> impl Widget<State> {
    Flex::row().with_child(
        Label::new(|data: &String, _env: &_| data.clone())
            .controller(BackendStatusController::new(
                |data: &mut String, status: &ConnectionStatus| {
                    *data = match status {
                        ConnectionStatus::Disconnected => "Disconnected",
                        ConnectionStatus::Connecting => "Connecting",
                        ConnectionStatus::Authenticating => "Authenticating",
                        ConnectionStatus::Authorising => "Authorising",
                        ConnectionStatus::Ready => "Connected",
                    }
                    .to_string()
                },
            ))
            .lens(State::connection_status),
    )
}

struct BackendStatusController<T> {
    selector: Selector<ConnectionStatus>,
    action: Box<dyn Fn(&mut T, &ConnectionStatus)>,
}

impl<T: Data> BackendStatusController<T> {
    pub fn new(action: impl Fn(&mut T, &ConnectionStatus) + 'static) -> Self {
        BackendStatusController {
            selector: BACKEND_CLIENT_STATUS,
            action: Box::new(action),
        }
    }
}

impl<T, W> Controller<T, W> for BackendStatusController<T>
where
    T: Data,
    W: Widget<T>,
{
    fn event(
        &mut self,
        child: &mut W,
        ctx: &mut druid::EventCtx,
        event: &druid::Event,
        data: &mut T,
        env: &druid::Env,
    ) {
        match event {
            Event::Command(cmd) => {
                if let Some(status) = cmd.get(self.selector) {
                    (self.action)(data, &status);
                    ctx.set_handled();
                }
            }
            _ => {}
        };

        child.event(ctx, event, data, env);
    }
}
