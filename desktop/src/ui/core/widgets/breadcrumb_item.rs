use druid::*;
use druid::widget::*;


pub const BREADCRUMB_ITEM_HIGHLIGHT: Key<Color> = Key::new("pms.ui.breadcrumb_item.highlight");


pub struct BreadcrumbItem<T> {
    inner: WidgetPod<T, Box<dyn Widget<T>>>,
}


impl<T: Data> BreadcrumbItem<T> {
    pub fn new(inner: impl Widget<T> + 'static) -> Self {
        Self {
            inner: WidgetPod::new(inner).boxed(),
        }
    }

    pub fn on_click(self, f: impl Fn(&mut EventCtx, &mut T, &Env) + 'static) -> ControllerHost<Self, Click<T>> {
        ControllerHost::new(self, Click::new(f))
    }
}


impl<T: Data> Widget<T> for BreadcrumbItem<T> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        self.inner.event(ctx, event, data, env);
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, data: &T, env: &Env) {
        if let LifeCycle::HotChanged(_) = event {
            ctx.request_paint();
        }

        self.inner.lifecycle(ctx, event, data, env);
    }

    fn update(&mut self, ctx: &mut UpdateCtx, _old_data: &T, data: &T, env: &Env) {
        self.inner.update(ctx, data, env);
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &T, env: &Env) -> Size {
        bc.debug_check("BreadcrumbItem");

        let size = self.inner.layout(ctx, bc, data, env);
        self.inner.set_origin(ctx, data, env, Point::ORIGIN);
        size
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &T, env: &Env) {
        let bg = if ctx.is_hot() {
            env.get(theme::BACKGROUND_LIGHT)
        } else {
            env.get(theme::BACKGROUND_DARK)
        };

        let panel = ctx.size().to_rect();
        ctx.fill(panel, &bg);

        let highlight = env.try_get(BREADCRUMB_ITEM_HIGHLIGHT).unwrap_or_else(|_| env.get(theme::PRIMARY_DARK));
        let hl = Rect::new(panel.min_x(), panel.max_y() - 2.0, panel.max_x(), panel.max_y());

        ctx.stroke(hl, &highlight, 2.0);

        self.inner.paint(ctx, data, env);
    }
}
