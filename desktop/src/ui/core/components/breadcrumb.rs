use druid::*;
use druid::widget::*;

use crate::ui::core::widgets::*;
use crate::ui::core::router::{View, ViewMatcher, ViewStack, ViewStackItem, ROUTER_POP_UNTIL};

use crate::ui::patients;
use crate::ui::visits;


pub fn view() -> impl Widget<ViewStack> {
    List::new(navigation_item).horizontal().lens(ViewStack::items)
}


fn navigation_item() -> impl Widget<ViewStackItem> {
    let matcher = ViewMatcher::new()
        .home(home())
        .new_patient(new_patient())
        .find_patient(find_patient())
        .view_patient(view_patient())
        .new_visit(new_visit())
        .edit_visit(edit_visit())
        .default(unknown());

    let item = matcher
        .padding(20.0)
        .align_vertical(UnitPoint::CENTER)
        .lens(ViewStackItem::view);


    let breadcrumb_item = BreadcrumbItem::new(item).on_click(|ctx: &mut EventCtx, item: &mut ViewStackItem, _env: &Env| {
        let command = Command::new(ROUTER_POP_UNTIL, item.index, Target::Auto);
        ctx.submit_command(command);
    });

    EnvScope::new(
        |env, data: &ViewStackItem| {
            let color = match data.view {
                View::Home => env.get(theme::PRIMARY_DARK),

                View::NewPatient(_) => Color::GREEN,
                View::FindPatient(_) => Color::SILVER,
                View::ViewPatient(_) => Color::PURPLE,

                View::NewVisit(_) => Color::OLIVE,
                View::EditVisit(_) => Color::LIME,
            };
            env.set(breadcrumb_item::BREADCRUMB_ITEM_HIGHLIGHT, color);
        },
        breadcrumb_item
            .fix_height(100.0)
    )
}


fn home() -> impl Widget<()> {
    Label::new("Home")
}

fn unknown() -> impl Widget<View> {
    Label::new("Unknown")
}

fn find_patient() -> impl Widget<patients::find_patient::State> {
    Label::new("Find Patient")
}

fn new_patient() -> impl Widget<patients::new_patient::State> {
    Label::new("New Patient")
}

fn view_patient() -> impl Widget<patients::view_patient::State> {
    patients::view_patient::breadcrumb()
}

fn new_visit() -> impl Widget<visits::visit::State> {
    visits::visit::breadcrumb()
}

fn edit_visit() -> impl Widget<visits::visit::State> {
    visits::visit::breadcrumb()
}
