use druid::*;
use druid::widget::*;
use druid::im::{Vector, vector};
use druid_enums::Matcher;

use crate::rpc::*;
use crate::client::*;

use super::home;

use crate::ui::patients;
use crate::ui::visits;


pub const ROUTER_CHANGE_VIEW: Selector<Route> = Selector::new("pms.router.change");
pub const ROUTER_POP_UNTIL: Selector<usize> = Selector::new("pms.router.pop_until");


#[derive(Clone, Data, Lens)]
pub struct ViewStackItem {
    pub index: usize,
    pub view: View,
}

#[derive(Clone, Data, Lens)]
pub struct ViewStack {
    pub current_index: usize,
    pub items: Vector<ViewStackItem>,
}

 #[allow(non_upper_case_globals)]
impl ViewStack {
    pub const current_view: CurrentViewLens = CurrentViewLens;

    pub fn push(&mut self, view: View) {
        self.current_index = self.items.len();
        self.items.push_back(ViewStackItem {
            index: self.current_index,
            view,
        });
    }

    pub fn pop_until(&mut self, index: usize) {
        self.current_index = index;
        self.items.truncate(index + 1);
    }
}

pub struct CurrentViewLens;

impl Lens<ViewStack, View> for CurrentViewLens {
    fn with<V, F: FnOnce(&View) -> V>(&self, data: &ViewStack, f: F) -> V {
        f(&data.items[data.current_index].view)
    }

    fn with_mut<V, F: FnOnce(&mut View) -> V>(&self, data: &mut ViewStack, f: F) -> V {
        f(&mut data.items[data.current_index].view)
    }
}


#[derive(Clone, Data, Matcher)]
pub enum View {
    Home,

    NewPatient(patients::new_patient::State),
    FindPatient(patients::find_patient::State),
    ViewPatient(patients::view_patient::State),

    NewVisit(visits::visit::State),
    EditVisit(visits::visit::State),
}


#[derive(Debug)]
pub enum Route {
    NewPatient,
    FindPatient,
    ViewPatient(Patient),

    NewVisit(Patient),
    EditVisit(Visit),
}

#[derive(Clone, Data, Lens)]
pub struct State {
    stack: ViewStack,

    #[data(ignore)]
    client: Client,
}

impl State {
    pub fn new(client: Client) -> Self {
        Self {
            stack: ViewStack {
                current_index: 0,
                items: vector![ViewStackItem { index: 0, view: View::Home }]
            },
            client,
        }
    }
}


pub fn view() -> impl Widget<State> {
    let matcher = route_matcher().lens(State::stack.then(ViewStack::current_view));

    Container::new(matcher).controller(RouteController {})
}

pub fn route_matcher() -> impl Widget<View> {
    ViewMatcher::new()
        .home(home::view())
        .new_patient(patients::new_patient::view())
        .find_patient(patients::find_patient::view())
        .view_patient(patients::view_patient::view())
        .new_visit(visits::visit::view())
        .edit_visit(visits::visit::view())
}

fn build_view(data: &State, route: &Route) -> View {
    match route {
        Route::NewPatient => new_patient(&data.client),
        Route::FindPatient => find_patient(&data.client),
        Route::ViewPatient(patient) => view_patient(&data.client, patient),

        Route::NewVisit(patient) => new_visit(&data.client, patient),
        Route::EditVisit(visit) => edit_visit(&data.client, visit),
    }
}


fn new_patient(client: &Client) -> View {
    let state = patients::new_patient::State::new(client.new_scope());
    View::NewPatient(state)
}

fn find_patient(client: &Client) -> View {
    let state = patients::find_patient::State::new(client.new_scope());
    View::FindPatient(state)
}

fn view_patient(client: &Client, patient: &Patient) -> View {
    let state = patients::view_patient::State::new(client.new_scope(), patient.clone());
    View::ViewPatient(state)
}

fn new_visit(client: &Client, patient: &Patient) -> View {
    let state = visits::visit::State::with_new_visit(client.new_scope(), patient);
    View::NewVisit(state)
}

fn edit_visit(client: &Client, visit: &Visit) -> View {
    let state = visits::visit::State::with_existing_visit(client.new_scope(), visit.clone());
    View::EditVisit(state)
}


fn route_to_view(data: &mut State, route: &Route) {
    let view = build_view(data, route);
    data.stack.push(view);
}

fn pop_until_view(data: &mut State, index: usize) {
    data.stack.pop_until(index);
}


pub struct RouteController;

impl Controller<State, Container<State>> for RouteController {
    fn event(&mut self, child: &mut Container<State>, ctx: &mut EventCtx, event: &Event, data: &mut State, env: &Env) {
        match event {
            Event::Command(command) => {
                if let Some(route) = command.get(ROUTER_CHANGE_VIEW) {
                    route_to_view(data, route);
                    ctx.set_handled();
                }

                if let Some(index) = command.get(ROUTER_POP_UNTIL) {
                    pop_until_view(data, *index);
                    ctx.set_handled();
                }
            }
            Event::Notification(notification) => {
                if let Some(route) = notification.get(ROUTER_CHANGE_VIEW) {
                    route_to_view(data, route);
                    ctx.set_handled();
                }
            }
            _ => {}
        };

        child.event(ctx, event, data, env);
    }
}
