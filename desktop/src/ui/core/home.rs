use druid::*;
use druid::widget::*;

use super::router;


pub fn view() -> impl Widget<()> {
    button_grid()
}


fn button_grid() -> impl Widget<()> {
    Flex::column()
        .with_child(patient_row().expand_width())
}

fn patient_row() -> impl Widget<()> {
    let buttons = Flex::row()
        .with_child(find_patient_button().align_left())
        .with_spacer(10.0)
        .with_child(new_patient_button().align_left())
        .expand_width();

    Flex::column()
        .with_child(Label::new("Patients").with_text_size(30.0))
        .with_spacer(20.0)
        .with_child(buttons)
        .padding(20.0)
        .border(Color::BLACK, 1.0)
}


fn find_patient_button() -> impl Widget<()> {
    Button::new("Find Patient")
        .fix_size(200.0, 200.0)
        .on_click(|ctx, _data, _env| {
            ctx.submit_notification(router::ROUTER_CHANGE_VIEW.with(router::Route::FindPatient));
        })
}

fn new_patient_button() -> impl Widget<()> {
    Button::new("New Patient")
        .fix_size(200.0, 200.0)
        .on_click(|ctx, _data, _env| {
            ctx.submit_notification(router::ROUTER_CHANGE_VIEW.with(router::Route::NewPatient));
        })
}
