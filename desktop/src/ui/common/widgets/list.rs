use druid::*;
use druid::widget::*;


pub struct ListItem<T> {
    inner: WidgetPod<T, Box<dyn Widget<T>>>,
}

impl<T: Data> ListItem<T> {
    pub fn new(inner: impl Widget<T> + 'static) -> Self {
        Self {
            inner: WidgetPod::new(inner).boxed(),
        }
    }

    pub fn on_click(self, f: impl Fn(&mut EventCtx, &mut T, &Env) + 'static) -> ControllerHost<Self, Click<T>> {
        ControllerHost::new(self, Click::new(f))
    }
}

impl<T: Data> Widget<T> for ListItem<T> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        self.inner.event(ctx, event, data, env);
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, data: &T, env: &Env) {
        if let LifeCycle::HotChanged(_) = event {
            ctx.request_paint();
        }
        self.inner.lifecycle(ctx, event, data, env);
    }

    fn update(&mut self, ctx: &mut UpdateCtx, _old_data: &T, data: &T, env: &Env) {
        self.inner.update(ctx, data, env);
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &T, env: &Env) -> Size {
        bc.debug_check("ListItem");
        let size = self.inner.layout(ctx, &bc.clone(), data, env).clone();
        self.inner.set_origin(ctx, data, env, Point::new(0.0, 0.0));
        size
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &T, env: &Env) {
        let colour = if ctx.is_hot() {
            env.get(theme::BACKGROUND_LIGHT)
        } else {
            env.get(theme::BACKGROUND_DARK)
        };

        let panel = ctx.size().to_rect();
        ctx.fill(panel, &colour);

        self.inner.paint(ctx, data, env);
    }
}



pub struct ItemSelected<T: Data, U> {
    selector: Selector<U>,
    f: Box<dyn Fn(&mut T, &U)>,
}

impl<T: Data, U> ItemSelected<T, U> {
    pub fn new(selector: Selector<U>, f: impl Fn(&mut T, &U) + 'static) -> Self {
        Self {
            selector,
            f: Box::new(f),
        }
    }
}

impl<T: Data, U: 'static, W: Widget<T>> Controller<T, W> for ItemSelected<T, U> {
    fn event(&mut self, child: &mut W, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        match event {
            Event::Notification(notification) => {
                if let Some(selected) = notification.get(self.selector) {
                    (self.f)(data, selected);
                    ctx.set_handled();
                }
            }
            _ => {}
        }

        child.event(ctx, event, data, env);
    }
}
