use druid::*;
use druid::widget::*;

use crate::ui::common::RemoteData;


pub struct Loader<T: Data> {
    inner: WidgetPod<RemoteData<T>, Box<dyn Widget<RemoteData<T>>>>,
}


impl<T: Data> Loader<T> {
    pub fn new(loaded: impl Widget<T> + 'static) -> Self {
        let either = Either::new(
            |data: &RemoteData<T>, _env: &Env| data.loading() || data.failed(),
            Loader::loading_view(),
            loaded.lens(RemoteData::<T>::data),
        );

        Self {
            inner: WidgetPod::new(either).boxed(),
        }
    }

    fn loading_view() -> impl Widget<RemoteData<T>> {
        let loading = Flex::row()
            .with_child(Label::new("Loading").with_text_size(30.0))
            .center();

        let error = Label::new(|data: &String, _env: &_| format!("Error: {}", data))
            .with_text_color(Color::RED)
            .lens(RemoteData::<T>::error)
            .center();

        Either::new(|data, _env| data.failed(), error, loading)
    }
}



impl<T: Data> Widget<RemoteData<T>> for Loader<T> {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut RemoteData<T>, env: &Env) {
        self.inner.event(ctx, event, data, env)
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, data: &RemoteData<T>, env: &Env) {
        self.inner.lifecycle(ctx, event, data, env)
    }

    fn update(&mut self, ctx: &mut UpdateCtx, _old_data: &RemoteData<T>, data: &RemoteData<T>, env: &Env) {
        self.inner.update(ctx, data, env)
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &RemoteData<T>, env: &Env) -> Size {
        bc.debug_check("Loader");
        let size = self.inner.layout(ctx, &bc.clone(), data, env).clone();
        self.inner.set_origin(ctx, data, env, Point::ORIGIN);
        size
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &RemoteData<T>, env: &Env) {
        self.inner.paint(ctx, data, env)
    }
}
