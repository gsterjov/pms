use druid::*;
use druid::widget::*;

use crate::ui::common::RemoteDataLoader;
use crate::client::{Client, CLIENT_RESPONSE};


pub struct RemoteDataLoaderController<T, L>
where T: Data, L: Lens<T, Client>
{
    lens: L,
    phantom_t: std::marker::PhantomData<T>,
}

impl<T, L> RemoteDataLoaderController<T, L>
where T: Data, L: Lens<T, Client>
{
    pub fn new(lens: L) -> Self {
        Self { lens, phantom_t: Default::default() }
    }
}

impl<T, W, L> Controller<T, W> for RemoteDataLoaderController<T, L>
where
    T: Data + RemoteDataLoader,
    W: Widget<T>,
    L: Lens<T, Client>
{
    fn event(&mut self, child: &mut W, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        match event {
            Event::Command(cmd) => {
                if let Some(responder) = cmd.get(CLIENT_RESPONSE) {
                    let matching = self.lens.with(data, |client: &Client| {
                        client.id == responder.id
                    });

                    if matching {
                        data.load_response(&responder.response);
                        ctx.set_handled();
                    }
                }
            }
            _ => {}
        };

        child.event(ctx, event, data, env);
    }

    fn lifecycle(&mut self, child: &mut W, ctx: &mut LifeCycleCtx, event: &LifeCycle, data: &T, env: &Env) {
        child.lifecycle(ctx, event, data, env)
    }

    fn update(&mut self, child: &mut W, ctx: &mut UpdateCtx, old_data: &T, data: &T, env: &Env) {
        child.update(ctx, old_data, data, env)
    }
}
