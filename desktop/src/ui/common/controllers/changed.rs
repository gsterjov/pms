use std::time::Duration;

use druid::*;
use druid::widget::*;


pub struct DataChanged<T: Data> {
    timer: Option<TimerToken>,
    delay: Duration,
    notifier: Selector<T>,
}

impl<T: Data> DataChanged<T> {
    pub fn new(notifier: Selector<T>) -> Self {
        DataChanged::with_delay(notifier, Duration::from_secs(3))
    }

    pub fn with_delay(notifier: Selector<T>, delay: Duration) -> Self {
        DataChanged { timer: None, delay, notifier }
    }
}

impl<T: Data, W: Widget<T>> Controller<T, W> for DataChanged<T> {
    fn event(&mut self, child: &mut W, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        match event {
            Event::Timer(token) if Some(*token) == self.timer => {
                ctx.submit_notification(self.notifier.with(data.clone()));
            }
            _ => {}
        }
        child.event(ctx, event, data, env)
    }

    fn update(&mut self, child: &mut W, ctx: &mut UpdateCtx, old_data: &T, data: &T, env: &Env) {
        if !data.same(old_data) {
            self.timer = Some(ctx.request_timer(self.delay));
        }
        child.update(ctx, old_data, data, env)
    }
}



pub struct TextChanged<T> {
    delay: Duration,
    action: Box<dyn Fn(&mut EventCtx, &mut T)>,
    timer: Option<TimerToken>,
}

impl<T: Data> TextChanged<T> {
    pub fn new(action: impl Fn(&mut EventCtx, &mut T) + 'static) -> Self {
        Self::with_delay(Duration::from_millis(300), action)
    }

    pub fn with_delay(delay: Duration, action: impl Fn(&mut EventCtx, &mut T) + 'static) -> Self {
        Self {
            delay,
            action: Box::new(action),
            timer: None,
        }
    }
}

impl Controller<String, TextBox<String>> for TextChanged<String> {
    fn event(&mut self, child: &mut TextBox<String>, ctx: &mut EventCtx, event: &Event, data: &mut String, env: &Env) {
        match event {
            Event::KeyDown(k) if k.code == Code::Enter => {
                (self.action)(ctx, data);
            }
            Event::Timer(token) if Some(*token) == self.timer => {
                (self.action)(ctx, data);
            }
            _ => child.event(ctx, event, data, env),
        }
    }

    fn update(&mut self, child: &mut TextBox<String>, ctx: &mut UpdateCtx, old_data: &String, data: &String, env: &Env) {
        if !data.same(old_data) {
            self.timer = Some(ctx.request_timer(self.delay));
        }
        child.update(ctx, old_data, data, env)
    }
}
