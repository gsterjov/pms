use druid::*;
use druid::widget::*;


pub const FILTER_CHANGED: Selector<()> = Selector::new("pms.widget.filter.changed");

pub struct FilterController<T> {
    action: Box<dyn Fn(&mut EventCtx, &mut T)>,
}

impl<T: Data> FilterController<T> {
    pub fn new(action: impl Fn(&mut EventCtx, &mut T) + 'static) -> Self {
        Self { action: Box::new(action) }
    }
}

impl<T, W> Controller<T, W> for FilterController<T>
where
    T: Data,
    W: Widget<T>,
{
    fn event(&mut self, child: &mut W, ctx: &mut EventCtx, event: &Event, data: &mut T, env: &Env) {
        match event {
            Event::Notification(notification) => {
                if notification.is(FILTER_CHANGED) {
                    (self.action)(ctx, data);
                    ctx.set_handled();
                }
            }
            _ => {}
        };

        child.event(ctx, event, data, env);
    }
}
