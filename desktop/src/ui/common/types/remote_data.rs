use druid::{Data, Lens};
use crate::client::{ClientResponse, ClientResult};


#[derive(Clone, Debug, Data, PartialEq)]
pub enum RemoteState {
    Unloaded,
    Loading,
    Loaded,
    Failed,
}

#[derive(Clone, Debug, Data, Lens)]
pub struct RemoteData<T: Data> {
    pub state: RemoteState,
    pub data: T,
    pub error: String,
}

impl<T: Data> RemoteData<T> {
    pub fn new(data: T) -> Self {
        RemoteData {
            data,
            state: RemoteState::Unloaded,
            error: "".to_string(),
        }
    }

    pub fn update_result(&mut self, result: &ClientResult<T>) {
        match result {
            Ok(data) => {
                self.data = data.clone();
                self.state = RemoteState::Loaded;
            }
            Err(err) => {
                self.error = format!("{}", err);
                self.state = RemoteState::Failed;
            }
        }
    }

    pub fn loading(&self) -> bool {
        return self.state == RemoteState::Loading;
    }
    pub fn failed(&self) -> bool {
        return self.state == RemoteState::Failed;
    }
}



pub trait RemoteDataLoader {
    fn load_response(&mut self, response: &ClientResponse);
}
