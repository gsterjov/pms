use druid::*;
use druid::widget::*;
use druid::im::Vector;

use crate::rpc::*;
use crate::ui::common::*;
use crate::ui::common::widgets::*;

use crate::ui::core::router;


type RemotePatientVec = RemoteData<Vector<Patient>>;


pub fn view() -> impl Widget<RemotePatientVec> {
    Loader::new(search_results())
}

fn search_results() -> impl Widget<Vector<Patient>> {
    Scroll::new(
        List::new(patient_list_item).with_spacing(10.0)
    ).vertical()
}


fn patient_list_item() -> impl Widget<Patient> {
    let renderer = |item: &String, _env: &Env| item.clone();

    let first_name = Label::new(renderer).lens(Patient::first_name);
    let last_name = Label::new(renderer).lens(Patient::last_name);
    // let mobile = Label::new(renderer).lens(Patient::mobile_phone);

    let row = Flex::row()
        .cross_axis_alignment(CrossAxisAlignment::Center)
        .must_fill_main_axis(true)
        .with_child(first_name)
        .with_child(last_name)
        .with_flex_spacer(1.0)
        // .with_child(mobile)
        .padding(10.0)
        .expand_width();

    ListItem::new(row)
        .on_click(|ctx, data: &mut Patient, _env| {
            let route = router::Route::ViewPatient(data.clone());
            ctx.submit_notification(router::ROUTER_CHANGE_VIEW.with(route));
        })
}
