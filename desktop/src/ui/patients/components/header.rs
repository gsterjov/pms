use druid::*;
use druid::widget::*;

use crate::rpc::*;

use crate::ui::core::router;
use crate::ui::common::*;
use crate::ui::patients::view_patient::PatientData;


pub fn view() -> impl Widget<PatientData> {
    Flex::row()
        .with_child(summary())
        .with_spacer(50.0)
        .with_child(action_bar())
}


fn summary() -> impl Widget<PatientData> {
    let name = Label::new(|data: &Patient, _env: &_| {
        format!("{} {}", data.first_name, data.last_name)
    })
    .with_text_size(30.0);

    let dob = Label::new(|dob: &Patient, _env: &_| {
        let dob = match dob.birth_date {
            Some(ref date) => date.format("%d/%m/%Y").to_string(),
            None => "".to_string(),
        };
        format!("{}", dob)
    });

    Flex::column()
        .with_child(name)
        .with_child(dob)
        .lens(PatientData::patient.then(RemoteData::<Patient>::data))
}


fn action_bar() -> impl Widget<PatientData> {
    Flex::row()
        .with_child(start_visit_button().fix_height(40.0))
        .lens(PatientData::patient.then(RemoteData::<Patient>::data))
}


fn start_visit_button() -> impl Widget<Patient> {
    Button::new("Start Visit").on_click(|ctx, data: &mut Patient, _env| {
        let route = router::Route::NewVisit(data.clone());
        ctx.submit_notification(router::ROUTER_CHANGE_VIEW.with(route));
    })
}

