use druid::widget::*;
use druid::text::Selection;
use druid::text::format::*;

use crate::rpc::*;


const LABEL_COLUMN_WIDTH: f64 = 120.0;

pub fn patient_details() -> Flex<Patient> {
    let first_name_row = Flex::row()
        .with_child(Label::new("Full Name").fix_width(LABEL_COLUMN_WIDTH))
        .with_child(TextBox::new().lens(Patient::first_name));

    let last_name_row = Flex::row()
        .with_child(Label::new("Last Name").fix_width(LABEL_COLUMN_WIDTH))
        .with_child(TextBox::new().lens(Patient::last_name));

    let middle_name_row = Flex::row()
        .with_child(Label::new("Middle Name").fix_width(LABEL_COLUMN_WIDTH))
        .with_child(TextBox::new().lens(Patient::middle_name));

    let preferred_name_row = Flex::row()
        .with_child(Label::new("Preferred Name").fix_width(LABEL_COLUMN_WIDTH))
        .with_child(TextBox::new().lens(Patient::preferred_name));

    let birth_date = Flex::row()
        .with_child(Label::new("Birth Date").fix_width(LABEL_COLUMN_WIDTH))
        .with_child(TextBox::new()
                    .with_formatter(NaiveDateFormatter {})
                    .lens(Patient::birth_date));

    Flex::column()
        .with_child(first_name_row)
        .with_spacer(5.0)
        .with_child(last_name_row)
        .with_spacer(5.0)
        .with_child(middle_name_row)
        .with_spacer(5.0)
        .with_child(preferred_name_row)
        .with_spacer(5.0)
        .with_child(birth_date)
}



#[derive(Debug, Clone)]
pub enum NaiveDateValidationError {
    Parse(chrono::ParseError),
    InvalidChar(char),
}

impl std::fmt::Display for NaiveDateValidationError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            NaiveDateValidationError::Parse(err) => write!(f, "Invalid date format: {}", err),
            NaiveDateValidationError::InvalidChar(c) => write!(f, "Invalid character: '{}'", c),
        }
    }
}

impl std::error::Error for NaiveDateValidationError {}


pub struct NaiveDateFormatter;

impl Formatter<Option<NaiveDate>> for NaiveDateFormatter {
    fn format(&self, value: &Option<NaiveDate>) -> String {
        match value {
            Some(ref date) => date.format("%d/%m/%Y").to_string(),
            None => "".to_string(),
        }
    }

    fn validate_partial_input(&self, input: &str, _sel: &Selection) -> Validation {
        if input.is_empty() { return Validation::success(); }

        match input.chars().skip_while(|c| c.is_ascii_digit() || *c == '/').next() {
            None => Validation::success(),
            Some(c) => Validation::failure(NaiveDateValidationError::InvalidChar(c)),
        }
    }

    fn value(&self, input: &str) -> Result<Option<NaiveDate>, ValidationError> {
        if input.is_empty() { return Ok(None); }

        match chrono::NaiveDate::parse_from_str(input, "%d/%m/%Y") {
            Ok(date) => Ok(Some(date.into())),
            Err(err) => Err(ValidationError::new(NaiveDateValidationError::Parse(err))),
        }
    }
}
