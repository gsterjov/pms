use druid::*;
use druid::widget::*;

use crate::rpc::*;


pub fn phone_numbers() -> impl Widget<Contact> {
    Flex::column()
        .with_child(phone_number_list())
        .with_spacer(10.0)
        .with_child(add_number_button())
}

fn phone_number_list() -> impl Widget<Contact> {
    List::new(phone_number_item)
        .with_spacing(5.0)
        .lens(Contact::phone_numbers)
}

fn phone_number_item() -> impl Widget<PhoneNumber> {
    TextBox::new().fix_width(150.0).lens(PhoneNumber::number)
}

fn add_number_button() -> impl Widget<Contact> {
    Button::new("Add contact number").on_click(|_event, data: &mut Contact, _env| {
        data.phone_numbers.push_back(PhoneNumber::default());
    })
}

pub fn emails() -> impl Widget<Contact> {
    Flex::column()
        .with_child(email_list())
        .with_spacer(10.0)
        .with_child(add_email_button())
}

fn email_list() -> impl Widget<Contact> {
    List::new(email_item)
        .with_spacing(5.0)
        .lens(Contact::emails)
}

fn email_item() -> impl Widget<Email> {
    TextBox::new().fix_width(300.0).lens(Email::address)
}

fn add_email_button() -> impl Widget<Contact> {
    Button::new("Add email address").on_click(|_event, data: &mut Contact, _env| {
        data.emails.push_back(Email::default());
    })
}
