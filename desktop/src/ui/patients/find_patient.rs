use druid::*;
use druid::widget::*;
use druid::im::{vector, Vector};

use crate::rpc::*;
use crate::client::*;

use crate::ui::common::*;
use crate::ui::common::controllers::*;

use super::components::search_results;


#[derive(Clone, Data, Lens, Debug, Default)]
pub struct PatientFilter {
    name: String,
}

#[derive(Clone, Data, Lens, Debug)]
pub struct State {
    #[data(ignore)]
    client: Client,

    filter: PatientFilter,
    patients: RemoteData<Vector<Patient>>,
}

impl State {
    pub fn new(client: Client) -> State {
        State {
            client,
            filter: PatientFilter::default(),
            patients: RemoteData::new(vector![]),
        }
    }
}

impl RemoteDataLoader for State {
    fn load_response(&mut self, response: &ClientResponse) {
        match response {
            ClientResponse::Patient(PatientReply::Find(result)) => {
                self.patients.update_result(result);
            }
            _ => {}
        }
    }
}

pub fn view() -> impl Widget<State> {
    Flex::column()
        .with_child(filter())
        .with_child(
            search_results::view()
                .fix_width(400.0)
                .padding(10.0)
                .center()
                .lens(State::patients)
        )
        .controller(RemoteDataLoaderController::new(State::client))
}

fn filter() -> impl Widget<State> {
    let search_box = search_box().lens(PatientFilter::name);

    search_box
        .fix_width(400.0)
        .padding(10.0)
        .border(Color::rgb(0.0, 0.0, 0.0), 1.0)
        .center()
        .lens(State::filter)
        .controller(FilterController::new(|_ctx: &mut EventCtx, data: &mut State| {
            data.patients.state = RemoteState::Loading;
            data.client.patient.find(&data.filter.name).unwrap();
        }))
}

fn search_box() -> impl Widget<String> {
    TextBox::new()
        .with_placeholder("Search by name")
        .controller(TextChanged::new(|ctx: &mut EventCtx, _data: &mut String| {
            ctx.submit_notification(FILTER_CHANGED);
        }))
}
