use druid::*;
use druid::widget::*;

use crate::rpc::*;
use crate::client::*;

use super::components::patient_contact;
use super::components::patient_details::patient_details;


#[derive(Clone, Data, Lens, Debug)]
pub struct State {
    #[data(ignore)]
    client: Client,

    patient: Patient,
    contact: Contact,
}

impl State {
    pub fn new(client: Client) -> State {
        State {
            client,
            patient: Patient::default(),
            contact: Contact::default(),
        }
    }
}

pub fn view() -> impl Widget<State> {
    Flex::column()
        .with_child(
            patient_details()
                .fix_width(400.0)
                .padding(10.0)
                .border(Color::BLACK, 1.0)
                .lens(State::patient),
        )
        .with_child(
            patient_contact::phone_numbers()
                .fix_width(400.0)
                .padding(10.0)
                .border(Color::BLACK, 1.0)
                .lens(State::contact),
        )
        .with_child(
            patient_contact::emails()
                .fix_width(400.0)
                .padding(10.0)
                .border(Color::BLACK, 1.0)
                .lens(State::contact),
        )
        .with_child(
            action_button()
                .padding(20.0)
                .align_vertical(UnitPoint::BOTTOM),
        )
}

fn action_button() -> impl Widget<State> {
    Button::new("Save").on_click(|_event, data: &mut State, _env| {
        data.client.patient.create(data.patient.clone()).unwrap();
    })
}
