use std::time::Duration;

use druid::*;
use druid::widget::*;
use druid::im::Vector;

use crate::rpc::*;
use crate::client::*;

use crate::ui::common::*;
use crate::ui::common::widgets::*;
use crate::ui::common::controllers::*;

use crate::ui::visits::components::{visit_list, summary, action_bars};
use super::components::{patient_details, patient_contact, header};


#[derive(Clone, Debug, Data, Lens)]
pub struct PatientData {
    patient: RemoteData<Patient>,
    contact: RemoteData<Contact>,
}



#[derive(Clone, Debug, Data, Lens)]
pub struct State {
    #[data(ignore)]
    client: Client,

    patient_data: PatientData,
    visits: RemoteData<Vector<Visit>>,
    selected_visit: Visit,
    selected_notes: RemoteData<Vector<Note>>,
    selected_findings: RemoteData<Vector<crate::rpc::clinical::Finding>>,
    selected_prescriptions: RemoteData<Vector<Prescription>>,
}

impl State {
    pub fn new(client: Client, patient: Patient) -> State {
        let mut data = State {
            patient_data: PatientData {
                patient: RemoteData::new(patient.clone()),
                contact: RemoteData::new(Contact { patient_id: patient.id, ..Contact::default() }),
            },
            visits: RemoteData::new(Vector::new()),
            selected_visit: Visit::default(),
            selected_notes: RemoteData::new(Vector::new()),
            selected_findings: RemoteData::new(Vector::new()),
            selected_prescriptions: RemoteData::new(Vector::new()),
            client,
        };

        load(&mut data);
        data
    }
}

impl RemoteDataLoader for State {
    fn load_response(&mut self, response: &ClientResponse) {
        match response {
            ClientResponse::Patient(PatientReply::Read(result)) =>{
                self.patient_data.patient.update_result(result);
            }
            ClientResponse::Contact(ContactReply::Read(result)) => {
                self.patient_data.contact.update_result(result);
            }
            ClientResponse::Visit(VisitReply::Find(result)) => {
                self.visits.update_result(result);
            }
            ClientResponse::Visit(VisitReply::Notes(result)) => {
                self.selected_notes.update_result(result);
            }
            ClientResponse::Visit(VisitReply::ClinicalFindings(result)) => {
                self.selected_findings.update_result(result);
            }
            ClientResponse::Visit(VisitReply::Prescriptions(result)) => {
                self.selected_prescriptions.update_result(result);
            }
            _ => {}
        }
    }
}


pub fn breadcrumb() -> impl Widget<State> {
    Label::new(|remote: &RemoteData<Patient>, _env: &Env| {
        match remote.state {
            RemoteState::Unloaded => "New patient".to_string(),
            RemoteState::Loading => "Loading".to_string(),
            RemoteState::Loaded => format!("{} {}", remote.data.first_name, remote.data.last_name),
            RemoteState::Failed => format!("{} {}", remote.data.first_name, remote.data.last_name),
        }
    }).lens(State::patient_data.then(PatientData::patient))
}


pub fn view() -> impl Widget<State> {
    let edit_details = Flex::column()
        .with_child(patient_details_view().center())
        .padding(10.0)
        .expand_width();

    let contact_details = Flex::column()
        .with_child(contact_details_view().center())
        .padding(10.0)
        .expand_width();

    let slide_duration = Duration::from_millis(120).as_nanos() as u64;

    let tabs = Tabs::new()
        .with_axis(druid::widget::Axis::Vertical)
        .with_transition(druid::widget::TabsTransition::Slide(slide_duration))
        .with_tab("Patient Details", edit_details)
        .with_tab("Contact Details", contact_details)
        .with_tab("Past Visits", visits_view());

    let view = Flex::column()
        .with_child(
            header::view()
                .expand_width()
                .padding(10.0)
                .lens(State::patient_data)
        )
        .with_spacer(20.0)
        .with_flex_child(tabs, 1.0);

    view.controller(RemoteDataLoaderController::new(State::client))
}


fn patient_details_view() -> impl Widget<State> {
    Loader::new(patient_details::patient_details())
        .lens(State::patient_data.then(PatientData::patient))
}

fn contact_details_view() -> impl Widget<State> {
    let view = Flex::column()
        .with_child(patient_contact::phone_numbers())
        .with_spacer(10.0)
        .with_child(patient_contact::emails());

    Loader::new(view)
        // .controller(RemoteDataResponse::new(PATIENT_CONTACT_UPDATE_RESPONSE, |data: &mut RemoteData<Contact>, _resp: &Response<()>| {
        //     data.state = RemoteState::Loaded;
        // }))
        .lens(State::patient_data.then(PatientData::contact))
}


fn visits_view() -> impl Widget<State> {
    let scroll = Scroll::new(visit_list::view()).vertical();

    let view = Flex::column()
        .with_flex_child(scroll.fix_width(200.0), 1.0);
        // .with_child(visit_summary_view().lens(State::selected_visit));

    let loader = Loader::new(view)
        .lens(State::visits)
        .controller(ItemSelected::new(visit_list::VISIT_SELECTED, |data: &mut State, visit: &Visit| {
            data.selected_visit = visit.clone();

            data.selected_notes.state = RemoteState::Loading;
            data.client.visit.notes(visit.id).unwrap();

            data.selected_findings.state = RemoteState::Loading;
            data.client.visit.clinical_findings(visit.id).unwrap();

            data.selected_prescriptions.state = RemoteState::Loading;
            data.client.visit.prescriptions(visit.id).unwrap();
        }));

    let summary = Flex::column()
        .with_child(
            action_bars::visit_summary_actions()
                .fix_height(40.0)
                .lens(State::selected_visit)
        )
        .with_child(
            summary::view()
                .padding(10.0)
                .lens(State::selected_visit)
        )
        .with_child(Label::new("Findings:").with_text_size(20.0).center())
        .with_child(
            summary::findings()
                .padding(10.0)
                .center()
                .lens(State::selected_findings)
        )
        .with_child(Label::new("Prescribed:").with_text_size(20.0).center())
        .with_child(
            summary::prescriptions()
                .padding(10.0)
                .center()
                .lens(State::selected_prescriptions)
        )
        .with_child(Label::new("Notes:").with_text_size(20.0).center())
        .with_child(
            summary::notes()
                .padding(10.0)
                .center()
                .lens(State::selected_notes)
        );

    Flex::row()
        .with_spacer(10.0)
        .with_child(loader)
        .with_spacer(10.0)
        .with_child(summary.expand_height())
}


fn load(data: &mut State) {
    data.patient_data.patient.state = RemoteState::Loading;
    data.client.patient.read(data.patient_data.patient.data.id).unwrap();

    data.patient_data.contact.state = RemoteState::Loading;
    data.client.contact.read(data.patient_data.patient.data.clone()).unwrap();

    data.visits.state = RemoteState::Loading;
    data.client.visit.find(data.patient_data.patient.data.id).unwrap();
}
