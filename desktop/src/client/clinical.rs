use async_trait::async_trait;

use crate::rpc::clinical::*;
use super::core::{Executer, ClientError, Command, ClientResult, JobSender};


#[derive(Debug)]
pub enum FindingCommand {
    Create(Finding),
    Update(Finding),
    Delete(i64),
}

#[derive(Debug)]
pub enum FindingReply {
    Create(ClientResult<Finding>),
    Update(ClientResult<Finding>),
    Delete(ClientResult<Finding>),
}

#[derive(Clone, Debug)]
pub struct FindingCommandSender {
    job_sender: JobSender,
}

impl FindingCommandSender {
    pub fn new(job_sender: JobSender) -> Self {
        FindingCommandSender { job_sender }
    }

    fn send_command(&mut self, command: FindingCommand) -> Result<(), ClientError> {
        self.job_sender.send(Command::Finding(command))
    }

    pub fn create(&mut self, record: Finding) -> Result<(), ClientError> {
        self.send_command(FindingCommand::Create(record))
    }

    pub fn update(&mut self, record: Finding) -> Result<(), ClientError> {
        self.send_command(FindingCommand::Update(record))
    }

    pub fn delete(&mut self, id: i64) -> Result<(), ClientError> {
        self.send_command(FindingCommand::Delete(id))
    }
}


pub struct FindingCommandProcessor {
    finding_store: FindingStore,
}

impl FindingCommandProcessor {
    pub fn new(finding_store: FindingStore) -> Self {
        Self { finding_store }
    }
}


#[async_trait(?Send)]
impl Executer for FindingCommandProcessor {
    type Command = FindingCommand;
    type Reply = FindingReply;

    async fn execute(&mut self, command: Self::Command) -> Self::Reply {
        match command {
            FindingCommand::Create(record) => {
                let result = self.finding_store.create(&record).await;
                FindingReply::Create(result)
            }
            FindingCommand::Update(record) => {
                let result = self.finding_store.update(&record).await;
                FindingReply::Update(result)
            }
            FindingCommand::Delete(id) => {
                let result = self.finding_store.delete(id).await;
                FindingReply::Delete(result)
            }
        }
    }
}
