pub mod core;
pub mod patients;
pub mod visits;
pub mod clinical;
pub mod terminology;

pub use crate::client::core::{Reactor, Client, BackendError, ClientError};
pub use crate::client::core::{ConnectionStatus, BACKEND_CLIENT_STATUS};
pub use crate::client::core::{CLIENT_RESPONSE, ClientResult, ClientResponse};

pub use patients::{
    PatientList,
    PatientReply,
    ContactReply,
};

pub use visits::{
    VisitList,
    NoteList,
    PrescriptionList,
    VisitReply,
    NoteReply,
    PrescriptionReply,
};

pub use terminology::{
    FindingList,
    MedicineList,
    SearchReply,
};
