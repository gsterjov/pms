use async_trait::async_trait;
use druid::im::Vector;

use crate::rpc::terminology::*;
use super::core::{Executer, ClientError, ClientResult, Command, JobSender};


pub type FindingList = Vector<Finding>;
pub type MedicineList = Vector<Medicine>;


#[derive(Debug)]
pub enum SearchCommand {
    Findings(String),
    Medicines(String),
}

#[derive(Debug)]
pub enum SearchReply {
    Findings(ClientResult<FindingList>),
    Medicines(ClientResult<MedicineList>),
}


#[derive(Clone, Debug)]
pub struct SearchCommandSender {
    job_sender: JobSender,
}

impl SearchCommandSender {
    pub fn new(job_sender: JobSender) -> Self {
        SearchCommandSender { job_sender }
    }

    fn send_command(&mut self, command: SearchCommand) -> Result<(), ClientError> {
        self.job_sender.send(Command::Terminology(command))
    }

    pub fn findings(&mut self, query: String) -> Result<(), ClientError> {
        self.send_command(SearchCommand::Findings(query))
    }

    pub fn medicines(&mut self, query: String) -> Result<(), ClientError> {
        self.send_command(SearchCommand::Medicines(query))
    }
}


pub struct SearchCommandProcessor {
    terminology_search: TerminologySearch,
}

impl SearchCommandProcessor {
    pub fn new(terminology_search: TerminologySearch) -> Self {
        Self { terminology_search }
    }
}


#[async_trait(?Send)]
impl Executer for SearchCommandProcessor {
    type Command = SearchCommand;
    type Reply = SearchReply;

    async fn execute(&mut self, command: Self::Command) -> Self::Reply {
        match command {
            SearchCommand::Findings(query) => {
                let criteria = FindingSearchCriteria { query, refset_id: 0 };
                let result = self.terminology_search.findings(&criteria).await.map(|list| Vector::from(list.findings));
                SearchReply::Findings(result)
            },
            SearchCommand::Medicines(query) => {
                let criteria = MedicineSearchCriteria { query };
                let result = self.terminology_search.medicines(&criteria).await.map(|list| Vector::from(list.medicines));
                SearchReply::Medicines(result)
            },
        }
    }
}
