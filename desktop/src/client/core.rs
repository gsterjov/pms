use std::time::Duration;
use std::sync::mpsc::{channel, Receiver, Sender, RecvTimeoutError};
use std::sync::atomic::{AtomicUsize, Ordering};

use thiserror::Error;

use async_trait::async_trait;
use futures::{AsyncReadExt, FutureExt};
use tokio_util::compat::TokioAsyncReadCompatExt;
use tokio::time::error::Elapsed;

use capnp_rpc::{rpc_twoparty_capnp::Side, twoparty, RpcSystem};
use druid::{ExtEventSink, Selector, Target};

use crate::rpc::core::{Auth, Capabilities};
use super::patients::{
    ContactCommand, ContactCommandProcessor, ContactCommandSender, PatientCommand,
    PatientCommandProcessor, PatientCommandSender,
};
use super::visits::{VisitCommand, VisitCommandSender, VisitCommandProcessor};

use super::{patients, visits, clinical, terminology};


pub type ClientId = usize;
static GLOBAL_NEXT_CLIENT_ID: AtomicUsize = AtomicUsize::new(0);


pub type ClientResult<T> = Result<T, BackendError>;

pub const BACKEND_CLIENT_STATUS: Selector<ConnectionStatus> = Selector::new("pms.backend.connection.status");
pub const CLIENT_RESPONSE: Selector<ClientResponder> = Selector::new("pms.client.response");


#[derive(Debug)]
pub struct ClientResponder {
    pub id: ClientId,
    pub response: ClientResponse,
}


#[derive(Debug, Clone, PartialEq)]
pub enum ConnectionStatus {
    Disconnected,
    Connecting,
    Authenticating,
    Authorising,
    Ready,
}

#[derive(Error, Debug)]
pub enum BackendError {
    #[error("Disconnected from backend. {0}")]
    Disconnected(String),

    #[error("Backend call failed. {0}")]
    Failed(String),

    #[error("Backend overloaded. {0}")]
    Overloaded(String),

    #[error("The backend has not implemented the call. {0}")]
    Unimplemented(String),
}

impl From<capnp::Error> for BackendError {
    fn from(err: capnp::Error) -> Self {
        match err.kind {
            capnp::ErrorKind::Failed => BackendError::Failed(err.description),
            capnp::ErrorKind::Overloaded => BackendError::Overloaded(err.description),
            capnp::ErrorKind::Disconnected => BackendError::Disconnected(err.description),
            capnp::ErrorKind::Unimplemented => BackendError::Unimplemented(err.description),
        }
    }
}

#[derive(Error, Debug)]
pub enum ClientError {
    #[error("Network error: {0}")]
    Network(#[from] std::io::Error),

    #[error("RPC thread has died: {0}")]
    Thread(#[from] std::sync::mpsc::SendError<Job>),

    #[error("Backend error: {0}")]
    Backend(#[from] BackendError),

    #[error("UI thread has died: {0}")]
    UiThread(#[from] druid::ExtEventError),
}

#[derive(Debug)]
pub enum Command {
    Patient(PatientCommand),
    Contact(ContactCommand),

    Visit(VisitCommand),
    VisitNote(visits::NoteCommand),
    Prescription(visits::PrescriptionCommand),

    Finding(clinical::FindingCommand),

    Terminology(terminology::SearchCommand),
}

impl From<PatientCommand> for Command {
    fn from(cmd: PatientCommand) -> Self {
        Command::Patient(cmd)
    }
}

impl From<ContactCommand> for Command {
    fn from(cmd: ContactCommand) -> Self {
        Command::Contact(cmd)
    }
}


#[derive(Debug)]
pub enum ClientResponse {
    Patient(patients::PatientReply),
    Contact(patients::ContactReply),

    Visit(visits::VisitReply),
    VisitNote(visits::NoteReply),
    Prescription(visits::PrescriptionReply),

    Finding(clinical::FindingReply),

    Terminology(terminology::SearchReply),
}

impl From<patients::PatientReply> for ClientResponse {
    fn from(reply: patients::PatientReply) -> Self {
        ClientResponse::Patient(reply)
    }
}

impl From<patients::ContactReply> for ClientResponse {
    fn from(reply: patients::ContactReply) -> Self {
        ClientResponse::Contact(reply)
    }
}

impl From<visits::VisitReply> for ClientResponse {
    fn from(reply: visits::VisitReply) -> Self {
        ClientResponse::Visit(reply)
    }
}

impl From<visits::NoteReply> for ClientResponse {
    fn from(reply: visits::NoteReply) -> Self {
        ClientResponse::VisitNote(reply)
    }
}

impl From<visits::PrescriptionReply> for ClientResponse {
    fn from(reply: visits::PrescriptionReply) -> Self {
        ClientResponse::Prescription(reply)
    }
}

impl From<clinical::FindingReply> for ClientResponse {
    fn from(reply: clinical::FindingReply) -> Self {
        ClientResponse::Finding(reply)
    }
}

impl From<terminology::SearchReply> for ClientResponse {
    fn from(reply: terminology::SearchReply) -> Self {
        ClientResponse::Terminology(reply)
    }
}


#[async_trait(?Send)]
pub trait Executer {
    type Command;
    type Reply;

    async fn execute(&mut self, command: Self::Command) -> Self::Reply;
}

struct Capability<T> {
    executer: Option<T>,
}

impl<T: Executer> Capability<T> {
    fn new(executer: Option<T>) -> Self {
        Self { executer }
    }

    async fn execute(&mut self, command: T::Command) -> Result<T::Reply, ClientError> {
        match self.executer.as_mut() {
            Some(executer) => Ok(executer.execute(command).await),
            None => Err(ClientError::Backend(BackendError::Failed(
                "PERMISSION DENIED".to_string(),
            ))),
        }
    }
}

struct CommandProcessor {
    patients: Capability<PatientCommandProcessor>,
    contacts: Capability<ContactCommandProcessor>,

    visits: Capability<VisitCommandProcessor>,
    visit_notes: Capability<visits::NoteCommandProcessor>,
    prescriptions: Capability<visits::PrescriptionCommandProcessor>,

    findings: Capability<clinical::FindingCommandProcessor>,

    terminology_search: Capability<terminology::SearchCommandProcessor>,
}

impl CommandProcessor {
    fn new(capabilities: Capabilities) -> Self {
        let patient_executer = capabilities.patient_store.map_or(None, |store| {
            Some(PatientCommandProcessor::new(store))
        });
        let contact_executer = capabilities.contact_store.map_or(None, |store| {
            Some(ContactCommandProcessor::new(store))
        });

        let visit_executer = capabilities.visit_store.map_or(None, |store| {
            Some(VisitCommandProcessor::new(store))
        });
        let visit_note_executer = capabilities.visit_note_store.map_or(None, |store| {
            Some(visits::NoteCommandProcessor::new(store))
        });
        let prescription_executer = capabilities.prescription_store.map_or(None, |store| {
            Some(visits::PrescriptionCommandProcessor::new(store))
        });

        let finding_executer = capabilities.finding_store.map_or(None, |store| {
            Some(clinical::FindingCommandProcessor::new(store))
        });

        let terminology_search_executer = capabilities.terminology_search.map_or(None, |search| {
            Some(terminology::SearchCommandProcessor::new(search))
        });

        CommandProcessor {
            patients: Capability::new(patient_executer),
            contacts: Capability::new(contact_executer),

            visits: Capability::new(visit_executer),
            visit_notes: Capability::new(visit_note_executer),
            prescriptions: Capability::new(prescription_executer),

            findings: Capability::new(finding_executer),

            terminology_search: Capability::new(terminology_search_executer),
        }
    }

    async fn process(&mut self, job: Job) -> Result<ClientResponder, ClientError> {
        let reply = match job.command {
            Command::Patient(cmd) => self.patients.execute(cmd).await?.into(),
            Command::Contact(cmd) => self.contacts.execute(cmd).await?.into(),

            Command::Visit(cmd) => self.visits.execute(cmd).await?.into(),
            Command::VisitNote(cmd) => self.visit_notes.execute(cmd).await?.into(),
            Command::Prescription(cmd) => self.prescriptions.execute(cmd).await?.into(),

            Command::Finding(cmd) => self.findings.execute(cmd).await?.into(),

            Command::Terminology(cmd) => self.terminology_search.execute(cmd).await?.into(),
        };

        Ok(ClientResponder {
            id: job.client_id,
            response: reply,
        })
    }
}

enum Transition {
    Disconnect,
    Connect,
    Authenticate(Connection),
    Authorise(Connection),
    ProcessCommands(Connection, CommandProcessor),
}


#[derive(Clone, Debug)]
pub struct JobSender {
    client_id: ClientId,
    sender: Sender<Job>,
}

impl JobSender {
    pub fn new(client_id: ClientId, sender: Sender<Job>) -> Self {
        Self { client_id, sender }
    }
    pub fn send(&mut self, command: Command) -> Result<(), ClientError> {
        self.sender.send(Job {
            client_id: self.client_id,
            command
        })?;
        Ok(())
    }
}

#[derive(Debug)]
pub struct Job {
    client_id: ClientId,
    command: Command,
}


#[derive(Clone, Debug)]
pub struct Client {
    pub id: ClientId,
    sender: Sender<Job>,

    pub patient: PatientCommandSender,
    pub contact: ContactCommandSender,

    pub visit: VisitCommandSender,
    pub visit_note: visits::NoteCommandSender,
    pub prescription: visits::PrescriptionCommandSender,

    pub finding: clinical::FindingCommandSender,

    pub terminology: terminology::SearchCommandSender,
}

impl Client {
    pub fn new(sender: Sender<Job>) -> Self {
        let id = Self::generate_client_id();
        let job_sender = JobSender::new(id, sender.clone());

        Self {
            // patient protocols
            patient: PatientCommandSender::new(job_sender.clone()),
            contact: ContactCommandSender::new(job_sender.clone()),

            // visit protocols
            visit: VisitCommandSender::new(job_sender.clone()),
            visit_note: visits::NoteCommandSender::new(job_sender.clone()),
            prescription: visits::PrescriptionCommandSender::new(job_sender.clone()),

            // clinical protocols
            finding: clinical::FindingCommandSender::new(job_sender.clone()),

            // terminology server protocols
            terminology: terminology::SearchCommandSender::new(job_sender.clone()),

            // internal scope and response fields
            id,
            sender,
        }
    }

    pub fn new_scope(&self) -> Self {
        Self::new(self.sender.clone())
    }

    fn generate_client_id() -> ClientId {
        // using an atomic global will do for now but there are better ways to
        // do this. right now its possible to exhaust the id range from ui
        // interaction since a scope can be created with any new app data
        GLOBAL_NEXT_CLIENT_ID.fetch_add(1, Ordering::SeqCst)
    }
}

pub struct Reactor {
    receiver: Receiver<Job>,
    status: ConnectionStatus,

    event_sink: ExtEventSink,
}

impl Reactor {
    pub fn new(event_sink: ExtEventSink) -> (Reactor, Client) {
        let (sender, receiver) = channel();
        (
            Reactor {
                receiver,
                event_sink,
                status: ConnectionStatus::Disconnected,
            },
            Client::new(sender),
        )
    }

    pub async fn run(&mut self) -> Result<(), ClientError> {
        let mut next_transition = Transition::Disconnect;

        loop {
            next_transition = match next_transition {
                Transition::Disconnect => {
                    self.status(ConnectionStatus::Disconnected)?;
                    Transition::Connect
                }
                Transition::Connect => {
                    self.status(ConnectionStatus::Connecting)?;
                    match Reactor::connect_with_retry_timeout().await {
                        Ok(connection) => Transition::Authenticate(connection),
                        Err(_) => Transition::Disconnect,
                    }
                }
                Transition::Authenticate(connection) => {
                    self.status(ConnectionStatus::Authenticating)?;
                    Transition::Authorise(connection)
                }
                Transition::Authorise(mut connection) => {
                    self.status(ConnectionStatus::Authorising)?;
                    match connection.auth.stores().await {
                        Ok(capabilities) => Transition::ProcessCommands(connection, CommandProcessor::new(capabilities)),
                        Err(err) => panic!("Authorisation failed: {}", err),
                    }
                }
                Transition::ProcessCommands(connection, mut processor) => {
                    self.status(ConnectionStatus::Ready)?;
                    match self.receiver.recv_timeout(Duration::from_secs(30)) {
                        Ok(job) => Reactor::execute_command(&mut processor, job, &self.event_sink).await,
                        Err(RecvTimeoutError::Timeout) => self.check_connection(),
                        Err(RecvTimeoutError::Disconnected) => {
                            panic!("All senders are disconnected")
                        }
                    };

                    Transition::ProcessCommands(connection, processor)
                }
            };
        }
    }

    pub async fn connect_with_retry_timeout() -> Result<Connection, Elapsed> {
        // if the OS TCP timeout is longer than the timeout duration then this
        // will only ever make one connection attempt.
        tokio::time::timeout(Duration::from_secs(30), async {
            // make connection attempts and only log errors if they occur.
            // the timeout ensures that the socket is closed once elapsed.
            loop {
                match Connection::connect("127.0.0.1:8080").await {
                    Ok(connection) => return connection,
                    Err(err) => println!("Failed to connect to backend: {}", err),
                };
                tokio::time::sleep(Duration::from_secs(3)).await;
            }
        })
        .await
    }

    fn check_connection(&mut self) {
        println!("ping");
    }

    fn status(&mut self, status: ConnectionStatus) -> Result<(), ClientError> {
        if self.status != status {
            println!("Backend: {:?}", status);
            self.event_sink.submit_command(
                BACKEND_CLIENT_STATUS,
                status.clone(),
                Target::Global,
            )?;
            self.status = status;
        }

        Ok(())
    }

    async fn execute_command(processor: &mut CommandProcessor, job: Job, event_sink: &ExtEventSink) {
        // when we get a disconnected error from an rpc call the connection
        // is no longer useable. we have to recreate the rpc client so the
        // response should be intercepted and the error bubbled up for the
        // event loop to process appropriately.
        // let result = match response {
        //     Err(BackendError::Disconnected(reason)) => {
        //         self.event_sink.submit_command(selector, Err(BackendError::Disconnected(reason.clone()).into()), Target::Global)?;
        //         return Err(BackendError::Disconnected(reason).into());
        //     }
        //     Ok(payload) => Ok(payload),
        //     Err(err) => Err(ClientError::from(err)),
        // };

        // self.event_sink.submit_command(selector, result, Target::Global)?;

        match processor.process(job).await {
            Ok(reply) => {
                event_sink.submit_command(CLIENT_RESPONSE, reply, Target::Global).unwrap();
            },
            Err(err) => println!("ERROR: {}", err),
        }
    }
}

pub struct Connection {
    pub handle: tokio::task::JoinHandle<()>,
    pub auth: Auth,
}

impl Connection {
    pub async fn connect<A: tokio::net::ToSocketAddrs>(addr: A) -> Result<Connection, ClientError> {
        let socket = tokio::net::TcpStream::connect(addr).await?;
        socket.set_nodelay(true)?;

        // get the remote connection
        let (reader, writer) = TokioAsyncReadCompatExt::compat(socket).split();
        let network = twoparty::VatNetwork::new(reader, writer, Side::Client, Default::default());

        // define the rpc client
        let mut rpc_system = RpcSystem::new(Box::new(network), None);

        let auth = Auth::new(rpc_system.bootstrap(Side::Server));

        // drive the system
        let handle = tokio::task::spawn_local(Box::pin(rpc_system.map(|_| ())));

        Ok(Connection { handle, auth })
    }
}
