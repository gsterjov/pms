use async_trait::async_trait;
use druid::im::Vector;

use crate::rpc::visits::*;
use crate::rpc::clinical::Finding;
use super::core::{Executer, ClientError, Command, ClientResult, JobSender};


pub type VisitList = Vector<Visit>;
pub type NoteList = Vector<Note>;
pub type PrescriptionList = Vector<Prescription>;
pub type FindingList = Vector<Finding>;


#[derive(Debug)]
pub enum VisitCommand {
    Create(Visit),
    Update(Visit),
    Find(i32),

    Notes(i64),
    Prescriptions(i64),
    ClinicalFindings(i64),
}

#[derive(Debug)]
pub enum VisitReply {
    Create(ClientResult<()>),
    Update(ClientResult<()>),
    Find(ClientResult<VisitList>),

    Notes(ClientResult<NoteList>),
    Prescriptions(ClientResult<PrescriptionList>),
    ClinicalFindings(ClientResult<FindingList>),
}


#[derive(Clone, Debug)]
pub struct VisitCommandSender {
    job_sender: JobSender,
}

impl VisitCommandSender {
    pub fn new(job_sender: JobSender) -> Self {
        VisitCommandSender { job_sender }
    }

    fn send_command(&mut self, command: VisitCommand) -> Result<(), ClientError> {
        self.job_sender.send(Command::Visit(command))
    }

    pub fn create(&mut self, record: Visit) -> Result<(), ClientError> {
        self.send_command(VisitCommand::Create(record))
    }

    pub fn update(&mut self, record: Visit) -> Result<(), ClientError> {
        self.send_command(VisitCommand::Update(record))
    }

    pub fn find(&mut self, patient_id: i32) -> Result<(), ClientError> {
        self.send_command(VisitCommand::Find(patient_id))
    }

    pub fn notes(&mut self, visit_id: i64) -> Result<(), ClientError> {
        self.send_command(VisitCommand::Notes(visit_id))
    }

    pub fn prescriptions(&mut self, visit_id: i64) -> Result<(), ClientError> {
        self.send_command(VisitCommand::Prescriptions(visit_id))
    }

    pub fn clinical_findings(&mut self, visit_id: i64) -> Result<(), ClientError> {
        self.send_command(VisitCommand::ClinicalFindings(visit_id))
    }
}


pub struct VisitCommandProcessor {
    visit_store: VisitStore,
}

impl VisitCommandProcessor {
    pub fn new(visit_store: VisitStore) -> Self {
        Self { visit_store }
    }
}


#[async_trait(?Send)]
impl Executer for VisitCommandProcessor {
    type Command = VisitCommand;
    type Reply = VisitReply;

    async fn execute(&mut self, command: Self::Command) -> Self::Reply {
        match command {
            VisitCommand::Create(record) => {
                let result = self.visit_store.create(&record).await;
                VisitReply::Create(result)
            }
            VisitCommand::Update(record) => {
                let result = self.visit_store.update(&record).await;
                VisitReply::Update(result)
            }
            VisitCommand::Find(patient_id) => {
                let result = self.visit_store.find(patient_id).await.map(|list| Vector::from(list));
                VisitReply::Find(result)
            }
            VisitCommand::Notes(visit_id) => {
                let result = self.visit_store.notes(visit_id).await.map(|list| Vector::from(list));
                VisitReply::Notes(result)
            }
            VisitCommand::Prescriptions(visit_id) => {
                let result = self.visit_store.prescriptions(visit_id).await.map(|list| Vector::from(list));
                VisitReply::Prescriptions(result)
            }
            VisitCommand::ClinicalFindings(visit_id) => {
                let result = self.visit_store.clinical_findings(visit_id).await.map(|list| Vector::from(list));
                VisitReply::ClinicalFindings(result)
            }
        }
    }
}



#[derive(Debug)]
pub enum NoteCommand {
    Create(Note),
    Update(Note),
}

#[derive(Debug)]
pub enum NoteReply {
    Create(ClientResult<Note>),
    Update(ClientResult<()>),
}

#[derive(Clone, Debug)]
pub struct NoteCommandSender {
    job_sender: JobSender,
}

impl NoteCommandSender {
    pub fn new(job_sender: JobSender) -> Self {
        NoteCommandSender { job_sender }
    }

    fn send_command(&mut self, command: NoteCommand) -> Result<(), ClientError> {
        self.job_sender.send(Command::VisitNote(command))
    }

    pub fn create(&mut self, record: Note) -> Result<(), ClientError> {
        self.send_command(NoteCommand::Create(record))
    }

    pub fn update(&mut self, record: Note) -> Result<(), ClientError> {
        self.send_command(NoteCommand::Update(record))
    }
}


pub struct NoteCommandProcessor {
    note_store: NoteStore,
}

impl NoteCommandProcessor {
    pub fn new(note_store: NoteStore) -> Self {
        Self { note_store }
    }
}


#[async_trait(?Send)]
impl Executer for NoteCommandProcessor {
    type Command = NoteCommand;
    type Reply = NoteReply;

    async fn execute(&mut self, command: Self::Command) -> Self::Reply {
        match command {
            NoteCommand::Create(record) => {
                let result = self.note_store.create(&record).await;
                NoteReply::Create(result)
            }
            NoteCommand::Update(record) => {
                let result = self.note_store.update(&record).await;
                NoteReply::Update(result)
            }
        }
    }
}



#[derive(Debug)]
pub enum PrescriptionCommand {
    Create(Prescription),
    Update(Prescription),
}

#[derive(Debug)]
pub enum PrescriptionReply {
    Create(ClientResult<Prescription>),
    Update(ClientResult<Prescription>),
}

#[derive(Clone, Debug)]
pub struct PrescriptionCommandSender {
    job_sender: JobSender,
}

impl PrescriptionCommandSender {
    pub fn new(job_sender: JobSender) -> Self {
        PrescriptionCommandSender { job_sender }
    }

    fn send_command(&mut self, command: PrescriptionCommand) -> Result<(), ClientError> {
        self.job_sender.send(Command::Prescription(command))
    }

    pub fn create(&mut self, record: Prescription) -> Result<(), ClientError> {
        self.send_command(PrescriptionCommand::Create(record))
    }

    pub fn update(&mut self, record: Prescription) -> Result<(), ClientError> {
        self.send_command(PrescriptionCommand::Update(record))
    }
}


pub struct PrescriptionCommandProcessor {
    prescription_store: PrescriptionStore,
}

impl PrescriptionCommandProcessor {
    pub fn new(prescription_store: PrescriptionStore) -> Self {
        Self { prescription_store }
    }
}


#[async_trait(?Send)]
impl Executer for PrescriptionCommandProcessor {
    type Command = PrescriptionCommand;
    type Reply = PrescriptionReply;

    async fn execute(&mut self, command: Self::Command) -> Self::Reply {
        match command {
            PrescriptionCommand::Create(record) => {
                let result = self.prescription_store.create(&record).await;
                PrescriptionReply::Create(result)
            }
            PrescriptionCommand::Update(record) => {
                let result = self.prescription_store.update(&record).await;
                PrescriptionReply::Update(result)
            }
        }
    }
}
