use async_trait::async_trait;
use druid::im::Vector;

use crate::rpc::patients::*;
use crate::client::core::{ClientError, ClientResult, Executer, JobSender, Command};


pub type PatientList = Vector<Patient>;


#[derive(Debug)]
pub enum PatientCommand {
    Create(Patient),
    Read(i32),
    Update(Patient),
    Find(String),
}


#[derive(Debug)]
pub enum PatientReply {
    Create(ClientResult<()>),
    Read(ClientResult<Patient>),
    Update(ClientResult<()>),
    Find(ClientResult<PatientList>),
}

#[derive(Debug)]
pub enum ContactReply {
    Read(ClientResult<Contact>),
    Update(ClientResult<()>),
}


#[derive(Clone, Debug)]
pub struct PatientCommandSender {
    job_sender: JobSender,
}

impl PatientCommandSender {
    pub fn new(job_sender: JobSender) -> Self {
        PatientCommandSender { job_sender }
    }

    fn send_command(&mut self, command: PatientCommand) -> Result<(), ClientError> {
        self.job_sender.send(command.into())
    }

    pub fn create(&mut self, record: Patient) -> Result<(), ClientError> {
        self.send_command(PatientCommand::Create(record))
    }

    pub fn read(&mut self, id: i32) -> Result<(), ClientError> {
        self.send_command(PatientCommand::Read(id))
    }

    pub fn update(&mut self, record: Patient) -> Result<(), ClientError> {
        self.send_command(PatientCommand::Update(record))
    }

    pub fn find(&mut self, name: &str) -> Result<(), ClientError> {
        self.send_command(PatientCommand::Find(name.to_string()))
    }
}

pub struct PatientCommandProcessor {
    patient_store: PatientStore,
}

impl PatientCommandProcessor {
    pub fn new(patient_store: PatientStore) -> Self {
        Self { patient_store }
    }
}

#[async_trait(?Send)]
impl Executer for PatientCommandProcessor {
    type Command = PatientCommand;
    type Reply = PatientReply;

    async fn execute(&mut self, command: Self::Command) -> Self::Reply {
        match command {
            PatientCommand::Create(record) => {
                let result = self.patient_store.create(&record).await;
                PatientReply::Create(result)
            }
            PatientCommand::Read(id) => {
                let result = self.patient_store.read(id).await;
                PatientReply::Read(result)
            }
            PatientCommand::Update(record) => {
                let result = self.patient_store.update(&record).await;
                PatientReply::Update(result)
            }
            PatientCommand::Find(name) => {
                let result = self.patient_store.find(&name).await.map(|list| Vector::from(list));
                PatientReply::Find(result)
            }
        }
    }
}

#[derive(Debug)]
pub enum ContactCommand {
    Read(Patient),
    Update(Contact),
}

#[derive(Clone, Debug)]
pub struct ContactCommandSender {
    job_sender: JobSender,
}

impl ContactCommandSender {
    pub fn new(job_sender: JobSender) -> Self {
        ContactCommandSender { job_sender }
    }

    fn send_command(&mut self, command: ContactCommand) -> Result<(), ClientError> {
        self.job_sender.send(Command::Contact(command))
    }

    pub fn read(&mut self, record: Patient) -> Result<(), ClientError> {
        self.send_command(ContactCommand::Read(record))
    }

    pub fn update(&mut self, record: Contact) -> Result<(), ClientError> {
        self.send_command(ContactCommand::Update(record))
    }
}

pub struct ContactCommandProcessor {
    contact_store: ContactStore,
}

impl ContactCommandProcessor {
    pub fn new(contact_store: ContactStore) -> Self {
        Self { contact_store }
    }
}

#[async_trait(?Send)]
impl Executer for ContactCommandProcessor {
    type Command = ContactCommand;
    type Reply = ContactReply;

    async fn execute(&mut self, command: Self::Command) -> Self::Reply {
        match command {
            ContactCommand::Read(patient) => {
                let result = self.contact_store.read(patient.id).await;
                ContactReply::Read(result)
            }
            ContactCommand::Update(contact) => {
                let result = self.contact_store.update(&contact).await;
                ContactReply::Update(result)
            }
        }
    }
}
